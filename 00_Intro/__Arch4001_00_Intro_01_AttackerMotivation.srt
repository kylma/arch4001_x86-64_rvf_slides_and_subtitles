1
00:00:00,320 --> 00:00:04,240
so let's talk briefly about attacker

2
00:00:01,920 --> 00:00:07,200
motivations why would someone go and

3
00:00:04,240 --> 00:00:10,160
infect a firmware in short the answer is

4
00:00:07,200 --> 00:00:12,559
real ultimate power

5
00:00:10,160 --> 00:00:14,400
realistically the firmware is typically

6
00:00:12,559 --> 00:00:17,119
the most privileged software on the

7
00:00:14,400 --> 00:00:19,520
system and so there are certain nation

8
00:00:17,119 --> 00:00:21,760
states and certain adversaries who

9
00:00:19,520 --> 00:00:23,519
typically like to take up residence at

10
00:00:21,760 --> 00:00:25,519
that most powerful position on the

11
00:00:23,519 --> 00:00:27,519
system there's others who are just fine

12
00:00:25,519 --> 00:00:29,279
with running around down in user space

13
00:00:27,519 --> 00:00:30,640
and you know they can get the job done

14
00:00:29,279 --> 00:00:33,680
that way too

15
00:00:30,640 --> 00:00:36,399
so to understand why the infection of

16
00:00:33,680 --> 00:00:38,000
firmware leads to real ultimate power we

17
00:00:36,399 --> 00:00:40,000
have to understand how computers

18
00:00:38,000 --> 00:00:41,600
actually do useful things

19
00:00:40,000 --> 00:00:44,079
well it starts typically with the

20
00:00:41,600 --> 00:00:46,239
firmware and the firmware configures the

21
00:00:44,079 --> 00:00:48,399
hardware hands off to something like a

22
00:00:46,239 --> 00:00:50,399
bootloader which is going to load a more

23
00:00:48,399 --> 00:00:52,800
full-featured system like an operating

24
00:00:50,399 --> 00:00:54,879
system or a hypervisor and that's going

25
00:00:52,800 --> 00:00:57,440
to then control the resources throughout

26
00:00:54,879 --> 00:00:58,399
the rest of the power on cycle of the

27
00:00:57,440 --> 00:00:59,840
system

28
00:00:58,399 --> 00:01:02,079
and it's going to load things like

29
00:00:59,840 --> 00:01:04,080
applications which do the actual useful

30
00:01:02,079 --> 00:01:07,360
things it's generally speaking you know

31
00:01:04,080 --> 00:01:08,960
the databases the word processors those

32
00:01:07,360 --> 00:01:10,479
are the things that are actually doing

33
00:01:08,960 --> 00:01:13,040
useful stuff

34
00:01:10,479 --> 00:01:15,439
so the history of attacks is that

35
00:01:13,040 --> 00:01:17,280
attackers had started creating malicious

36
00:01:15,439 --> 00:01:19,360
software at the application level

37
00:01:17,280 --> 00:01:21,520
defenders started creating defensive

38
00:01:19,360 --> 00:01:23,439
software which would catch the malicious

39
00:01:21,520 --> 00:01:25,360
software at the application level and

40
00:01:23,439 --> 00:01:28,240
then attackers got the bright idea that

41
00:01:25,360 --> 00:01:30,400
hey if i just move one level up

42
00:01:28,240 --> 00:01:32,240
then i'm going to have more power and

43
00:01:30,400 --> 00:01:34,720
privilege than the defensive software

44
00:01:32,240 --> 00:01:36,799
and i will be able to defeat it

45
00:01:34,720 --> 00:01:38,560
and the defensive software saw that and

46
00:01:36,799 --> 00:01:41,119
said well i guess i need to get up there

47
00:01:38,560 --> 00:01:44,079
too and that way they can defeat the

48
00:01:41,119 --> 00:01:46,640
malware and that process continued back

49
00:01:44,079 --> 00:01:49,439
and back and back until you finally get

50
00:01:46,640 --> 00:01:52,560
to the firmware level but unfortunately

51
00:01:49,439 --> 00:01:54,399
as of today architecturally the attacker

52
00:01:52,560 --> 00:01:56,640
wins down at the firmware level

53
00:01:54,399 --> 00:01:58,960
once you get to something where it's the

54
00:01:56,640 --> 00:02:01,520
same privileged software you know

55
00:01:58,960 --> 00:02:03,119
fighting over the same resources then

56
00:02:01,520 --> 00:02:05,280
it's really just a question of you know

57
00:02:03,119 --> 00:02:07,840
who wants it more who knows more about

58
00:02:05,280 --> 00:02:09,280
the other software in order to defeat it

59
00:02:07,840 --> 00:02:11,200
and then when it comes to detecting

60
00:02:09,280 --> 00:02:12,720
malicious software one of these things

61
00:02:11,200 --> 00:02:15,280
is not like the other

62
00:02:12,720 --> 00:02:17,760
so applications operating systems

63
00:02:15,280 --> 00:02:19,520
hypervisors and even boot loaders are

64
00:02:17,760 --> 00:02:22,879
all just what we think of as normal

65
00:02:19,520 --> 00:02:24,959
software which just runs and lives on a

66
00:02:22,879 --> 00:02:27,520
hard drive or an ssd

67
00:02:24,959 --> 00:02:29,599
on the other hand firmware is something

68
00:02:27,520 --> 00:02:32,319
that is typically stored in a

69
00:02:29,599 --> 00:02:33,840
non-volatile flash memory on a

70
00:02:32,319 --> 00:02:35,599
motherboard it's typically you know

71
00:02:33,840 --> 00:02:36,560
soldered down to some circuit board

72
00:02:35,599 --> 00:02:39,360
somewhere

73
00:02:36,560 --> 00:02:41,760
and so that means even trying to detect

74
00:02:39,360 --> 00:02:43,519
this software you know you could hope to

75
00:02:41,760 --> 00:02:46,080
find something in a bootloader by just

76
00:02:43,519 --> 00:02:47,760
doing hard drive firmware forensics but

77
00:02:46,080 --> 00:02:49,680
you can't hope to find malicious

78
00:02:47,760 --> 00:02:51,360
software down at the firmware level by

79
00:02:49,680 --> 00:02:53,280
doing that sort of approach

80
00:02:51,360 --> 00:02:55,760
and as of today robust software to do

81
00:02:53,280 --> 00:02:56,800
firmware forensics is very much in its

82
00:02:55,760 --> 00:02:58,959
infancy

83
00:02:56,800 --> 00:03:00,959
so even if companies wanted to detect it

84
00:02:58,959 --> 00:03:02,239
very often they don't have good options

85
00:03:00,959 --> 00:03:04,000
to do that

86
00:03:02,239 --> 00:03:07,200
unfortunately today the only truly

87
00:03:04,000 --> 00:03:09,120
trustworthy way to detect or mitigate

88
00:03:07,200 --> 00:03:11,040
the infection down at the firmware level

89
00:03:09,120 --> 00:03:13,680
is to actually physically attach to

90
00:03:11,040 --> 00:03:16,480
flash chips and rewrite the contents or

91
00:03:13,680 --> 00:03:18,720
read the contents for detection purposes

92
00:03:16,480 --> 00:03:20,319
and if malware got into the system

93
00:03:18,720 --> 00:03:21,680
through the traditional means you know

94
00:03:20,319 --> 00:03:24,159
coming in from the internet and then

95
00:03:21,680 --> 00:03:25,840
spreading around inside the network well

96
00:03:24,159 --> 00:03:28,640
if it guts down to this level it would

97
00:03:25,840 --> 00:03:31,200
be extremely costly and time-consuming

98
00:03:28,640 --> 00:03:32,560
to actually try to recover from it

99
00:03:31,200 --> 00:03:35,519
so again why would someone want to

100
00:03:32,560 --> 00:03:37,360
infect the bios well it's free past

101
00:03:35,519 --> 00:03:39,120
persistence forever they can get down to

102
00:03:37,360 --> 00:03:41,599
that deepest level and it's extremely

103
00:03:39,120 --> 00:03:43,280
hard to dig them out attackers know that

104
00:03:41,599 --> 00:03:44,799
very few organizations are actually

105
00:03:43,280 --> 00:03:46,239
checking down at the bios level and

106
00:03:44,799 --> 00:03:48,000
consequently they can pretty much

107
00:03:46,239 --> 00:03:50,000
guarantee that if they can get there

108
00:03:48,000 --> 00:03:52,319
they can live on the systems for years

109
00:03:50,000 --> 00:03:54,799
on end so then is hacking the bios the

110
00:03:52,319 --> 00:03:56,720
end goal in and of itself well probably

111
00:03:54,799 --> 00:03:58,000
not they probably want to get down to

112
00:03:56,720 --> 00:03:59,920
some other

113
00:03:58,000 --> 00:04:01,599
portion of the system such as the kernel

114
00:03:59,920 --> 00:04:04,400
or applications where interesting stuff

115
00:04:01,599 --> 00:04:07,040
goes on keystrokes come in application

116
00:04:04,400 --> 00:04:08,640
data is processed and then collect the

117
00:04:07,040 --> 00:04:10,480
information from there

118
00:04:08,640 --> 00:04:13,120
and so what can an attacker really do

119
00:04:10,480 --> 00:04:15,040
once they infect bios well the answer is

120
00:04:13,120 --> 00:04:17,040
they can do anything that the system can

121
00:04:15,040 --> 00:04:19,199
do because they have the full power and

122
00:04:17,040 --> 00:04:22,320
privileges of the system

123
00:04:19,199 --> 00:04:24,240
indeed they who run first run best so

124
00:04:22,320 --> 00:04:26,240
let's see some quick examples of that

125
00:04:24,240 --> 00:04:28,400
well you could infect the bios and then

126
00:04:26,240 --> 00:04:30,479
you could actually just brick it as we

127
00:04:28,400 --> 00:04:32,240
say you corrupt the firmware and

128
00:04:30,479 --> 00:04:34,320
rendering the system a brick it won't

129
00:04:32,240 --> 00:04:35,919
boot anymore and in reality it's

130
00:04:34,320 --> 00:04:38,080
actually only required to change a

131
00:04:35,919 --> 00:04:39,840
single byte of the firmware and you can

132
00:04:38,080 --> 00:04:41,199
successfully make the system never boot

133
00:04:39,840 --> 00:04:43,759
again

134
00:04:41,199 --> 00:04:46,800
this was actually seen in the cih virus

135
00:04:43,759 --> 00:04:49,120
back in 1998 and although wikipedia says

136
00:04:46,800 --> 00:04:51,120
that it infected 60 million systems i

137
00:04:49,120 --> 00:04:52,800
find that number a little bit dubious

138
00:04:51,120 --> 00:04:55,440
given the spreading techniques that were

139
00:04:52,800 --> 00:04:57,360
available at the time nevertheless it

140
00:04:55,440 --> 00:05:00,080
was you know an extremely bad day for

141
00:04:57,360 --> 00:05:01,600
anyone who had their system bricked

142
00:05:00,080 --> 00:05:03,280
then there's the bios back dooring like

143
00:05:01,600 --> 00:05:05,520
we already said an attacker starting

144
00:05:03,280 --> 00:05:07,600
from the bios can infect their way down

145
00:05:05,520 --> 00:05:10,400
to the bootloader because they load the

146
00:05:07,600 --> 00:05:12,080
boot loader which can in turn infect the

147
00:05:10,400 --> 00:05:14,400
operating system because the bootloader

148
00:05:12,080 --> 00:05:16,960
loads the operating system which can in

149
00:05:14,400 --> 00:05:19,039
turn infect the applications which do

150
00:05:16,960 --> 00:05:21,360
the interesting stuff

151
00:05:19,039 --> 00:05:25,120
this was in fact caught in the wild in

152
00:05:21,360 --> 00:05:27,440
2011 in some chinese systems where

153
00:05:25,120 --> 00:05:30,080
malware was infecting the bios and then

154
00:05:27,440 --> 00:05:31,919
it was bouncing its way down to attack

155
00:05:30,080 --> 00:05:33,440
the rest of the system

156
00:05:31,919 --> 00:05:35,840
another thing an attacker could do is

157
00:05:33,440 --> 00:05:37,840
what i call an uber evil made attack in

158
00:05:35,840 --> 00:05:40,080
a traditional evil mate attack a

159
00:05:37,840 --> 00:05:41,520
attacker infects the bootloader and then

160
00:05:40,080 --> 00:05:43,120
captures the full disk encryption

161
00:05:41,520 --> 00:05:44,160
password when someone logs into their

162
00:05:43,120 --> 00:05:46,160
system

163
00:05:44,160 --> 00:05:48,160
now we said before that a bootloader is

164
00:05:46,160 --> 00:05:50,240
actually just software running on the

165
00:05:48,160 --> 00:05:52,000
hard drive and consequently it's

166
00:05:50,240 --> 00:05:54,560
vulnerable to detection through normal

167
00:05:52,000 --> 00:05:57,199
hard drive forensics so if an attacker

168
00:05:54,560 --> 00:05:59,199
actually lives back on the spy flash

169
00:05:57,199 --> 00:06:01,680
chip where nobody's checking then they

170
00:05:59,199 --> 00:06:03,520
can just in memory only infect the

171
00:06:01,680 --> 00:06:05,759
bootloader so that it's not vulnerable

172
00:06:03,520 --> 00:06:08,080
to hard drive forensics and then they

173
00:06:05,759 --> 00:06:10,800
can just go ahead and capture the hard

174
00:06:08,080 --> 00:06:12,960
drive password as an attack as a

175
00:06:10,800 --> 00:06:14,639
person logs into their own system and

176
00:06:12,960 --> 00:06:16,639
then at that point they can you know go

177
00:06:14,639 --> 00:06:18,720
capture the full hard drive contents and

178
00:06:16,639 --> 00:06:20,080
decrypt it at their leisure or of course

179
00:06:18,720 --> 00:06:22,080
they can make their way into the

180
00:06:20,080 --> 00:06:24,479
operating system as normal

181
00:06:22,080 --> 00:06:26,240
when bios infection is applied to more

182
00:06:24,479 --> 00:06:28,080
of a server or virtualization

183
00:06:26,240 --> 00:06:30,479
infrastructure it means they can play

184
00:06:28,080 --> 00:06:33,280
the exact same game going from the bios

185
00:06:30,479 --> 00:06:35,440
to a bootloader to a hypervisor and then

186
00:06:33,280 --> 00:06:38,240
they can infect every virtual machine

187
00:06:35,440 --> 00:06:40,160
that that hypervisor is managing and so

188
00:06:38,240 --> 00:06:42,160
you've got all the eggs in one basket

189
00:06:40,160 --> 00:06:44,160
and they can go ahead and smash or steal

190
00:06:42,160 --> 00:06:45,520
the eggs so this of course would be a

191
00:06:44,160 --> 00:06:48,000
problem for you know large

192
00:06:45,520 --> 00:06:51,360
virtualization infrastructure providers

193
00:06:48,000 --> 00:06:53,440
now between 2006 and 2009 jonathan her

194
00:06:51,360 --> 00:06:55,680
colleagues at invisible things labs had

195
00:06:53,440 --> 00:06:57,599
done investigation into different ways

196
00:06:55,680 --> 00:07:00,800
to attack the system at ever more

197
00:06:57,599 --> 00:07:03,280
progressively lower levels in 2006 with

198
00:07:00,800 --> 00:07:06,560
her blue pill research she talked about

199
00:07:03,280 --> 00:07:08,400
using virtualization to as malicious

200
00:07:06,560 --> 00:07:10,960
software rather than just normal

201
00:07:08,400 --> 00:07:12,639
software in order to basically

202
00:07:10,960 --> 00:07:14,800
virtualize an operating system that was

203
00:07:12,639 --> 00:07:16,800
not expecting it and in so doing take

204
00:07:14,800 --> 00:07:19,199
control over all of the resources that

205
00:07:16,800 --> 00:07:20,000
it thought it was managing

206
00:07:19,199 --> 00:07:22,240
so

207
00:07:20,000 --> 00:07:24,639
in that work she coined the phrase ring

208
00:07:22,240 --> 00:07:26,080
negative one to indicate that this

209
00:07:24,639 --> 00:07:28,639
virtualization software was

210
00:07:26,080 --> 00:07:31,280
intrinsically more privileged than ring

211
00:07:28,639 --> 00:07:34,080
zero kernel software because again

212
00:07:31,280 --> 00:07:36,720
intel's convention was that higher ring

213
00:07:34,080 --> 00:07:38,639
numbers were less privileged and lower

214
00:07:36,720 --> 00:07:40,720
ring numbers were more privileged so

215
00:07:38,639 --> 00:07:42,720
therefore ring negative one must be more

216
00:07:40,720 --> 00:07:45,120
privileged than ring zero

217
00:07:42,720 --> 00:07:47,120
later on the team had found attacks on a

218
00:07:45,120 --> 00:07:49,520
system management mode which they termed

219
00:07:47,120 --> 00:07:50,960
ring negative two because they found

220
00:07:49,520 --> 00:07:53,360
that this was actually even more

221
00:07:50,960 --> 00:07:55,759
powerful than virtualization software it

222
00:07:53,360 --> 00:07:58,080
had full control over all of memory

223
00:07:55,759 --> 00:08:00,160
including the vmm's memory and

224
00:07:58,080 --> 00:08:02,319
furthermore it could not be directly

225
00:08:00,160 --> 00:08:05,280
inspected by the vmwa

226
00:08:02,319 --> 00:08:07,199
also in 2009 they ultimately found an

227
00:08:05,280 --> 00:08:08,720
attack on the intel

228
00:08:07,199 --> 00:08:10,240
what was at the time called management

229
00:08:08,720 --> 00:08:12,160
engine later called the converged

230
00:08:10,240 --> 00:08:14,000
security and management engine it's also

231
00:08:12,160 --> 00:08:15,680
called active management technology that

232
00:08:14,000 --> 00:08:17,039
was just the marketing term

233
00:08:15,680 --> 00:08:19,759
and at the time because of the

234
00:08:17,039 --> 00:08:22,240
architecture it was the case that that

235
00:08:19,759 --> 00:08:24,560
particular location where the code ran

236
00:08:22,240 --> 00:08:27,440
was on a processor that had full memory

237
00:08:24,560 --> 00:08:29,199
access so it could inspect the otherwise

238
00:08:27,440 --> 00:08:31,440
uninspectable memory of system

239
00:08:29,199 --> 00:08:32,880
management mode so these terms ring

240
00:08:31,440 --> 00:08:34,800
negative one ring negative two and ring

241
00:08:32,880 --> 00:08:36,399
negative three came into existence

242
00:08:34,800 --> 00:08:38,560
referring to ever deeper and more

243
00:08:36,399 --> 00:08:40,240
privileged code and so obviously an

244
00:08:38,560 --> 00:08:42,080
attacker would like to go as privileged

245
00:08:40,240 --> 00:08:44,000
as possible

246
00:08:42,080 --> 00:08:45,600
now the situation is more complicated

247
00:08:44,000 --> 00:08:47,920
than that and so i'm not going to

248
00:08:45,600 --> 00:08:49,839
actually use ring levels but just you

249
00:08:47,920 --> 00:08:51,519
know to to give you a sense of you know

250
00:08:49,839 --> 00:08:54,080
how these different privilege levels

251
00:08:51,519 --> 00:08:56,640
build up so of course we have you know

252
00:08:54,080 --> 00:08:58,959
user space and root processes root of

253
00:08:56,640 --> 00:09:01,760
course is expected to be more privileged

254
00:08:58,959 --> 00:09:03,600
than using other user space processes or

255
00:09:01,760 --> 00:09:05,440
admin processes are supposed to be more

256
00:09:03,600 --> 00:09:08,399
privileged then of course you have the

257
00:09:05,440 --> 00:09:10,480
kernel which is more privileged still

258
00:09:08,399 --> 00:09:12,399
and a typical privilege escalation

259
00:09:10,480 --> 00:09:14,320
attack would be trying to find your way

260
00:09:12,399 --> 00:09:16,320
for instance from a non-root process

261
00:09:14,320 --> 00:09:18,720
like a web browser all the way into the

262
00:09:16,320 --> 00:09:21,360
kernel which consequently can you know

263
00:09:18,720 --> 00:09:23,440
manage and compromise all other user

264
00:09:21,360 --> 00:09:24,959
space processes

265
00:09:23,440 --> 00:09:27,120
beyond the kernel then of course there's

266
00:09:24,959 --> 00:09:28,880
virtualization technology which could be

267
00:09:27,120 --> 00:09:31,519
more privileged than the kernel if it's

268
00:09:28,880 --> 00:09:33,600
a bare metal virtual machine otherwise

269
00:09:31,519 --> 00:09:35,920
things that you're usually used to

270
00:09:33,600 --> 00:09:37,360
thinking about such as vmware those are

271
00:09:35,920 --> 00:09:39,440
actually not more privileged than the

272
00:09:37,360 --> 00:09:41,760
kernel those are just run as you know

273
00:09:39,440 --> 00:09:44,480
special combinations of you know kernel

274
00:09:41,760 --> 00:09:46,399
drivers and user space applications

275
00:09:44,480 --> 00:09:47,839
so vmm more privilege but then of course

276
00:09:46,399 --> 00:09:49,920
we have the bootloader which wasn't

277
00:09:47,839 --> 00:09:52,240
talked about in this you know original

278
00:09:49,920 --> 00:09:54,320
rokowska taxonomy and the bootloader

279
00:09:52,240 --> 00:09:57,200
must necessarily load either a bare

280
00:09:54,320 --> 00:09:58,800
metal virtual virtualization system or a

281
00:09:57,200 --> 00:10:01,200
kernel you know if a virtual

282
00:09:58,800 --> 00:10:02,720
immunization system doesn't exist so we

283
00:10:01,200 --> 00:10:05,279
can say that you know infection of a

284
00:10:02,720 --> 00:10:07,200
bootloader would necessarily lead to

285
00:10:05,279 --> 00:10:09,040
more privilege than a virtualization

286
00:10:07,200 --> 00:10:10,399
system

287
00:10:09,040 --> 00:10:11,519
then there's system management mode

288
00:10:10,399 --> 00:10:13,760
which we'll be learning about in this

289
00:10:11,519 --> 00:10:15,760
class but smm gets a little bit

290
00:10:13,760 --> 00:10:17,600
complicated because there is a

291
00:10:15,760 --> 00:10:20,720
technology that is meant to actually

292
00:10:17,600 --> 00:10:22,079
virtualize smm so wrap a virtual machine

293
00:10:20,720 --> 00:10:24,399
around smm

294
00:10:22,079 --> 00:10:26,720
that's called the stm or smi transfer

295
00:10:24,399 --> 00:10:28,560
monitor but in practice that's not

296
00:10:26,720 --> 00:10:29,519
typically used for reasons we'll see

297
00:10:28,560 --> 00:10:32,399
later

298
00:10:29,519 --> 00:10:35,760
now modern systems also introduce the

299
00:10:32,399 --> 00:10:38,160
capability to de-privilege smm and so

300
00:10:35,760 --> 00:10:40,720
between a bios setting up smm and a

301
00:10:38,160 --> 00:10:43,040
special way or new hardware extensions

302
00:10:40,720 --> 00:10:45,120
it may be possible to de-privilege smm

303
00:10:43,040 --> 00:10:47,120
to about the level of a kernel possibly

304
00:10:45,120 --> 00:10:48,800
appear to the kernel or you could even

305
00:10:47,120 --> 00:10:50,959
de-privilege it even further so that

306
00:10:48,800 --> 00:10:52,640
it's less privileged than the kernel in

307
00:10:50,959 --> 00:10:54,399
most regards

308
00:10:52,640 --> 00:10:56,240
so it just kind of depends on how the

309
00:10:54,399 --> 00:10:58,399
particular system is set up

310
00:10:56,240 --> 00:11:01,040
then there's the bios which is the code

311
00:10:58,399 --> 00:11:02,720
that runs first and sets up the smm and

312
00:11:01,040 --> 00:11:05,200
loads the bootloader which loads the

313
00:11:02,720 --> 00:11:07,440
subsequent things

314
00:11:05,200 --> 00:11:09,120
and an extreme privilege escalation

315
00:11:07,440 --> 00:11:11,200
would be to make your way all the way

316
00:11:09,120 --> 00:11:13,279
from you know a root or admin privilege

317
00:11:11,200 --> 00:11:14,720
inside of you know ring three all the

318
00:11:13,279 --> 00:11:17,120
way up to the bios and then you could

319
00:11:14,720 --> 00:11:18,560
control all of these systems thereafter

320
00:11:17,120 --> 00:11:21,680
and this was you know the invisible

321
00:11:18,560 --> 00:11:23,680
things labs work in 2009

322
00:11:21,680 --> 00:11:25,519
and even further invisible things labs

323
00:11:23,680 --> 00:11:28,000
found their way to break from the kernel

324
00:11:25,519 --> 00:11:29,920
level all the way into the management

325
00:11:28,000 --> 00:11:32,079
engine but once we start getting to this

326
00:11:29,920 --> 00:11:34,800
level it turns out that the management

327
00:11:32,079 --> 00:11:36,560
engine is off on its own processor and

328
00:11:34,800 --> 00:11:38,320
we can start to consider other things

329
00:11:36,560 --> 00:11:40,880
that are off on their own processor like

330
00:11:38,320 --> 00:11:43,279
dma capable peripherals so dma direct

331
00:11:40,880 --> 00:11:46,560
memory access these are things typically

332
00:11:43,279 --> 00:11:48,480
out on the pcie bus which are capable of

333
00:11:46,560 --> 00:11:49,680
accessing arbitrary memory if it's not

334
00:11:48,480 --> 00:11:51,440
protected

335
00:11:49,680 --> 00:11:52,399
so we have to we start to get a little

336
00:11:51,440 --> 00:11:54,839
bit

337
00:11:52,399 --> 00:11:56,639
messier once we get down to this lowest

338
00:11:54,839 --> 00:11:59,040
level so

339
00:11:56,639 --> 00:12:00,880
all of these things the bios smm boot

340
00:11:59,040 --> 00:12:03,600
loader etc these are all things that are

341
00:12:00,880 --> 00:12:07,200
executing on the actual cpu itself the

342
00:12:03,600 --> 00:12:09,040
intel cpu the csme and the dma capable

343
00:12:07,200 --> 00:12:12,079
peripherals are off on peripheral

344
00:12:09,040 --> 00:12:13,839
processors so the cpu has mechanisms to

345
00:12:12,079 --> 00:12:15,760
defend itself against peripheral

346
00:12:13,839 --> 00:12:18,639
processors and then it really becomes a

347
00:12:15,760 --> 00:12:20,959
question of is the particular bios smam

348
00:12:18,639 --> 00:12:23,120
bootloader are those things using what's

349
00:12:20,959 --> 00:12:25,120
available in order to defend themselves

350
00:12:23,120 --> 00:12:27,839
or can these external entities just

351
00:12:25,120 --> 00:12:29,839
arbitrarily and always get powered

352
00:12:27,839 --> 00:12:31,200
privileged access over them

353
00:12:29,839 --> 00:12:33,839
one of the most important things to

354
00:12:31,200 --> 00:12:35,839
defend themselves is the intel vtd

355
00:12:33,839 --> 00:12:38,560
technology virtualization technology for

356
00:12:35,839 --> 00:12:40,959
directed i o also frequently known as an

357
00:12:38,560 --> 00:12:42,079
i o memory management unit

358
00:12:40,959 --> 00:12:44,800
and this is the thing that can

359
00:12:42,079 --> 00:12:45,839
potentially defend against a dma capable

360
00:12:44,800 --> 00:12:47,839
peripheral

361
00:12:45,839 --> 00:12:51,120
but it really becomes a question of

362
00:12:47,839 --> 00:12:53,360
what's in use so if vtd is not

363
00:12:51,120 --> 00:12:55,760
protecting the bios then a dma capable

364
00:12:53,360 --> 00:12:57,839
peripheral can potentially infect the

365
00:12:55,760 --> 00:12:59,920
memory of the bios as the system is

366
00:12:57,839 --> 00:13:02,240
booting early in boot

367
00:12:59,920 --> 00:13:04,959
and so some systems you know most

368
00:13:02,240 --> 00:13:08,079
systems don't use vtd at the level of

369
00:13:04,959 --> 00:13:09,440
the bios some systems actually do use it

370
00:13:08,079 --> 00:13:11,760
at the level of protecting a

371
00:13:09,440 --> 00:13:13,279
virtualization system so that malware

372
00:13:11,760 --> 00:13:15,680
can't just dma its way into the

373
00:13:13,279 --> 00:13:18,240
hypervisor and take over that way

374
00:13:15,680 --> 00:13:20,480
and other systems only use it to protect

375
00:13:18,240 --> 00:13:22,720
the kernel itself and so

376
00:13:20,480 --> 00:13:25,279
either way if the you know protection is

377
00:13:22,720 --> 00:13:28,160
down here then the attacker has the

378
00:13:25,279 --> 00:13:32,320
power of the earliest location that they

379
00:13:28,160 --> 00:13:34,320
can infect with this dma type capability

380
00:13:32,320 --> 00:13:36,160
and so this for instance was the case

381
00:13:34,320 --> 00:13:38,240
for mac os

382
00:13:36,160 --> 00:13:40,560
before myself and my colleagues came and

383
00:13:38,240 --> 00:13:43,120
you know pushed the vtd boundary all the

384
00:13:40,560 --> 00:13:44,720
way to cover the bios

385
00:13:43,120 --> 00:13:47,440
and while it would be nice if i could

386
00:13:44,720 --> 00:13:50,320
say that because of intel's vtd and

387
00:13:47,440 --> 00:13:52,720
other technologies that the cpu resident

388
00:13:50,320 --> 00:13:54,079
software and firmware is actually just

389
00:13:52,720 --> 00:13:56,000
always protected against these

390
00:13:54,079 --> 00:13:59,839
peripheral processors things like the

391
00:13:56,000 --> 00:14:02,160
csme ultimately complicate this because

392
00:13:59,839 --> 00:14:04,320
researchers like positive technologies

393
00:14:02,160 --> 00:14:07,199
have found that on some hardware such as

394
00:14:04,320 --> 00:14:09,040
intel atom processors the capability to

395
00:14:07,199 --> 00:14:12,160
enable hardware debugging is

396
00:14:09,040 --> 00:14:15,199
intrinsically linked to the csme so it

397
00:14:12,160 --> 00:14:17,040
can actually just always arbitrarily

398
00:14:15,199 --> 00:14:18,639
take over control of the processor you

399
00:14:17,040 --> 00:14:20,639
know it's a hardware debugger so they

400
00:14:18,639 --> 00:14:22,800
can you know set registers read

401
00:14:20,639 --> 00:14:25,199
registers write registers and ultimately

402
00:14:22,800 --> 00:14:27,120
you know force the cpu to jump to some

403
00:14:25,199 --> 00:14:29,519
attacker controlled code there's also an

404
00:14:27,120 --> 00:14:31,839
open question about whether the csme

405
00:14:29,519 --> 00:14:35,760
just by virtue of the way that intel has

406
00:14:31,839 --> 00:14:37,760
designed it is able to bypass intel vtd

407
00:14:35,760 --> 00:14:39,920
this hasn't been shown yet but it may be

408
00:14:37,760 --> 00:14:41,440
shown very soon in the future

409
00:14:39,920 --> 00:14:43,519
and then while we have this picture up

410
00:14:41,440 --> 00:14:45,600
it's worth mentioning things like

411
00:14:43,519 --> 00:14:47,760
microsoft's approach of trying to use

412
00:14:45,600 --> 00:14:50,320
what they call secure launch where they

413
00:14:47,760 --> 00:14:51,760
use intel vtd to protect a

414
00:14:50,320 --> 00:14:53,760
virtualization system their

415
00:14:51,760 --> 00:14:56,160
virtualization-based security and

416
00:14:53,760 --> 00:14:58,399
they're also starting to use intel txt

417
00:14:56,160 --> 00:15:00,560
trusted execution technology this is a

418
00:14:58,399 --> 00:15:02,320
technology that tries to say i don't

419
00:15:00,560 --> 00:15:04,639
care whether all of this stuff up here

420
00:15:02,320 --> 00:15:07,600
is infected i'm just going to relaunch

421
00:15:04,639 --> 00:15:09,600
my system into a nice clean state with

422
00:15:07,600 --> 00:15:11,120
some measured content which then can

423
00:15:09,600 --> 00:15:12,480
protect things like your full disk

424
00:15:11,120 --> 00:15:14,240
encryption password

425
00:15:12,480 --> 00:15:16,399
so that you can't even get into the hard

426
00:15:14,240 --> 00:15:18,560
drive unless everything is measured and

427
00:15:16,399 --> 00:15:20,959
in the correct configuration

428
00:15:18,560 --> 00:15:23,199
they also you know require biospenders

429
00:15:20,959 --> 00:15:24,959
to use these smm de-privileging

430
00:15:23,199 --> 00:15:27,120
mechanisms

431
00:15:24,959 --> 00:15:30,079
because historically it had been shown

432
00:15:27,120 --> 00:15:31,920
that smm could always defeat this intel

433
00:15:30,079 --> 00:15:33,360
txt

434
00:15:31,920 --> 00:15:35,600
so whether this approach will work or

435
00:15:33,360 --> 00:15:37,920
not will depend on further research in

436
00:15:35,600 --> 00:15:39,600
the areas of these low-level attacks and

437
00:15:37,920 --> 00:15:41,920
seeing whether they can bypass these

438
00:15:39,600 --> 00:15:43,519
intel security technologies

439
00:15:41,920 --> 00:15:46,320
and hopefully in the future we'll be

440
00:15:43,519 --> 00:15:48,800
providing classes on intel vtd and intel

441
00:15:46,320 --> 00:15:50,959
txt in order to make it so that more

442
00:15:48,800 --> 00:15:52,959
people understand how these security

443
00:15:50,959 --> 00:15:54,560
defensive mechanisms work so that they

444
00:15:52,959 --> 00:15:57,040
can apply them to other operating

445
00:15:54,560 --> 00:15:58,720
systems but also so that more people can

446
00:15:57,040 --> 00:16:00,560
do research on whether they actually

447
00:15:58,720 --> 00:16:03,360
work or whether they're bypassable

448
00:16:00,560 --> 00:16:04,639
through some mechanism or another

449
00:16:03,360 --> 00:16:06,639
so i think it's important to make a

450
00:16:04,639 --> 00:16:08,959
distinction between architectural

451
00:16:06,639 --> 00:16:10,959
privilege and implemented privilege

452
00:16:08,959 --> 00:16:12,959
architecturally a hardware maker like

453
00:16:10,959 --> 00:16:15,440
intel might add some defensive

454
00:16:12,959 --> 00:16:18,079
technologies they might add new features

455
00:16:15,440 --> 00:16:20,000
and new hardware but it's ultimately the

456
00:16:18,079 --> 00:16:22,800
responsibility of firmware and software

457
00:16:20,000 --> 00:16:25,440
makers to adopt these features

458
00:16:22,800 --> 00:16:28,160
so while bios makers could use vtd it's

459
00:16:25,440 --> 00:16:29,600
been around for well over a decade most

460
00:16:28,160 --> 00:16:31,680
of them don't

461
00:16:29,600 --> 00:16:34,720
and while operating system makers could

462
00:16:31,680 --> 00:16:35,839
use virtualization in order to partition

463
00:16:34,720 --> 00:16:37,600
some of the more privileged

464
00:16:35,839 --> 00:16:38,639
functionality into a more isolated

465
00:16:37,600 --> 00:16:40,880
location

466
00:16:38,639 --> 00:16:43,040
most of them don't and even those who do

467
00:16:40,880 --> 00:16:45,440
don't have the capability to implement

468
00:16:43,040 --> 00:16:48,000
it all the time on all the hardware and

469
00:16:45,440 --> 00:16:50,240
all the configurations and so architects

470
00:16:48,000 --> 00:16:52,320
are always trying to further partition

471
00:16:50,240 --> 00:16:54,079
in jail and sandbox and isolate

472
00:16:52,320 --> 00:16:56,160
different chunks of privilege on the

473
00:16:54,079 --> 00:16:58,480
system and so that means that you know

474
00:16:56,160 --> 00:17:00,880
rings ultimately are not the best way to

475
00:16:58,480 --> 00:17:03,360
convey what has what capabilities on the

476
00:17:00,880 --> 00:17:05,919
system they're a good shorthand but they

477
00:17:03,360 --> 00:17:07,679
fall very short in terms of actually

478
00:17:05,919 --> 00:17:09,679
providing you a meaningful description

479
00:17:07,679 --> 00:17:11,839
of what can do what and the key point is

480
00:17:09,679 --> 00:17:14,319
that some attackers want the most power

481
00:17:11,839 --> 00:17:15,679
and privilege possible and consequently

482
00:17:14,319 --> 00:17:17,679
they're always going to be digging

483
00:17:15,679 --> 00:17:20,079
deeper and deeper into the system where

484
00:17:17,679 --> 00:17:23,240
most people don't look in order to find

485
00:17:20,079 --> 00:17:23,240
that power

