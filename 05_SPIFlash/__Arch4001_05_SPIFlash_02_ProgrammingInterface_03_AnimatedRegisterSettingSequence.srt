1
00:00:00,160 --> 00:00:04,720
all right so let's put it all together

2
00:00:01,760 --> 00:00:06,080
in animation form enter an address to

3
00:00:04,720 --> 00:00:08,960
read

4
00:00:06,080 --> 00:00:12,320
f adder equals zero so that would be

5
00:00:08,960 --> 00:00:15,120
written to the f adder at offset eight

6
00:00:12,320 --> 00:00:17,440
enter a size to read

7
00:00:15,120 --> 00:00:21,119
hardware sequencing flash status

8
00:00:17,440 --> 00:00:23,519
register flash data byte count equals

9
00:00:21,119 --> 00:00:25,599
let's say hex 10 so it's going to read

10
00:00:23,519 --> 00:00:28,240
16 bytes starting at flash linear

11
00:00:25,599 --> 00:00:31,039
address 0.

12
00:00:28,240 --> 00:00:32,960
there you go write that in set the flash

13
00:00:31,039 --> 00:00:36,239
cycle type to read

14
00:00:32,960 --> 00:00:40,399
hardware sequencing flash status dot f

15
00:00:36,239 --> 00:00:43,040
cycle equals zero for a read operation

16
00:00:40,399 --> 00:00:46,000
check that no one's using it already

17
00:00:43,040 --> 00:00:49,280
while hardware sequencing flash control

18
00:00:46,000 --> 00:00:52,000
dot cycle in progress is equal to one

19
00:00:49,280 --> 00:00:54,480
read the hardware sequencing flash

20
00:00:52,000 --> 00:00:56,480
control register and check whether cycle

21
00:00:54,480 --> 00:00:57,920
in progress is still equal to one so

22
00:00:56,480 --> 00:00:59,840
just keep looping back and forth

23
00:00:57,920 --> 00:01:03,039
checking if it's one

24
00:00:59,840 --> 00:01:04,479
and if not say go for the flash cycle to

25
00:01:03,039 --> 00:01:06,479
go

26
00:01:04,479 --> 00:01:09,360
hardware sequencing flash status

27
00:01:06,479 --> 00:01:13,119
register fgo equals one

28
00:01:09,360 --> 00:01:14,880
and then boom some magic happens

29
00:01:13,119 --> 00:01:17,040
now you have to wait until the status

30
00:01:14,880 --> 00:01:19,200
says it's done and you do that by

31
00:01:17,040 --> 00:01:22,240
checking hardware sequencing flash

32
00:01:19,200 --> 00:01:25,280
status register f done if it's not equal

33
00:01:22,240 --> 00:01:28,240
to one then you need to keep waiting and

34
00:01:25,280 --> 00:01:30,960
checking and waiting and checking

35
00:01:28,240 --> 00:01:33,119
finally once it says it's done then have

36
00:01:30,960 --> 00:01:36,079
your fun and check the flash data

37
00:01:33,119 --> 00:01:38,000
registers read from flash data zero for

38
00:01:36,079 --> 00:01:39,520
the first four bytes flash data one for

39
00:01:38,000 --> 00:01:42,079
the next four bytes

40
00:01:39,520 --> 00:01:44,720
because we asked for 16 bytes so we're

41
00:01:42,079 --> 00:01:47,119
going to read flash data 0 through 3 in

42
00:01:44,720 --> 00:01:49,040
order to get those 16 bytes of data back

43
00:01:47,119 --> 00:01:50,960
and there you go we successfully

44
00:01:49,040 --> 00:01:53,520
interacted with the spy flash chip

45
00:01:50,960 --> 00:01:56,880
through the spy bar memory mapped i o

46
00:01:53,520 --> 00:01:58,880
registers via the unseen spy hardware

47
00:01:56,880 --> 00:02:00,880
component that's embedded somewhere deep

48
00:01:58,880 --> 00:02:02,880
inside the pch

49
00:02:00,880 --> 00:02:05,520
and with that we're done we have just

50
00:02:02,880 --> 00:02:06,960
learned about how spybar memory mapped i

51
00:02:05,520 --> 00:02:08,879
o works and how there's a bunch of

52
00:02:06,960 --> 00:02:11,599
registers there that can be manipulated

53
00:02:08,879 --> 00:02:13,680
in order to cause flash cycles to happen

54
00:02:11,599 --> 00:02:16,000
and we learned about flash writing we

55
00:02:13,680 --> 00:02:18,560
saw exactly which of those registers we

56
00:02:16,000 --> 00:02:20,879
had to set

57
00:02:18,560 --> 00:02:23,760
and we saw the spy config address space

58
00:02:20,879 --> 00:02:25,920
for those of you who are running pch 100

59
00:02:23,760 --> 00:02:28,959
series and newer that is how you find

60
00:02:25,920 --> 00:02:30,879
spybar rather than the rickerbah plus

61
00:02:28,959 --> 00:02:33,200
hex 3800

62
00:02:30,879 --> 00:02:34,959
now we're not actually really done done

63
00:02:33,200 --> 00:02:36,560
there's going to be one last place where

64
00:02:34,959 --> 00:02:38,879
the dram controller shows up in the

65
00:02:36,560 --> 00:02:41,360
context of smm but we're done enough for

66
00:02:38,879 --> 00:02:41,360
now

