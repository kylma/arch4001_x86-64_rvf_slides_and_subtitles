1
00:00:00,04 --> 00:00:02,65
So let's talk more about those regions that the flash

2
00:00:02,65 --> 00:00:04,06
descriptor describes

3
00:00:04,64 --> 00:00:07,19
So region zero is the flash descriptor itself

4
00:00:07,19 --> 00:00:07,8
Region one,

5
00:00:07,8 --> 00:00:10,43
the bios to the intel management engine etcetera

6
00:00:10,78 --> 00:00:13,93
Now these region numbers do not correspond to what order

7
00:00:13,93 --> 00:00:16,67
they have to take inside of the spi flash chip

8
00:00:16,69 --> 00:00:19,02
It's just the order that intel added them

9
00:00:19,21 --> 00:00:21,3
And there's also more bits available so that they can

10
00:00:21,3 --> 00:00:22,44
add more things in the future

11
00:00:22,44 --> 00:00:25,35
And like I said before Platform data region didn't exist

12
00:00:25,36 --> 00:00:26,14
prior to the i

13
00:00:26,14 --> 00:00:26,35
c

14
00:00:26,35 --> 00:00:26,54
h

15
00:00:26,54 --> 00:00:29,99
nine And the embedded controller region didn't exist prior to

16
00:00:29,99 --> 00:00:31,46
the 100 series chip set

17
00:00:32,44 --> 00:00:32,69
Now,

18
00:00:32,69 --> 00:00:36,88
previously the description of how exactly the flash descriptor was

19
00:00:36,88 --> 00:00:38,78
laid out was provided in I

20
00:00:38,78 --> 00:00:38,9
O

21
00:00:38,9 --> 00:00:40,4
Controller hub data sheets

22
00:00:40,55 --> 00:00:44,78
But unfortunately over time those have moved into the spy

23
00:00:44,78 --> 00:00:47,16
programming guides for a given chunk of hardware

24
00:00:47,34 --> 00:00:50,65
So those are named like the Sky Lakes by programming

25
00:00:50,65 --> 00:00:54,4
God has well spy programming guide but unfortunately those documents

26
00:00:54,4 --> 00:00:57,76
are intel confidential so the information still exists out there

27
00:00:57,76 --> 00:00:59,27
And if you want to sign an N

28
00:00:59,27 --> 00:00:59,42
D

29
00:00:59,42 --> 00:00:59,59
A,

30
00:00:59,59 --> 00:01:01,2
you can go get that and read that

31
00:01:01,21 --> 00:01:02,89
Or it turns out that you know,

32
00:01:02,89 --> 00:01:05,25
of course over time these sort of documents leak out

33
00:01:05,25 --> 00:01:07,28
onto the internet and then you can just read it

34
00:01:07,28 --> 00:01:07,86
there too

35
00:01:07,94 --> 00:01:10,65
Nothing about the particulars of the security provided by this

36
00:01:10,65 --> 00:01:13,06
mechanism has anything to do with secrecy

37
00:01:13,14 --> 00:01:16,59
I think it's just the typical until thing that documentation

38
00:01:16,59 --> 00:01:20,55
is proprietary and India until absolutely necessary

39
00:01:20,94 --> 00:01:23,32
So throughout the next sections we're going to be using

40
00:01:23,32 --> 00:01:23,64
the I

41
00:01:23,64 --> 00:01:23,84
C

42
00:01:23,84 --> 00:01:24,04
H

43
00:01:24,04 --> 00:01:27,63
10 last sort of public documentation of how these work

44
00:01:27,64 --> 00:01:30,61
and it's accurate enough for our purposes of understanding how

45
00:01:30,61 --> 00:01:31,49
these are laid out

46
00:01:31,54 --> 00:01:33,62
There was a brief reference to this before when I

47
00:01:33,62 --> 00:01:36,25
showed you a picture of four chunks of memory,

48
00:01:36,25 --> 00:01:37,65
all showing the same information

49
00:01:37,94 --> 00:01:40,77
And that is the fact that at the base of

50
00:01:40,77 --> 00:01:42,84
the data structure is a signature

51
00:01:42,84 --> 00:01:45,21
It's not a digital signature in the security sense,

52
00:01:45,44 --> 00:01:47,98
it's just a bite signature in the sense of the

53
00:01:47,98 --> 00:01:50,28
hardware is going to read the base and it's going

54
00:01:50,28 --> 00:01:51,88
to see is it this value?

55
00:01:51,88 --> 00:01:52,56
If it is,

56
00:01:52,74 --> 00:01:54,79
it expects that this is going to be running into

57
00:01:54,79 --> 00:01:57,42
scripture mode and the hardware reads and parses all this

58
00:01:57,42 --> 00:02:00,18
various stuff and if that signature is invalid,

59
00:02:00,18 --> 00:02:03,06
then it'll fall back to non descriptor mode on those

60
00:02:03,06 --> 00:02:05,53
things that support it and otherwise it's just not gonna

61
00:02:05,53 --> 00:02:05,86
work

62
00:02:06,24 --> 00:02:06,98
Additionally,

63
00:02:06,98 --> 00:02:10,06
the start of this entry has actually moved to offset

64
00:02:10,07 --> 00:02:13,5
10 in the flash chip rather than offset zero on

65
00:02:13,5 --> 00:02:14,25
future hardware

66
00:02:14,84 --> 00:02:17,44
So this is a little bit about the evolution of

67
00:02:17,44 --> 00:02:19,57
the thing because we can still find this sort of

68
00:02:19,57 --> 00:02:21,15
picture in some of the data sheets,

69
00:02:21,15 --> 00:02:23,45
just not the description of the internal registers

70
00:02:23,84 --> 00:02:26,19
So if the I C H looked like this and

71
00:02:26,19 --> 00:02:26,75
it had,

72
00:02:26,79 --> 00:02:27,39
you know,

73
00:02:27,39 --> 00:02:31,37
this uppermost portion called 256 bytes of om section

74
00:02:31,52 --> 00:02:31,84
Well,

75
00:02:31,84 --> 00:02:34,55
the subsequent documentation just called it the om section

76
00:02:34,55 --> 00:02:36,07
But this is just me letting you know,

77
00:02:36,07 --> 00:02:37,85
It's still 256 bites

78
00:02:38,53 --> 00:02:41,43
There was a portion called the VSc See table and

79
00:02:41,43 --> 00:02:44,33
that has changed being named the management engine vsc see

80
00:02:44,33 --> 00:02:44,76
table

81
00:02:45,34 --> 00:02:46,67
And then like I said,

82
00:02:46,68 --> 00:02:50,78
the signature has moved to offset 10 instead of offset

83
00:02:50,78 --> 00:02:52,26
zero where it used to exist

84
00:02:53,02 --> 00:02:53,53
Furthermore,

85
00:02:53,53 --> 00:02:56,89
I briefly mentioned the concept of soft straps before and

86
00:02:56,89 --> 00:02:59,76
how they are a way to reconfigure the hardware without

87
00:02:59,76 --> 00:03:02,59
having to actually pull some particular pin up to a

88
00:03:02,59 --> 00:03:04,67
high voltage or down to a low voltage

89
00:03:04,73 --> 00:03:07,65
And so this M C H I C H straps

90
00:03:07,65 --> 00:03:10,25
that existed in the North Bridge Southbridge sort of world

91
00:03:10,44 --> 00:03:14,16
has turned into the PCH straps on PCH systems

92
00:03:14,94 --> 00:03:18,44
Despite the fact that we don't have the detailed description

93
00:03:18,44 --> 00:03:20,57
of these data structures available in the public

94
00:03:20,58 --> 00:03:22,54
There is a little bit of elf magic that you

95
00:03:22,54 --> 00:03:23,29
should know about

96
00:03:23,34 --> 00:03:25,56
So we said that the P C H R I

97
00:03:25,56 --> 00:03:28,86
C H hardware actually reads and parses this data structure

98
00:03:29,14 --> 00:03:31,94
and so it will actually take some of these elements

99
00:03:31,94 --> 00:03:35,06
out of these fields and map them into memory at

100
00:03:35,06 --> 00:03:37,8
various registers so we can inspect them there instead

101
00:03:38,34 --> 00:03:41,1
And those registers are generally documented in the data sheets

102
00:03:41,2 --> 00:03:42,03
So,

103
00:03:42,04 --> 00:03:43,44
elf magic boot time,

104
00:03:43,44 --> 00:03:46,75
the hardware reads from the flash chip at offset zero

105
00:03:46,75 --> 00:03:49,82
parses the data structures and it maps some of those

106
00:03:49,82 --> 00:03:51,96
into the Spy bear Spy bar region

107
00:03:52,34 --> 00:03:54,04
Similarly on the newer hardware

108
00:03:54,04 --> 00:03:56,17
Same thing into the spy bar region

109
00:03:56,21 --> 00:03:59,73
So the FL rig and F rig are two things

110
00:03:59,73 --> 00:04:02,85
we would see references to specifically the F rig is

111
00:04:02,85 --> 00:04:04,75
the memory mapped copy of this

112
00:04:04,75 --> 00:04:07,02
This is going to be documented in the data sheets

113
00:04:07,26 --> 00:04:09,05
somewhere in the Spy bar region

114
00:04:09,14 --> 00:04:10,76
And it'll have things like,

115
00:04:10,76 --> 00:04:10,94
you know,

116
00:04:10,94 --> 00:04:12,56
the region base in the region limit

117
00:04:12,74 --> 00:04:16,58
And these values are pulled from the flash descriptor on

118
00:04:16,58 --> 00:04:17,55
the flash chip

119
00:04:17,56 --> 00:04:19,41
They are read only as it says right here,

120
00:04:19,41 --> 00:04:19,9
read only,

121
00:04:19,9 --> 00:04:21,91
read only and as it says right here,

122
00:04:21,91 --> 00:04:23,97
the value in this register is loaded from the contents

123
00:04:23,97 --> 00:04:25,19
in the flash descriptor,

124
00:04:25,2 --> 00:04:29,15
FL regs zero region dot region base

125
00:04:29,44 --> 00:04:32,42
So the basic point is the hardware pulls values out

126
00:04:32,42 --> 00:04:34,13
of this and maps them into memory

127
00:04:34,13 --> 00:04:36,42
So if you really need to see some of these

128
00:04:36,42 --> 00:04:37,75
they are exposed in memory

