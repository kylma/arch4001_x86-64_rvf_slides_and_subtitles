1
00:00:00,160 --> 00:00:04,560
now there's another form of smi

2
00:00:02,080 --> 00:00:08,080
suppression that involves some registers

3
00:00:04,560 --> 00:00:11,120
called tco for total cost of ownership

4
00:00:08,080 --> 00:00:13,280
uh registers and so typically tco stuff

5
00:00:11,120 --> 00:00:14,639
was used for a bunch of miscellaneous

6
00:00:13,280 --> 00:00:17,440
functionality

7
00:00:14,639 --> 00:00:20,080
around things like uh detection of

8
00:00:17,440 --> 00:00:22,640
intrusion into a chassis of a you know

9
00:00:20,080 --> 00:00:24,480
desktop system things like that cause a

10
00:00:22,640 --> 00:00:26,320
system management interrupt that smi

11
00:00:24,480 --> 00:00:29,119
code could say oh i see someone broke in

12
00:00:26,320 --> 00:00:32,000
you know or wipe the secrets whatever

13
00:00:29,119 --> 00:00:34,160
and so basically there's a way to set

14
00:00:32,000 --> 00:00:36,239
and enable here to zero and similar to

15
00:00:34,160 --> 00:00:38,399
setting this enable to zero that will

16
00:00:36,239 --> 00:00:41,040
cause suppression of

17
00:00:38,399 --> 00:00:43,040
tco sms but it turns out that the

18
00:00:41,040 --> 00:00:44,960
hardware that's used for tco stuff is

19
00:00:43,040 --> 00:00:46,640
also used for this

20
00:00:44,960 --> 00:00:49,120
bios lock enable

21
00:00:46,640 --> 00:00:51,120
so there is also a tco lock that goes

22
00:00:49,120 --> 00:00:52,879
along with that similar to this smi lock

23
00:00:51,120 --> 00:00:54,719
and that is how this can be prevented

24
00:00:52,879 --> 00:00:56,480
from being written to xero and then

25
00:00:54,719 --> 00:00:58,000
while we're here we'll just say that you

26
00:00:56,480 --> 00:01:01,199
could also defend this with intel

27
00:00:58,000 --> 00:01:02,719
bioscard or smm bwp as with this other

28
00:01:01,199 --> 00:01:04,720
suppression attack

29
00:01:02,719 --> 00:01:07,280
so this was just mentioned offhandedly

30
00:01:04,720 --> 00:01:09,600
by some intel researchers when they were

31
00:01:07,280 --> 00:01:11,280
doing a survey paper at defcon to talk

32
00:01:09,600 --> 00:01:13,280
about the different types of attacks

33
00:01:11,280 --> 00:01:15,119
that were known at the time so they

34
00:01:13,280 --> 00:01:17,360
referenced this you know charizard

35
00:01:15,119 --> 00:01:19,600
attack from our work and then they said

36
00:01:17,360 --> 00:01:23,360
by the way there's another variant which

37
00:01:19,600 --> 00:01:26,080
is if you disable tco smi sources

38
00:01:23,360 --> 00:01:29,119
specifically by clearing smi enable tco

39
00:01:26,080 --> 00:01:31,200
enable then it won't actually lock the

40
00:01:29,119 --> 00:01:33,360
bios lock enables the bios lock enable

41
00:01:31,200 --> 00:01:35,600
will not effectively work so i was kind

42
00:01:33,360 --> 00:01:38,159
of curious about this before the class

43
00:01:35,600 --> 00:01:40,479
and so i pinged yuri belijan on twitter

44
00:01:38,159 --> 00:01:41,840
to ask him to really confirm that this

45
00:01:40,479 --> 00:01:44,079
was true and he said yes this is

46
00:01:41,840 --> 00:01:46,720
definitely true so i went off and tried

47
00:01:44,079 --> 00:01:48,079
it on one of these dell optiplex 7010s

48
00:01:46,720 --> 00:01:50,320
that we've been referencing through the

49
00:01:48,079 --> 00:01:52,560
class and it turns out it had exactly

50
00:01:50,320 --> 00:01:54,880
the configuration that i needed because

51
00:01:52,560 --> 00:01:56,960
it was defended against this because it

52
00:01:54,880 --> 00:01:58,960
had the smi lock set so you couldn't use

53
00:01:56,960 --> 00:02:00,560
this attack but it was not defended

54
00:01:58,960 --> 00:02:02,479
against this because it didn't have the

55
00:02:00,560 --> 00:02:04,880
tco lockset

56
00:02:02,479 --> 00:02:08,080
so these are the relevant registers so

57
00:02:04,880 --> 00:02:09,599
smi enable as before tco enable is just

58
00:02:08,080 --> 00:02:12,400
a thing that if set to zero means

59
00:02:09,599 --> 00:02:14,160
there's no tco smis and it just turns

60
00:02:12,400 --> 00:02:16,160
out that those you know despite not

61
00:02:14,160 --> 00:02:18,640
really saying it anywhere

62
00:02:16,160 --> 00:02:22,560
yuri basically confirmed that the little

63
00:02:18,640 --> 00:02:26,879
hardware block inside of the pch that

64
00:02:22,560 --> 00:02:28,720
is used for tcio tco events also is the

65
00:02:26,879 --> 00:02:30,800
same hardware block that's used for smis

66
00:02:28,720 --> 00:02:32,720
related to bios right protection

67
00:02:30,800 --> 00:02:35,280
so tco enable an attacker would want to

68
00:02:32,720 --> 00:02:37,840
set that to zero and then the smis would

69
00:02:35,280 --> 00:02:39,599
not fire and that can be prevented by

70
00:02:37,840 --> 00:02:43,800
setting tco lock

71
00:02:39,599 --> 00:02:43,800
over in some other register

