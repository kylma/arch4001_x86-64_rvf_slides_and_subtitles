1
00:00:00,04 --> 00:00:00,4
All right

2
00:00:00,4 --> 00:00:02,61
So what does it actually mean for peripheral to be

3
00:00:02,61 --> 00:00:03,56
memory mapped?

4
00:00:03,57 --> 00:00:05,67
I think the term can be a little bit confusing

5
00:00:05,67 --> 00:00:07,57
Certainly when I was first starting out,

6
00:00:07,58 --> 00:00:08,65
I thought for instance,

7
00:00:08,65 --> 00:00:09,64
it meant that like a D

8
00:00:09,64 --> 00:00:11,72
M A capable peripheral with like D

9
00:00:11,72 --> 00:00:11,88
M

10
00:00:11,88 --> 00:00:12,3
A

11
00:00:12,3 --> 00:00:13,75
Its contents to RAM

12
00:00:13,75 --> 00:00:15,8
And then they would be like back and forth,

13
00:00:15,81 --> 00:00:17,05
manipulating RAM

14
00:00:17,05 --> 00:00:19,35
And then the D M A contents would be pulled

15
00:00:19,35 --> 00:00:19,66
back

16
00:00:19,84 --> 00:00:21,25
But that's not actually the case

17
00:00:21,55 --> 00:00:24,56
The way you should think about a memory mapped peripheral

18
00:00:24,69 --> 00:00:27,99
is that when the memory controller would otherwise be going

19
00:00:27,99 --> 00:00:28,72
to RAM,

20
00:00:28,81 --> 00:00:32,23
it sees that this should instead be destined for some

21
00:00:32,23 --> 00:00:33,27
particular peripheral

22
00:00:33,44 --> 00:00:37,49
It hands it to the peripheral via whatever peripheral access

23
00:00:37,49 --> 00:00:41,33
mechanism exists on the system and then the peripheral decides

24
00:00:41,33 --> 00:00:42,46
where it goes from there

25
00:00:42,54 --> 00:00:44,06
So we'll see some examples of that

26
00:00:44,44 --> 00:00:45,55
So in the first case,

27
00:00:45,56 --> 00:00:47,88
let's imagine that we're talking about non memory mapped I

28
00:00:47,88 --> 00:00:50,64
o here we have a couple of assembly instructions,

29
00:00:50,64 --> 00:00:52,21
it's taking the address,

30
00:00:52,21 --> 00:00:52,85
foot,

31
00:00:52,86 --> 00:00:54,0
putting it into R E X

32
00:00:54,0 --> 00:00:57,42
And then reading from RAM at that address into register

33
00:00:57,42 --> 00:01:00,45
VL So if we follow our little bouncing ball here

34
00:01:00,46 --> 00:01:03,14
it would make a stop at the registers to look

35
00:01:03,14 --> 00:01:05,66
up that this R E X corresponds to address,

36
00:01:05,66 --> 00:01:06,15
foot,

37
00:01:06,54 --> 00:01:09,64
instruction decoder would pull out that kind of information handed

38
00:01:09,64 --> 00:01:10,77
to the memory controller

39
00:01:10,78 --> 00:01:12,85
And then the memory controller would say,

40
00:01:12,85 --> 00:01:13,27
okay,

41
00:01:13,27 --> 00:01:15,51
well the instruction decoder tells me there's some sort of

42
00:01:15,51 --> 00:01:18,39
memory access here and it's trying to access address,

43
00:01:18,39 --> 00:01:18,96
foot,

44
00:01:19,54 --> 00:01:21,24
what do I actually do with that?

45
00:01:21,47 --> 00:01:21,7
Well,

46
00:01:21,7 --> 00:01:24,24
the memory controller has to keep a giant look up

47
00:01:24,24 --> 00:01:27,57
table of all sorts of different things that have to

48
00:01:27,57 --> 00:01:29,26
do with different memory ranges

49
00:01:29,27 --> 00:01:31,61
And so it could be the case that it's in

50
00:01:31,61 --> 00:01:33,53
this range and then it goes to RAM or it's

51
00:01:33,53 --> 00:01:35,91
in this range and goes to a Pc express border

52
00:01:35,91 --> 00:01:38,26
this range and it goes to LPC or Spy

53
00:01:38,64 --> 00:01:42,25
And so the memory controller is responsible for looking up

54
00:01:42,26 --> 00:01:46,42
information about where a particular address should actually be mapped

55
00:01:46,43 --> 00:01:46,91
And again,

56
00:01:46,91 --> 00:01:48,76
this is the bio says job to set up a

57
00:01:48,76 --> 00:01:51,25
whole bunch of configuration in order to say,

58
00:01:51,25 --> 00:01:51,49
you know,

59
00:01:51,49 --> 00:01:54,6
this range right here should actually be mapped to that

60
00:01:54,6 --> 00:01:55,26
peripheral

61
00:01:55,36 --> 00:01:58,4
That range there should be mapped to that peripheral configures

62
00:01:58,4 --> 00:02:00,92
all a bunch of internal registers of the CPU and

63
00:02:00,92 --> 00:02:03,73
memory controller and then the memory controller uses it for

64
00:02:03,73 --> 00:02:04,76
look up in the future

65
00:02:05,24 --> 00:02:07,76
So let's consider the if it's not me

66
00:02:07,76 --> 00:02:07,88
Oh,

67
00:02:07,88 --> 00:02:11,36
case in that case are bouncing ball is going to

68
00:02:11,36 --> 00:02:14,51
follow this path here and the memory controller is going

69
00:02:14,51 --> 00:02:17,05
to send the request out to D RAM,

70
00:02:17,06 --> 00:02:20,78
the proper memory and then it will get back the

71
00:02:20,78 --> 00:02:23,33
memory and stick it into the register bl as you

72
00:02:23,33 --> 00:02:24,06
would expect

73
00:02:24,64 --> 00:02:24,84
Now,

74
00:02:24,84 --> 00:02:28,38
let's consider the case where the address corresponds to a

75
00:02:28,38 --> 00:02:31,96
pc device and therefore it's a memory map dial

76
00:02:32,34 --> 00:02:34,34
So I just randomly picked this address,

77
00:02:34,34 --> 00:02:38,21
let's say that this address corresponds to a gigabit ethernet

78
00:02:38,21 --> 00:02:41,22
card that addresses once again put into a register and

79
00:02:41,22 --> 00:02:44,76
then the assembly reads from memory memory

80
00:02:45,24 --> 00:02:48,95
And that memory is actually going to be routed down

81
00:02:48,95 --> 00:02:50,51
to the Pc IE device

82
00:02:50,52 --> 00:02:53,56
So here the bouncing ball goes down to the PC

83
00:02:53,56 --> 00:02:54,35
hardware,

84
00:02:54,36 --> 00:02:57,28
it's going to go across a DMI link down to

85
00:02:57,28 --> 00:02:58,5
the PCH

86
00:02:58,51 --> 00:03:01,19
PCH is going to send it over to the PC

87
00:03:01,19 --> 00:03:04,53
I network card and then once it reaches the card

88
00:03:04,59 --> 00:03:05,65
that card is going to say,

89
00:03:05,65 --> 00:03:05,96
okay,

90
00:03:05,96 --> 00:03:06,17
well,

91
00:03:06,17 --> 00:03:07,89
I know that they were actually accessing,

92
00:03:07,89 --> 00:03:08,15
you know,

93
00:03:08,15 --> 00:03:13,16
this offset offset F 007 within some particular range that

94
00:03:13,16 --> 00:03:14,15
was assigned to me

95
00:03:14,16 --> 00:03:17,22
And then that particular hardware makes the decision to say

96
00:03:17,22 --> 00:03:20,81
that offset corresponds to some registers corresponds to some memory

97
00:03:20,82 --> 00:03:22,25
whatever it corresponds to

98
00:03:22,25 --> 00:03:24,86
It could be some other non volatile memory attached to

99
00:03:24,86 --> 00:03:26,17
the Pc network hard,

100
00:03:26,18 --> 00:03:27,46
whatever it corresponds to,

101
00:03:27,46 --> 00:03:30,22
that's up to the chunk of hardware to decide and

102
00:03:30,22 --> 00:03:31,71
then it chooses goes out,

103
00:03:31,72 --> 00:03:34,13
grabs the information that was being requested,

104
00:03:34,42 --> 00:03:36,53
sends it back via the PC I E bus,

105
00:03:36,7 --> 00:03:40,03
goes back all the way up to the memory ultimately

106
00:03:40,06 --> 00:03:41,6
being fed into the register

107
00:03:41,75 --> 00:03:43,89
And from your perspective on the CPU well,

108
00:03:43,89 --> 00:03:46,11
it just looks like any other memory access

109
00:03:46,11 --> 00:03:46,42
Right?

110
00:03:46,43 --> 00:03:48,02
And that's what memory mapped

111
00:03:48,03 --> 00:03:51,87
I o means you look like you're accessing memory but

112
00:03:51,87 --> 00:03:53,2
in reality you're accessing,

113
00:03:53,2 --> 00:03:54,39
doing iO input,

114
00:03:54,39 --> 00:03:56,41
output to some peripheral device

115
00:03:57,44 --> 00:03:58,65
And then one last example,

116
00:03:58,65 --> 00:04:00,62
let's say it was the spy device which we've seen

117
00:04:00,62 --> 00:04:01,01
before,

118
00:04:01,01 --> 00:04:02,92
the bios flash chip?

119
00:04:03,05 --> 00:04:03,43
Well,

120
00:04:03,43 --> 00:04:04,53
this particular range,

121
00:04:04,54 --> 00:04:08,49
f f f f F 007 is going to correspond

122
00:04:08,49 --> 00:04:11,34
to the spi flash chip because we already said that

123
00:04:11,34 --> 00:04:13,9
particular ranges always map to the spi flash and there's

124
00:04:13,9 --> 00:04:15,16
nothing you can do about it

125
00:04:15,64 --> 00:04:18,64
And so the memory controller again sends it down via

126
00:04:18,64 --> 00:04:20,47
DM me to the PCH

127
00:04:20,48 --> 00:04:22,03
And then it's going to say,

128
00:04:22,03 --> 00:04:25,17
well this is actually destined for the spy hardware

129
00:04:25,26 --> 00:04:28,12
Spy hardware will use the actual spy protocol to go

130
00:04:28,12 --> 00:04:31,07
talk to a spi flash chip and then it's going

131
00:04:31,07 --> 00:04:31,88
to again decide,

132
00:04:31,88 --> 00:04:32,06
you know,

133
00:04:32,06 --> 00:04:32,64
what is this?

134
00:04:32,64 --> 00:04:33,02
Well,

135
00:04:33,03 --> 00:04:34,75
it's going to be non volatile storage

136
00:04:34,94 --> 00:04:36,76
It's going to fetch some value,

137
00:04:36,92 --> 00:04:39,47
read it back and ultimately it gets put into the

138
00:04:39,47 --> 00:04:40,04
registers,

139
00:04:40,04 --> 00:04:41,44
like any other memory read

140
00:04:42,44 --> 00:04:44,01
So from the Cpus perspective,

141
00:04:44,01 --> 00:04:46,69
what does it look like look like to access this

142
00:04:46,7 --> 00:04:47,24
Well,

143
00:04:47,24 --> 00:04:48,27
it just looks like,

144
00:04:48,27 --> 00:04:48,49
you know,

145
00:04:48,49 --> 00:04:52,53
the memory controller fetches from memory and then through some

146
00:04:52,53 --> 00:04:56,13
sort of magic it just access is a spy flash

147
00:04:56,13 --> 00:04:56,6
chip

148
00:04:56,61 --> 00:04:59,16
And on you go get the memory,

149
00:04:59,17 --> 00:04:59,97
pull it back,

150
00:04:59,98 --> 00:05:01,42
stick it into the registers

151
00:05:01,43 --> 00:05:01,96
Right?

152
00:05:02,14 --> 00:05:05,2
So that's the magic of mm I o So now

153
00:05:05,2 --> 00:05:08,84
let's talk about what happens when you make some slides

154
00:05:08,84 --> 00:05:10,46
and you stare at them for too long

155
00:05:10,47 --> 00:05:12,45
So I've been staring at this slide for way too

156
00:05:12,45 --> 00:05:12,87
long

157
00:05:12,89 --> 00:05:15,25
And so I'm looking at the CPU picture and I'm

158
00:05:15,25 --> 00:05:15,64
saying,

159
00:05:15,64 --> 00:05:15,99
you know,

160
00:05:16,0 --> 00:05:17,56
you know what that looks like to me,

161
00:05:17,94 --> 00:05:19,3
What do you think it looks like to me?

162
00:05:19,3 --> 00:05:22,07
What does it look like to you now?

163
00:05:22,08 --> 00:05:24,56
If you say it doesn't look like anything at all

164
00:05:24,94 --> 00:05:26,65
I want to get yourself checked out

165
00:05:27,34 --> 00:05:29,9
But if you say,

166
00:05:29,93 --> 00:05:32,61
I think that looks like a ninja turtle and proper

167
00:05:32,61 --> 00:05:33,63
stealth attire,

168
00:05:33,64 --> 00:05:35,98
then congratulations your creative mind,

169
00:05:35,98 --> 00:05:38,27
the likes of which the world has never known you're

170
00:05:38,27 --> 00:05:39,36
going to do Okay,

171
00:05:40,44 --> 00:05:40,84
All right,

172
00:05:40,84 --> 00:05:42,6
well that's all we need to know for now about

173
00:05:42,6 --> 00:05:43,19
memory maps,

174
00:05:43,19 --> 00:05:46,35
IO and memory map configuration by the bios

175
00:05:46,94 --> 00:05:49,4
We will come back to the memory map configuration a

176
00:05:49,4 --> 00:05:51,21
little bit and feeling some of the bits that I

177
00:05:51,21 --> 00:05:52,77
actually kind of skipped over here

178
00:05:52,77 --> 00:05:57,12
The tollan toad tom there's a whole bunch of acronyms

179
00:05:57,12 --> 00:05:59,46
and abbreviations that have to do with the memory met

180
00:05:59,47 --> 00:06:02,6
but thankfully we don't really need to know them right

181
00:06:02,6 --> 00:06:04,55
now to try to get where we're trying to go

182
00:06:04,64 --> 00:06:06,21
which is the flash right protection,

183
00:06:06,21 --> 00:06:08,58
but it'll come back into play again once we talk

184
00:06:08,58 --> 00:06:10,86
about as I'm RAM later and a little bit in

185
00:06:10,86 --> 00:06:13,71
the context of pC configuration address space

