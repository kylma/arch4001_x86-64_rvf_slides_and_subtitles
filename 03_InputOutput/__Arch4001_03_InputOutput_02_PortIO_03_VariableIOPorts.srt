1
00:00:00,160 --> 00:00:04,880
in contrast to fixed i o ports variable

2
00:00:02,639 --> 00:00:07,680
i o ports of course can be moved around

3
00:00:04,880 --> 00:00:08,559
the i o address space to any particular

4
00:00:07,680 --> 00:00:11,280
port

5
00:00:08,559 --> 00:00:13,519
number that is not actually in use

6
00:00:11,280 --> 00:00:15,360
there are some types of variable port

7
00:00:13,519 --> 00:00:18,160
ranges which are actually just built

8
00:00:15,360 --> 00:00:19,840
into intel hardware which are required

9
00:00:18,160 --> 00:00:21,840
by certain specifications and things

10
00:00:19,840 --> 00:00:24,720
like that but there's also the entire

11
00:00:21,840 --> 00:00:26,960
class of variable i o ports that are

12
00:00:24,720 --> 00:00:29,519
used by pcie devices

13
00:00:26,960 --> 00:00:31,599
so a particular peripheral such as a

14
00:00:29,519 --> 00:00:33,680
network card graphics card anything like

15
00:00:31,599 --> 00:00:35,840
that the software that interacts with

16
00:00:33,680 --> 00:00:38,079
that card could choose to use a

17
00:00:35,840 --> 00:00:40,879
mechanism called base address registers

18
00:00:38,079 --> 00:00:43,600
and pcie in order to set up the access

19
00:00:40,879 --> 00:00:45,200
ports for that particular device by

20
00:00:43,600 --> 00:00:47,120
locating them somewhere in the i o

21
00:00:45,200 --> 00:00:49,440
address space we'll see an example of

22
00:00:47,120 --> 00:00:51,360
this later on so the bios or other

23
00:00:49,440 --> 00:00:54,399
privileged software such as an operating

24
00:00:51,360 --> 00:00:56,800
system kernel drivers or the like can go

25
00:00:54,399 --> 00:01:00,000
ahead and move around these variable

26
00:00:56,800 --> 00:01:01,840
port i o ports to make it so that a

27
00:01:00,000 --> 00:01:04,000
particular peripheral is accessible at

28
00:01:01,840 --> 00:01:06,960
some particular port just like with

29
00:01:04,000 --> 00:01:09,200
fixed ports the other side of a port

30
00:01:06,960 --> 00:01:10,880
access is just some black box particular

31
00:01:09,200 --> 00:01:14,000
peripheral that behaves however it

32
00:01:10,880 --> 00:01:16,799
behaves and again determining what it is

33
00:01:14,000 --> 00:01:19,040
and how it behaves is a tricky situation

34
00:01:16,799 --> 00:01:21,040
but in the case of variable i o ports

35
00:01:19,040 --> 00:01:22,960
there's an extra consideration which is

36
00:01:21,040 --> 00:01:25,200
the fact that you can potentially have

37
00:01:22,960 --> 00:01:27,439
these things overlapped the hardware

38
00:01:25,200 --> 00:01:29,759
will not actually check for overlapped

39
00:01:27,439 --> 00:01:31,840
ranges and this has been used for

40
00:01:29,759 --> 00:01:33,759
attacks in the context of things like

41
00:01:31,840 --> 00:01:36,000
virtualization systems

42
00:01:33,759 --> 00:01:38,640
which peripheral gets the data when two

43
00:01:36,000 --> 00:01:41,520
ports are actually overlapped in their

44
00:01:38,640 --> 00:01:43,680
port i o range is typically undefined

45
00:01:41,520 --> 00:01:45,600
and that's what can lead to unexpected

46
00:01:43,680 --> 00:01:47,920
behaviors that can give an attacker an

47
00:01:45,600 --> 00:01:49,920
advantage so here's an example of some

48
00:01:47,920 --> 00:01:53,119
of the variable i o ranges which are

49
00:01:49,920 --> 00:01:54,880
just built into the 7 series pch

50
00:01:53,119 --> 00:01:56,719
and the first one is actually the one

51
00:01:54,880 --> 00:01:57,759
that we're going to see the most in this

52
00:01:56,719 --> 00:02:00,399
class

53
00:01:57,759 --> 00:02:02,880
acpi is the advanced configuration and

54
00:02:00,399 --> 00:02:05,520
power interface and it's a separate

55
00:02:02,880 --> 00:02:08,000
standard which intel conforms to

56
00:02:05,520 --> 00:02:10,560
which it has to do with power management

57
00:02:08,000 --> 00:02:13,040
and it specifies that there should be 64

58
00:02:10,560 --> 00:02:14,640
bytes that are usable for this power

59
00:02:13,040 --> 00:02:16,800
management interface

60
00:02:14,640 --> 00:02:19,120
so we will come to know that particular

61
00:02:16,800 --> 00:02:21,520
variable address range later on as the

62
00:02:19,120 --> 00:02:23,760
pm base power management base

63
00:02:21,520 --> 00:02:25,280
variable i o range

64
00:02:23,760 --> 00:02:28,560
but there are other things in here

65
00:02:25,280 --> 00:02:30,080
having to do with ide buses sata buses

66
00:02:28,560 --> 00:02:33,360
sm bus

67
00:02:30,080 --> 00:02:36,480
serial ports and pci bridges or more

68
00:02:33,360 --> 00:02:38,879
generally access to the pci root ports

69
00:02:36,480 --> 00:02:41,040
and so all of these are things where

70
00:02:38,879 --> 00:02:42,800
some number of bytes can be rearranged

71
00:02:41,040 --> 00:02:44,959
inside of the port i o address space in

72
00:02:42,800 --> 00:02:47,280
order to access some particular type of

73
00:02:44,959 --> 00:02:49,519
peripheral so the takeaway for variable

74
00:02:47,280 --> 00:02:52,800
i o ports is that again because there's

75
00:02:49,519 --> 00:02:55,360
a very limited range of i o ports total

76
00:02:52,800 --> 00:02:57,840
available intel doesn't recommend that

77
00:02:55,360 --> 00:03:00,640
people actually use port io they can if

78
00:02:57,840 --> 00:03:02,319
they want but it's not recommended

79
00:03:00,640 --> 00:03:04,720
some of the things like what we saw in

80
00:03:02,319 --> 00:03:07,120
the previous table are actually required

81
00:03:04,720 --> 00:03:09,280
and they're just typically implemented

82
00:03:07,120 --> 00:03:11,360
as registers somewhere inside of the

83
00:03:09,280 --> 00:03:12,959
intel hardware that something like the

84
00:03:11,360 --> 00:03:15,120
bios or the operating system would

85
00:03:12,959 --> 00:03:17,599
configure in order to set a particular

86
00:03:15,120 --> 00:03:19,840
port in order to access some particular

87
00:03:17,599 --> 00:03:22,159
configuration registers and because

88
00:03:19,840 --> 00:03:24,239
hardware and very frequently software

89
00:03:22,159 --> 00:03:26,879
such as virtualization software doesn't

90
00:03:24,239 --> 00:03:29,200
actually check for port overlap due to

91
00:03:26,879 --> 00:03:31,360
misconfiguration this can lead to very

92
00:03:29,200 --> 00:03:34,720
strange behavior which sometimes is to

93
00:03:31,360 --> 00:03:34,720
an attacker's advantage

