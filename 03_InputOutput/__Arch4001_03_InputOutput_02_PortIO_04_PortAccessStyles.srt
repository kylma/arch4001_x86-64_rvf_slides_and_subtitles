1
00:00:00,080 --> 00:00:04,240
now i want to talk about the two funky

2
00:00:02,159 --> 00:00:05,759
fresh port io styles which you may

3
00:00:04,240 --> 00:00:06,960
encounter when you're looking at some

4
00:00:05,759 --> 00:00:09,519
bios code

5
00:00:06,960 --> 00:00:12,480
there is the address data style and

6
00:00:09,519 --> 00:00:14,000
there is the poke peak style which i

7
00:00:12,480 --> 00:00:15,679
specifically differentiate from the

8
00:00:14,000 --> 00:00:18,160
normal peak poke

9
00:00:15,679 --> 00:00:20,400
now address data style is the type that

10
00:00:18,160 --> 00:00:22,560
you saw in architecture 2001 when we

11
00:00:20,400 --> 00:00:24,480
were dealing with the cmos

12
00:00:22,560 --> 00:00:26,640
this is the case where you've got the

13
00:00:24,480 --> 00:00:29,199
black box which is port i o peripheral

14
00:00:26,640 --> 00:00:31,599
over there and there's one port that is

15
00:00:29,199 --> 00:00:34,000
used to specify an address somewhere

16
00:00:31,599 --> 00:00:36,000
inside of that black box essentially

17
00:00:34,000 --> 00:00:38,399
treating it as if it was some sort of

18
00:00:36,000 --> 00:00:39,680
array or some sort of you know list of

19
00:00:38,399 --> 00:00:41,840
registers

20
00:00:39,680 --> 00:00:44,559
one port specifies an address and the

21
00:00:41,840 --> 00:00:47,360
next port specifies the data data either

22
00:00:44,559 --> 00:00:49,600
being read from or written to the

23
00:00:47,360 --> 00:00:51,840
particular address that was specified

24
00:00:49,600 --> 00:00:54,239
so in the case of cmos we saw how the

25
00:00:51,840 --> 00:00:56,719
first addresses were just used for the

26
00:00:54,239 --> 00:00:58,559
real-time clock bytes and then addresses

27
00:00:56,719 --> 00:01:00,320
after that were the actual non-volatile

28
00:00:58,559 --> 00:01:03,039
storage of cmos

29
00:01:00,320 --> 00:01:04,879
but this particular style address data

30
00:01:03,039 --> 00:01:06,720
is going to be the one that we're going

31
00:01:04,879 --> 00:01:09,600
to see repeatedly throughout the rest of

32
00:01:06,720 --> 00:01:12,560
the class in the context of port i o

33
00:01:09,600 --> 00:01:14,640
access to pcie devices

34
00:01:12,560 --> 00:01:16,720
now the other one i call poke peak

35
00:01:14,640 --> 00:01:17,759
because more often than not especially

36
00:01:16,720 --> 00:01:19,520
when you're

37
00:01:17,759 --> 00:01:20,720
talking about the context of a bios

38
00:01:19,520 --> 00:01:22,560
where everything has just been

39
00:01:20,720 --> 00:01:25,040
reinitialized and everything is clean

40
00:01:22,560 --> 00:01:27,520
start most often you're going to have

41
00:01:25,040 --> 00:01:29,680
the cpu with the bios doing some sort of

42
00:01:27,520 --> 00:01:32,240
poke to the black box sending a command

43
00:01:29,680 --> 00:01:34,799
to the black box and then reading back

44
00:01:32,240 --> 00:01:36,640
data from that particular peripheral now

45
00:01:34,799 --> 00:01:38,799
of course you could read it first you

46
00:01:36,640 --> 00:01:40,400
could have a peak which happens to just

47
00:01:38,799 --> 00:01:42,799
you know be checking the default

48
00:01:40,400 --> 00:01:44,479
initialized status of a black box but

49
00:01:42,799 --> 00:01:46,799
the software that knows how to deal with

50
00:01:44,479 --> 00:01:49,439
the black box probably knows what its

51
00:01:46,799 --> 00:01:51,520
default initialized power reset state is

52
00:01:49,439 --> 00:01:54,000
and so they probably usually just skip

53
00:01:51,520 --> 00:01:56,240
right to pokenit instead of peaking it

54
00:01:54,000 --> 00:01:58,479
in the poke peak paradigm usually it is

55
00:01:56,240 --> 00:02:01,600
a single port and it may be eight bits

56
00:01:58,479 --> 00:02:04,079
it may be 32 bits but it's a single port

57
00:02:01,600 --> 00:02:06,320
where both of the wrights and the reeds

58
00:02:04,079 --> 00:02:08,399
are accessing the same port it's not a

59
00:02:06,320 --> 00:02:13,120
two port pair like in the address data

60
00:02:08,399 --> 00:02:15,280
style so in the simmix debug 1015 class

61
00:02:13,120 --> 00:02:17,920
it was shown how to feed a different

62
00:02:15,280 --> 00:02:19,440
bios image into simix and be able to see

63
00:02:17,920 --> 00:02:21,520
the assembly and step through it if

64
00:02:19,440 --> 00:02:23,440
you'd like now for those of you who

65
00:02:21,520 --> 00:02:24,840
happen to be on your way towards the

66
00:02:23,440 --> 00:02:28,319
architecture

67
00:02:24,840 --> 00:02:30,319
4031 class on the core boot you were

68
00:02:28,319 --> 00:02:33,760
recommended to purchase something like

69
00:02:30,319 --> 00:02:35,440
an optiplex 710 or 9010

70
00:02:33,760 --> 00:02:37,519
and if you were to throw that image in

71
00:02:35,440 --> 00:02:39,280
there you would see something like this

72
00:02:37,519 --> 00:02:40,800
and this is just within the first few

73
00:02:39,280 --> 00:02:42,959
dozen assembly instructions that

74
00:02:40,800 --> 00:02:45,200
actually execute after it gets itself

75
00:02:42,959 --> 00:02:46,720
out of real mode so if we take a closer

76
00:02:45,200 --> 00:02:49,760
look at this we can actually see some

77
00:02:46,720 --> 00:02:51,519
patterns we see a bunch of out assembly

78
00:02:49,760 --> 00:02:54,080
instructions here that are all just

79
00:02:51,519 --> 00:02:55,040
writing data out to some particular port

80
00:02:54,080 --> 00:02:57,440
there's no

81
00:02:55,040 --> 00:03:01,280
port in here it's all out so some sort

82
00:02:57,440 --> 00:03:03,920
of initialization writing you know 4

83
00:03:01,280 --> 00:03:07,440
out to port 128 writing f's out to port

84
00:03:03,920 --> 00:03:09,360
33 237 161 etc

85
00:03:07,440 --> 00:03:11,040
so that's basically just poking a bunch

86
00:03:09,360 --> 00:03:13,760
of different black boxes that we don't

87
00:03:11,040 --> 00:03:17,200
necessarily know what they are

88
00:03:13,760 --> 00:03:20,560
there's also some address data accesses

89
00:03:17,200 --> 00:03:24,560
here this is going to be taking a port

90
00:03:20,560 --> 00:03:27,920
so cf8 into dx and then writing some

91
00:03:24,560 --> 00:03:31,920
value into that into eax and then eax

92
00:03:27,920 --> 00:03:32,799
out to cf8 so basically cf8 there f8

93
00:03:31,920 --> 00:03:35,360
here

94
00:03:32,799 --> 00:03:38,400
f8 there this is always just setting an

95
00:03:35,360 --> 00:03:40,560
address into the dx register to be used

96
00:03:38,400 --> 00:03:42,959
as the out address

97
00:03:40,560 --> 00:03:44,560
and then we see the corresponding data

98
00:03:42,959 --> 00:03:46,799
here so

99
00:03:44,560 --> 00:03:49,120
this is setting an address and this is

100
00:03:46,799 --> 00:03:52,480
writing data out to that address so

101
00:03:49,120 --> 00:03:54,480
specifically fed1c001

102
00:03:52,480 --> 00:03:58,159
is written out here to

103
00:03:54,480 --> 00:04:01,120
port cfc so this is already has dx

104
00:03:58,159 --> 00:04:03,360
already has 8 in it and so this is only

105
00:04:01,120 --> 00:04:04,959
writing to dl the least significant byte

106
00:04:03,360 --> 00:04:06,080
so it's still c

107
00:04:04,959 --> 00:04:09,760
fc

108
00:04:06,080 --> 00:04:11,360
so cfc is the port in dx right here and

109
00:04:09,760 --> 00:04:13,439
this is the data that is written out to

110
00:04:11,360 --> 00:04:15,519
that port then it changes back to the

111
00:04:13,439 --> 00:04:19,199
other port the address port so this

112
00:04:15,519 --> 00:04:22,800
becomes cf8 then it writes some address

113
00:04:19,199 --> 00:04:24,800
to that and then it changes back to cfc

114
00:04:22,800 --> 00:04:27,360
and then it reads in some data from

115
00:04:24,800 --> 00:04:29,759
there it does an or on it in order to

116
00:04:27,360 --> 00:04:31,919
set a bit and then it writes that value

117
00:04:29,759 --> 00:04:33,040
back out to the same address that was

118
00:04:31,919 --> 00:04:36,080
already there

119
00:04:33,040 --> 00:04:38,960
so that's an example of just some poke

120
00:04:36,080 --> 00:04:41,120
port io and some address data port io

121
00:04:38,960 --> 00:04:44,479
all happening in the same general

122
00:04:41,120 --> 00:04:47,759
location the spoiler here is that cf-8

123
00:04:44,479 --> 00:04:49,680
and cfc are going to be the pcie ports

124
00:04:47,759 --> 00:04:51,840
which we said we care a lot about

125
00:04:49,680 --> 00:04:53,759
because here you go in the first few

126
00:04:51,840 --> 00:04:56,880
dozen assembly instructions we see

127
00:04:53,759 --> 00:04:58,400
access to the pcie address space

128
00:04:56,880 --> 00:05:00,479
then i had a bunch of other

129
00:04:58,400 --> 00:05:02,320
miscellaneous bio stumps just laying

130
00:05:00,479 --> 00:05:04,880
around on my computer and i was too lazy

131
00:05:02,320 --> 00:05:07,360
to go get some new ones so here's an old

132
00:05:04,880 --> 00:05:09,600
macbook pro that i was using whenever i

133
00:05:07,360 --> 00:05:11,600
was doing the initial apple research

134
00:05:09,600 --> 00:05:13,600
back in 2015

135
00:05:11,600 --> 00:05:16,880
and so let's look at its assembly at the

136
00:05:13,600 --> 00:05:19,440
beginning we can see a poke out to port

137
00:05:16,880 --> 00:05:21,520
128 there and then we can see some

138
00:05:19,440 --> 00:05:24,960
address data access here

139
00:05:21,520 --> 00:05:28,479
address data oop go back that was also

140
00:05:24,960 --> 00:05:30,560
cf 8 cfc and so we know again that this

141
00:05:28,479 --> 00:05:32,800
is going to be pcie configuration

142
00:05:30,560 --> 00:05:34,639
address space accesses in there and then

143
00:05:32,800 --> 00:05:37,039
just some miscellaneous assembly between

144
00:05:34,639 --> 00:05:38,960
them also you see some early read and

145
00:05:37,039 --> 00:05:40,320
writes to model specific registers

146
00:05:38,960 --> 00:05:43,280
another thing that we learned about in

147
00:05:40,320 --> 00:05:45,199
architecture 2001 and then i had a hp

148
00:05:43,280 --> 00:05:46,960
elitebook again just one of my demo

149
00:05:45,199 --> 00:05:48,800
machines from back in the day just had

150
00:05:46,960 --> 00:05:51,759
the dump laying around so threw it into

151
00:05:48,800 --> 00:05:55,039
simmix and we see a bunch of pokes here

152
00:05:51,759 --> 00:05:58,639
we got two going out to port 128 3 going

153
00:05:55,039 --> 00:06:01,199
up to port 128 4 5 6 all this going up

154
00:05:58,639 --> 00:06:03,360
to port 128 whatever that is and at

155
00:06:01,199 --> 00:06:04,960
least in this first few assembly

156
00:06:03,360 --> 00:06:06,720
instructions we don't see any pci

157
00:06:04,960 --> 00:06:08,160
configured address space access but you

158
00:06:06,720 --> 00:06:10,160
know walk a little further you'd

159
00:06:08,160 --> 00:06:11,680
probably find some

160
00:06:10,160 --> 00:06:14,000
and if you were to take a look at the

161
00:06:11,680 --> 00:06:16,800
default simix code that comes with it

162
00:06:14,000 --> 00:06:18,639
you would again see some address and

163
00:06:16,800 --> 00:06:20,560
data access within the first few

164
00:06:18,639 --> 00:06:22,720
assembly instructions

165
00:06:20,560 --> 00:06:24,720
so some inferences we can take away from

166
00:06:22,720 --> 00:06:26,960
that just quick sampling of whatever

167
00:06:24,720 --> 00:06:29,919
biases i had laying around first thing

168
00:06:26,960 --> 00:06:31,680
is that port 128 must be sort of

169
00:06:29,919 --> 00:06:34,000
important because a bunch of the

170
00:06:31,680 --> 00:06:36,479
different bioses actually access that

171
00:06:34,000 --> 00:06:38,639
and indeed that is important because it

172
00:06:36,479 --> 00:06:42,479
is what is typically used for the bios

173
00:06:38,639 --> 00:06:45,120
power on self test or post output code

174
00:06:42,479 --> 00:06:46,800
and so that was traditionally a bios

175
00:06:45,120 --> 00:06:49,120
developer having you know very poor

176
00:06:46,800 --> 00:06:52,080
development tools they would rely on

177
00:06:49,120 --> 00:06:54,400
things like writing out numbers as they

178
00:06:52,080 --> 00:06:56,479
walk through the code in order to have a

179
00:06:54,400 --> 00:06:57,639
sense of if the bios crashed after

180
00:06:56,479 --> 00:07:00,639
number

181
00:06:57,639 --> 00:07:04,720
537 then they would know that okay well

182
00:07:00,639 --> 00:07:06,400
i got to 537 and i crashed between 537

183
00:07:04,720 --> 00:07:08,240
and 538

184
00:07:06,400 --> 00:07:10,639
so this was used for basic you know

185
00:07:08,240 --> 00:07:13,919
essentially printf style debugging of

186
00:07:10,639 --> 00:07:16,560
what was going on with the bios

187
00:07:13,919 --> 00:07:18,560
and then port cf-8 and cfc must be

188
00:07:16,560 --> 00:07:20,880
important because we see those a lot and

189
00:07:18,560 --> 00:07:24,000
indeed like i said those are the pcie

190
00:07:20,880 --> 00:07:26,800
port i o ports and they have to be used

191
00:07:24,000 --> 00:07:28,960
before memory mapped i o is available in

192
00:07:26,800 --> 00:07:31,199
order to configure things such as the

193
00:07:28,960 --> 00:07:33,520
memory mapped i o itself so there's a

194
00:07:31,199 --> 00:07:35,120
way to access pcie devices through

195
00:07:33,520 --> 00:07:36,560
memory mapped i o

196
00:07:35,120 --> 00:07:38,240
but you can't do that until you

197
00:07:36,560 --> 00:07:40,080
configure the memory mapped i o and

198
00:07:38,240 --> 00:07:42,160
therefore you have to do that through

199
00:07:40,080 --> 00:07:44,400
port i o to start with

200
00:07:42,160 --> 00:07:46,560
and because like we said back in the

201
00:07:44,400 --> 00:07:48,000
chipset architecture and evolution thing

202
00:07:46,560 --> 00:07:49,840
we said you know there's particular

203
00:07:48,000 --> 00:07:52,080
devices we care about like the dram

204
00:07:49,840 --> 00:07:54,879
controller and the lpc device but we

205
00:07:52,080 --> 00:07:58,080
said that the pcie is the view that the

206
00:07:54,879 --> 00:08:01,599
cpu actually has into the internal guts

207
00:07:58,080 --> 00:08:04,000
of the cpu or the pch therefore the bios

208
00:08:01,599 --> 00:08:06,400
actually has to access these various

209
00:08:04,000 --> 00:08:10,080
configuration registers in the pch for

210
00:08:06,400 --> 00:08:12,240
instance via this port i o ports

211
00:08:10,080 --> 00:08:14,080
so what would one do if one wanted to

212
00:08:12,240 --> 00:08:18,319
identify these various ports you know

213
00:08:14,080 --> 00:08:19,680
port 128 port 237 161. you know how

214
00:08:18,319 --> 00:08:21,360
would you figure that out and

215
00:08:19,680 --> 00:08:24,240
unfortunately i don't have a better

216
00:08:21,360 --> 00:08:26,240
answer for you than googling you have to

217
00:08:24,240 --> 00:08:28,639
basically just search around try to find

218
00:08:26,240 --> 00:08:30,240
documentation you can of course check

219
00:08:28,639 --> 00:08:32,959
and you should check your local

220
00:08:30,240 --> 00:08:34,959
documentation your local data sheets but

221
00:08:32,959 --> 00:08:36,640
more often than not you're not going to

222
00:08:34,959 --> 00:08:37,760
find exactly what you're looking for

223
00:08:36,640 --> 00:08:39,919
there

224
00:08:37,760 --> 00:08:42,560
also because you know now that variable

225
00:08:39,919 --> 00:08:44,399
port i o ranges are a thing you should

226
00:08:42,560 --> 00:08:46,399
check whether or not there was anything

227
00:08:44,399 --> 00:08:49,600
in the assembly that relocated a

228
00:08:46,399 --> 00:08:52,000
particular variable port range to the

229
00:08:49,600 --> 00:08:53,680
ports that are in question now of course

230
00:08:52,000 --> 00:08:55,200
it's hard to find that unless you read

231
00:08:53,680 --> 00:08:57,519
all of the assembly starting from the

232
00:08:55,200 --> 00:09:00,080
very beginning or you know search for

233
00:08:57,519 --> 00:09:02,320
something approximating the base of a

234
00:09:00,080 --> 00:09:04,160
variable port i o range

235
00:09:02,320 --> 00:09:06,720
there are some other things such as

236
00:09:04,160 --> 00:09:09,760
emulation materials that will frequently

237
00:09:06,720 --> 00:09:13,120
list ports those are the very very old

238
00:09:09,760 --> 00:09:14,560
circa 1994 definition of ports and while

239
00:09:13,120 --> 00:09:15,839
some of those may actually still be

240
00:09:14,560 --> 00:09:18,000
accurate because of backwards

241
00:09:15,839 --> 00:09:20,399
compatibility and various hardware

242
00:09:18,000 --> 00:09:22,399
getting accumulated into other hardware

243
00:09:20,399 --> 00:09:23,680
that's not always going to be useful

244
00:09:22,399 --> 00:09:25,760
either

245
00:09:23,680 --> 00:09:28,959
all right so that concludes our

246
00:09:25,760 --> 00:09:30,800
recovering of port io and like we said

247
00:09:28,959 --> 00:09:32,240
we mostly care about memory mapped i o

248
00:09:30,800 --> 00:09:34,560
and port i o because we're trying to get

249
00:09:32,240 --> 00:09:38,160
to pci configuration address space and

250
00:09:34,560 --> 00:09:40,399
we just learned that ports cf8 and cfc

251
00:09:38,160 --> 00:09:42,720
are the initial way that you have to use

252
00:09:40,399 --> 00:09:44,320
to get to pci configuration address

253
00:09:42,720 --> 00:09:46,959
space which we're going to learn about

254
00:09:44,320 --> 00:09:46,959
next

