1
00:00:00,320 --> 00:00:04,000
so what does the processor state look

2
00:00:01,920 --> 00:00:06,080
like after reset according to the manual

3
00:00:04,000 --> 00:00:08,559
well you can go look it up and see all

4
00:00:06,080 --> 00:00:10,400
this and look at all of this nice

5
00:00:08,559 --> 00:00:12,880
information about the various register

6
00:00:10,400 --> 00:00:14,639
states and that's all good and well what

7
00:00:12,880 --> 00:00:16,240
does it look like according to simmix

8
00:00:14,639 --> 00:00:18,000
well you should have seen that you know

9
00:00:16,240 --> 00:00:21,359
most of these registers are initialized

10
00:00:18,000 --> 00:00:24,800
to 0 edx being the parent exception eip

11
00:00:21,359 --> 00:00:26,160
is set to ffff0 and e flags is mostly 0

12
00:00:24,800 --> 00:00:29,119
as well

13
00:00:26,160 --> 00:00:32,239
now the specific values are 0s in all of

14
00:00:29,119 --> 00:00:34,160
the registers except edx edx will

15
00:00:32,239 --> 00:00:36,880
contain the information returned by the

16
00:00:34,160 --> 00:00:40,079
cpoid instruction if you were to pass

17
00:00:36,880 --> 00:00:41,680
one for the parameter to it in eax what

18
00:00:40,079 --> 00:00:43,840
this is actually going to be is

19
00:00:41,680 --> 00:00:46,320
information from a feature table that

20
00:00:43,840 --> 00:00:48,640
you can look up in the cpu id

21
00:00:46,320 --> 00:00:50,079
instruction area of the manual so that

22
00:00:48,640 --> 00:00:51,920
tells about you know different things

23
00:00:50,079 --> 00:00:53,920
does this particular processor support

24
00:00:51,920 --> 00:00:55,600
physical address extensions and

25
00:00:53,920 --> 00:00:57,840
conditional move instructions and a

26
00:00:55,600 --> 00:01:00,160
bunch of different feature extensions

27
00:00:57,840 --> 00:01:02,399
that's so that the bios can know what is

28
00:01:00,160 --> 00:01:04,479
supported and what is not

29
00:01:02,399 --> 00:01:06,799
now what's really interesting is what's

30
00:01:04,479 --> 00:01:08,000
going on with the segment registers and

31
00:01:06,799 --> 00:01:09,600
stuff like that

32
00:01:08,000 --> 00:01:12,400
so you can see that most of the segment

33
00:01:09,600 --> 00:01:14,640
registers are set to zeros but cs is set

34
00:01:12,400 --> 00:01:17,840
to f000

35
00:01:14,640 --> 00:01:20,240
and ip since this is a 16 bit execution

36
00:01:17,840 --> 00:01:22,080
mode like although this says eax blah

37
00:01:20,240 --> 00:01:24,320
blah blah these are all actually you

38
00:01:22,080 --> 00:01:28,400
know when you're truly in 16 bit mode it

39
00:01:24,320 --> 00:01:30,640
starts with you know ax bx ex bp etc

40
00:01:28,400 --> 00:01:32,799
although there is a instruction override

41
00:01:30,640 --> 00:01:35,360
that can allow 16-bit mode code to

42
00:01:32,799 --> 00:01:37,200
access 32-bit registers on systems that

43
00:01:35,360 --> 00:01:40,640
actually support 32-bits not like the

44
00:01:37,200 --> 00:01:44,720
original processors but anyways ip is 16

45
00:01:40,640 --> 00:01:47,040
bits and it's set to ffff0 and cs and ip

46
00:01:44,720 --> 00:01:50,159
so the full logical address remember

47
00:01:47,040 --> 00:01:53,040
architecture 2001 logical addresses are

48
00:01:50,159 --> 00:01:54,880
a segment selector and a

49
00:01:53,040 --> 00:01:56,560
regular register or a you know

50
00:01:54,880 --> 00:01:58,719
hard-coded value

51
00:01:56,560 --> 00:01:59,600
32-bit value or in this case a 16-bit

52
00:01:58,719 --> 00:02:03,840
value

53
00:01:59,600 --> 00:02:06,799
so that is f f-f-zero and why is it f f

54
00:02:03,840 --> 00:02:08,959
f-zero as opposed to f-zero zero zero f

55
00:02:06,799 --> 00:02:11,039
fff0 we'll talk about that in a little

56
00:02:08,959 --> 00:02:13,120
bit when we talk about the segmentation

57
00:02:11,039 --> 00:02:15,440
that you know from

58
00:02:13,120 --> 00:02:17,920
32 and 64-bit systems from architecture

59
00:02:15,440 --> 00:02:20,319
2001 segmentation actually works a bit

60
00:02:17,920 --> 00:02:22,640
different in the original 16-bit

61
00:02:20,319 --> 00:02:24,800
execution mode and the other thing is e

62
00:02:22,640 --> 00:02:28,160
flags being set to 2 because there's

63
00:02:24,800 --> 00:02:30,239
just the bit 1 is always hard coded to 1

64
00:02:28,160 --> 00:02:33,040
in modern systems

65
00:02:30,239 --> 00:02:34,319
so looking at the segment register

66
00:02:33,040 --> 00:02:35,920
information you can see some of the

67
00:02:34,319 --> 00:02:40,239
stuff we would expect from architecture

68
00:02:35,920 --> 00:02:44,959
2001 idtr the idt base register gdtr

69
00:02:40,239 --> 00:02:46,959
based register then we've got the essfs

70
00:02:44,959 --> 00:02:48,480
task register etc a whole bunch of

71
00:02:46,959 --> 00:02:50,480
segment stuff that should look familiar

72
00:02:48,480 --> 00:02:52,239
from architecture 2001

73
00:02:50,480 --> 00:02:54,480
but the really interesting thing about

74
00:02:52,239 --> 00:02:56,879
it is that simmix is actually exposing

75
00:02:54,480 --> 00:02:59,440
to us this stuff that we said was secret

76
00:02:56,879 --> 00:03:01,280
and hidden shadow register state from

77
00:02:59,440 --> 00:03:03,360
architecture 2001. so we said the

78
00:03:01,280 --> 00:03:05,360
manuals may say segment registers have a

79
00:03:03,360 --> 00:03:07,360
visible part and a hidden part where the

80
00:03:05,360 --> 00:03:08,879
hidden part includes base address limit

81
00:03:07,360 --> 00:03:10,720
access information

82
00:03:08,879 --> 00:03:12,560
and then we you know talked about how in

83
00:03:10,720 --> 00:03:14,879
64-bit mode

84
00:03:12,560 --> 00:03:17,200
because segmentation is not exactly used

85
00:03:14,879 --> 00:03:19,360
the same way in 64-bit as 32-bit these

86
00:03:17,200 --> 00:03:22,360
you know hidden portions had hard-coded

87
00:03:19,360 --> 00:03:24,159
bases of 0 and hard-coded limits of

88
00:03:22,360 --> 00:03:26,080
2-64-1

89
00:03:24,159 --> 00:03:28,239
now that is actually being shown to us

90
00:03:26,080 --> 00:03:30,480
explicitly inside of symmics so the

91
00:03:28,239 --> 00:03:32,640
shadow portion is this portion right

92
00:03:30,480 --> 00:03:35,120
here the segment selector may be these

93
00:03:32,640 --> 00:03:36,959
16 bits but then here is the shadow

94
00:03:35,120 --> 00:03:39,440
portion that we don't normally have

95
00:03:36,959 --> 00:03:41,599
visibility into on a normal you know

96
00:03:39,440 --> 00:03:42,959
debugger or normal executing system but

97
00:03:41,599 --> 00:03:45,200
because symmics is not a normal

98
00:03:42,959 --> 00:03:47,280
executing system it's a simulator of how

99
00:03:45,200 --> 00:03:49,200
an intel system should actually work

100
00:03:47,280 --> 00:03:51,120
they just expose this information for us

101
00:03:49,200 --> 00:03:53,760
to easily see

102
00:03:51,120 --> 00:03:55,519
now there's one particular bit in that

103
00:03:53,760 --> 00:03:59,599
is very interesting here and that is the

104
00:03:55,519 --> 00:04:01,680
cs so we said that's f00 but the base in

105
00:03:59,599 --> 00:04:04,319
particular is interesting the reset

106
00:04:01,680 --> 00:04:07,680
state of the base the secret hidden base

107
00:04:04,319 --> 00:04:11,040
portion of the cs register is actually

108
00:04:07,680 --> 00:04:13,360
set to fff 000

109
00:04:11,040 --> 00:04:14,959
so that means that when the base is

110
00:04:13,360 --> 00:04:16,959
added to an instruction pointer you're

111
00:04:14,959 --> 00:04:19,040
going to be getting something in the ff

112
00:04:16,959 --> 00:04:20,720
range why would the base be added to an

113
00:04:19,040 --> 00:04:22,240
instruction pointer because another

114
00:04:20,720 --> 00:04:24,479
little miscellaneous thing that was just

115
00:04:22,240 --> 00:04:27,759
thrown in there in architecture 2001 was

116
00:04:24,479 --> 00:04:29,680
the implicit use of segment registers

117
00:04:27,759 --> 00:04:32,000
so we said whenever

118
00:04:29,680 --> 00:04:34,160
an instruction is actually being

119
00:04:32,000 --> 00:04:36,639
executed by the processor behind the

120
00:04:34,160 --> 00:04:39,120
scenes it's always implicitly using the

121
00:04:36,639 --> 00:04:41,120
cs register so we learned about explicit

122
00:04:39,120 --> 00:04:44,160
usage but we also learned that there is

123
00:04:41,120 --> 00:04:46,080
implicit usage so implicit stack usage

124
00:04:44,160 --> 00:04:47,759
implicit data usage and in this

125
00:04:46,080 --> 00:04:51,680
particular case what we care about is

126
00:04:47,759 --> 00:04:54,080
the implicit code segment register usage

127
00:04:51,680 --> 00:04:56,560
so it says all instruction fetches are

128
00:04:54,080 --> 00:04:58,639
secretly using the cs information

129
00:04:56,560 --> 00:04:59,600
behind the scenes to construct a logical

130
00:04:58,639 --> 00:05:00,800
address

131
00:04:59,600 --> 00:05:02,240
for where it should be getting you know

132
00:05:00,800 --> 00:05:04,960
code from

133
00:05:02,240 --> 00:05:08,160
so that means that when you're actually

134
00:05:04,960 --> 00:05:11,840
executing code at the reset vector

135
00:05:08,160 --> 00:05:14,320
the cs base is added to the eip or ip in

136
00:05:11,840 --> 00:05:15,919
order to yield the actual location

137
00:05:14,320 --> 00:05:19,360
that's going to be accessed

138
00:05:15,919 --> 00:05:23,680
so if the cs base is fff zeros at the

139
00:05:19,360 --> 00:05:25,680
beginning and eip or ip is set to fff

140
00:05:23,680 --> 00:05:28,160
zero that means you're going to add them

141
00:05:25,680 --> 00:05:30,479
together and you're going to be getting

142
00:05:28,160 --> 00:05:33,440
fff ffff

143
00:05:30,479 --> 00:05:35,520
f one x ref zero so that's going to be

144
00:05:33,440 --> 00:05:38,639
that reset vector that we talked about

145
00:05:35,520 --> 00:05:42,160
the reset vector on an x86 system is all

146
00:05:38,639 --> 00:05:47,520
f's and a zero so seven f's in a zero

147
00:05:42,160 --> 00:05:48,639
64 32 gigs sorry four gigabytes minus 16

148
00:05:47,520 --> 00:05:50,800
bytes

149
00:05:48,639 --> 00:05:52,960
that is the location where the processor

150
00:05:50,800 --> 00:05:55,120
starts upon reset and just by virtue of

151
00:05:52,960 --> 00:05:57,919
the fact that the initialized reset

152
00:05:55,120 --> 00:06:01,039
state of the hardware is this particular

153
00:05:57,919 --> 00:06:02,880
eip this particular cs base so you might

154
00:06:01,039 --> 00:06:07,600
see some things when disassembling in

155
00:06:02,880 --> 00:06:09,199
16-bit mode showing it as f ffff0 but

156
00:06:07,600 --> 00:06:10,479
that address is actually translating

157
00:06:09,199 --> 00:06:12,240
through to this because the problem is

158
00:06:10,479 --> 00:06:14,319
the disassembler doesn't know doesn't

159
00:06:12,240 --> 00:06:16,400
account for the fact that the base

160
00:06:14,319 --> 00:06:18,720
address is actually this so it's really

161
00:06:16,400 --> 00:06:20,720
just saying well you know looks to me

162
00:06:18,720 --> 00:06:22,080
like 16-bit code would be this and again

163
00:06:20,720 --> 00:06:25,000
we'll talk a little bit more about why

164
00:06:22,080 --> 00:06:27,759
that's f and then f of zero instead of

165
00:06:25,000 --> 00:06:29,600
f-o-o-f-zero zero zero et cetera it's

166
00:06:27,759 --> 00:06:33,560
because of the way 16-bit segmentation

167
00:06:29,600 --> 00:06:33,560
versus 32-bit works

