1
00:00:00,880 --> 00:00:03,280
now at the very beginning of the class

2
00:00:02,159 --> 00:00:05,759
when we were talking about attacker

3
00:00:03,280 --> 00:00:08,160
motivation we said that bios lives on a

4
00:00:05,759 --> 00:00:10,160
flash chip soldered onto the motherboard

5
00:00:08,160 --> 00:00:12,719
what we just saw with respect to the

6
00:00:10,160 --> 00:00:15,440
reset vector is that the reset vector is

7
00:00:12,719 --> 00:00:17,440
at seven f's and a zero four gigabytes

8
00:00:15,440 --> 00:00:20,320
minus 16 bytes

9
00:00:17,440 --> 00:00:22,560
and the ip is set to that address in

10
00:00:20,320 --> 00:00:27,519
such a way that it'll start executing

11
00:00:22,560 --> 00:00:30,000
code from fff zero so code from fff0 and

12
00:00:27,519 --> 00:00:32,399
the initial starting state is spy flash

13
00:00:30,000 --> 00:00:34,719
chip that means that the hardware must

14
00:00:32,399 --> 00:00:36,719
automatically translate this memory

15
00:00:34,719 --> 00:00:40,960
address this physical memory looking

16
00:00:36,719 --> 00:00:42,320
address fff0 into access to the spy

17
00:00:40,960 --> 00:00:44,719
flash chip

18
00:00:42,320 --> 00:00:47,280
so what if i told you that you could

19
00:00:44,719 --> 00:00:50,079
actually partially control this mapping

20
00:00:47,280 --> 00:00:52,239
of memory to flash

21
00:00:50,079 --> 00:00:54,480
well we can do that and we can see that

22
00:00:52,239 --> 00:00:55,920
by fiddling around with some registers

23
00:00:54,480 --> 00:00:57,840
that we don't really understand yet and

24
00:00:55,920 --> 00:00:59,760
we're just gonna totally get ahead of

25
00:00:57,840 --> 00:01:01,840
ourselves but i just want to kind of

26
00:00:59,760 --> 00:01:04,720
show you what some of this behind the

27
00:01:01,840 --> 00:01:06,240
scenes hardware doing stuff looks like

28
00:01:04,720 --> 00:01:07,680
and what you can control which you can't

29
00:01:06,240 --> 00:01:09,200
control because we're going to be

30
00:01:07,680 --> 00:01:11,040
playing around a lot with this kind of

31
00:01:09,200 --> 00:01:13,360
thing in this class so if you look in

32
00:01:11,040 --> 00:01:16,320
the intel data sheets before the modern

33
00:01:13,360 --> 00:01:17,840
spy flash chip became commonly in use

34
00:01:16,320 --> 00:01:20,080
there was an older thing called the

35
00:01:17,840 --> 00:01:22,720
firmware hub which was on the low pin

36
00:01:20,080 --> 00:01:24,560
count bus so you could find things like

37
00:01:22,720 --> 00:01:26,799
this in the data sheet that were called

38
00:01:24,560 --> 00:01:29,920
the firmware hub the code enable

39
00:01:26,799 --> 00:01:32,960
register and it basically said like okay

40
00:01:29,920 --> 00:01:35,920
well this address range fff8 zero zero

41
00:01:32,960 --> 00:01:38,159
zero zero zero all the way to fff that

42
00:01:35,920 --> 00:01:40,079
is going to be something that decodes

43
00:01:38,159 --> 00:01:41,920
the firmware hub now if we look at a

44
00:01:40,079 --> 00:01:44,640
more modern thing that will instead be

45
00:01:41,920 --> 00:01:46,960
called the biostatic code enable and

46
00:01:44,640 --> 00:01:48,960
this is going to be mapping to the spy

47
00:01:46,960 --> 00:01:50,479
flash chip but again it just says the

48
00:01:48,960 --> 00:01:52,720
exact same thing and it's in the exact

49
00:01:50,479 --> 00:01:55,520
same location and it's saying this

50
00:01:52,720 --> 00:01:57,840
address fff8000

51
00:01:55,520 --> 00:02:00,799
all the way to all fs all of this is

52
00:01:57,840 --> 00:02:02,479
going to map into the spy flash chip now

53
00:02:00,799 --> 00:02:04,719
what's interesting about this bit right

54
00:02:02,479 --> 00:02:06,640
here is that it says read only so even

55
00:02:04,719 --> 00:02:09,759
though there's a zero option to disable

56
00:02:06,640 --> 00:02:13,200
this mapping you can't actually do that

57
00:02:09,759 --> 00:02:16,400
but bit 14 you can this is a read write

58
00:02:13,200 --> 00:02:20,319
thing so for this range three f's and

59
00:02:16,400 --> 00:02:23,200
five zeros all the way to fff seven and

60
00:02:20,319 --> 00:02:25,040
four f's that range you can actually

61
00:02:23,200 --> 00:02:27,200
control whether or not that physical

62
00:02:25,040 --> 00:02:29,360
memory address maps into the spy flash

63
00:02:27,200 --> 00:02:31,680
chip and so if you toggle this bit to

64
00:02:29,360 --> 00:02:33,440
zero then all of a sudden looking at

65
00:02:31,680 --> 00:02:36,800
that physical memory will go from

66
00:02:33,440 --> 00:02:38,560
looking at valid spy flash content to

67
00:02:36,800 --> 00:02:41,040
nothing at all

68
00:02:38,560 --> 00:02:43,519
so let's go ahead and have you go do a

69
00:02:41,040 --> 00:02:45,360
lab on that where you fiddle with things

70
00:02:43,519 --> 00:02:47,120
and you change out this bit you don't

71
00:02:45,360 --> 00:02:51,760
really understand anything yet about you

72
00:02:47,120 --> 00:02:53,920
know what all this means lpcif d31 f0 d8

73
00:02:51,760 --> 00:02:55,599
d9 blah blah blah you don't understand

74
00:02:53,920 --> 00:02:57,440
it yet but you will by the end of this

75
00:02:55,599 --> 00:02:59,920
class so for now i'm just going to give

76
00:02:57,440 --> 00:03:01,040
you basic instructions that say go poke

77
00:02:59,920 --> 00:03:02,959
this bit and you're going to see

78
00:03:01,040 --> 00:03:05,840
something change and later on you'll

79
00:03:02,959 --> 00:03:05,840
understand why that is

