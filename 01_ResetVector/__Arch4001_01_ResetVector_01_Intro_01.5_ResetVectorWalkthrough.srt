1
00:00:00,160 --> 00:00:04,880
all right let's see what's going on at

2
00:00:02,000 --> 00:00:06,160
the reset vector in Simics

3
00:00:04,880 --> 00:00:08,480
so

4
00:00:06,160 --> 00:00:10,400
if you start up your target can be any

5
00:00:08,480 --> 00:00:12,400
target really but i chose the first step

6
00:00:10,400 --> 00:00:15,519
Simics start up your target and then you

7
00:00:12,400 --> 00:00:17,760
go into debug and disassembly then

8
00:00:15,519 --> 00:00:20,880
you'll have the reset vector there

9
00:00:17,760 --> 00:00:22,880
you can go to debug and cpu registers

10
00:00:20,880 --> 00:00:23,840
and then you'll have stuff to look at

11
00:00:22,880 --> 00:00:26,080
there

12
00:00:23,840 --> 00:00:28,720
and then we'll pull up a memory window

13
00:00:26,080 --> 00:00:30,800
once it becomes necessary

14
00:00:28,720 --> 00:00:32,719
and you also wanted to enable the

15
00:00:30,800 --> 00:00:34,160
reverse execution so that you can walk

16
00:00:32,719 --> 00:00:36,079
backwards as needed

17
00:00:34,160 --> 00:00:38,800
so what do we see at the beginning we

18
00:00:36,079 --> 00:00:40,800
see knob knob jump so we just go ahead

19
00:00:38,800 --> 00:00:42,559
and step to that first jump

20
00:00:40,800 --> 00:00:45,920
then we see an assembly instruction

21
00:00:42,559 --> 00:00:47,680
putting 5042 into di all right that's

22
00:00:45,920 --> 00:00:48,719
fine you can see the change occurred

23
00:00:47,680 --> 00:00:50,399
there

24
00:00:48,719 --> 00:00:52,000
now another jump

25
00:00:50,399 --> 00:00:55,039
and another jump

26
00:00:52,000 --> 00:00:57,120
and a move eax to esp well eax is zero

27
00:00:55,039 --> 00:00:59,680
and esp is zero so that doesn't do

28
00:00:57,120 --> 00:01:00,879
anything then another jump and then

29
00:00:59,680 --> 00:01:02,640
another jump

30
00:01:00,879 --> 00:01:05,439
all right now we have some actual

31
00:01:02,640 --> 00:01:06,880
sequence of code got cli for clear the

32
00:01:05,439 --> 00:01:09,600
interrupt flag

33
00:01:06,880 --> 00:01:12,080
so if we step over that well the

34
00:01:09,600 --> 00:01:14,960
interrupt flag was already cleared so no

35
00:01:12,080 --> 00:01:18,640
change there now we have a move of f0

36
00:01:14,960 --> 00:01:20,159
into bx and then move of bx into ds so

37
00:01:18,640 --> 00:01:24,080
this is going to be moving into a

38
00:01:20,159 --> 00:01:25,920
segment register so step f0 and now we

39
00:01:24,080 --> 00:01:28,400
want to look at the segment registers we

40
00:01:25,920 --> 00:01:30,960
look at the system registers we see ds

41
00:01:28,400 --> 00:01:34,320
is right here it's set to zero so we

42
00:01:30,960 --> 00:01:37,600
step and then it says it's now got f00

43
00:01:34,320 --> 00:01:40,799
which has a base of f and four zeros

44
00:01:37,600 --> 00:01:42,399
because of 16-bit segmentation working a

45
00:01:40,799 --> 00:01:44,000
little bit differently than what we've

46
00:01:42,399 --> 00:01:46,399
seen before we'll talk about 16-bit

47
00:01:44,000 --> 00:01:50,720
segmentation in a bit next we're going

48
00:01:46,399 --> 00:01:55,759
to have move of ff98 into bx and then a

49
00:01:50,720 --> 00:01:57,200
load gdt from cs bx so we're going to

50
00:01:55,759 --> 00:02:00,719
step there

51
00:01:57,200 --> 00:02:02,960
and so bx at this point is going to be

52
00:02:00,719 --> 00:02:07,360
ff98

53
00:02:02,960 --> 00:02:09,759
and cs has a base of fff so it's

54
00:02:07,360 --> 00:02:10,920
basically going to be ffff

55
00:02:09,759 --> 00:02:14,000
and then

56
00:02:10,920 --> 00:02:15,760
ff98 so let's go ahead and get that

57
00:02:14,000 --> 00:02:18,480
memory window now

58
00:02:15,760 --> 00:02:20,560
debug memory contents and you're going

59
00:02:18,480 --> 00:02:23,840
to want to set that

60
00:02:20,560 --> 00:02:23,840
let me move this first

61
00:02:24,480 --> 00:02:31,360
you're going to want to set that to

62
00:02:26,080 --> 00:02:35,120
physical memory so board mb fism

63
00:02:31,360 --> 00:02:40,800
and then let's put in this value so

64
00:02:35,120 --> 00:02:42,160
one two three four f's and then ff98

65
00:02:40,800 --> 00:02:44,800
okay so

66
00:02:42,160 --> 00:02:46,239
this lgdt is going to be pulling from

67
00:02:44,800 --> 00:02:49,519
memory at

68
00:02:46,239 --> 00:02:51,599
cs is the segment selector and bx is the

69
00:02:49,519 --> 00:02:53,840
offset into that segment and the base of

70
00:02:51,599 --> 00:02:56,400
the segment is four f's and zeros so the

71
00:02:53,840 --> 00:02:57,760
offset is added to that and so it's

72
00:02:56,400 --> 00:02:59,920
essentially this

73
00:02:57,760 --> 00:03:01,280
now it's nine eight and this is nine

74
00:02:59,920 --> 00:03:03,280
zero so

75
00:03:01,280 --> 00:03:05,599
that tripped me up the first time

76
00:03:03,280 --> 00:03:08,159
we basically have to look right here at

77
00:03:05,599 --> 00:03:10,480
offset eight rather than offset zero

78
00:03:08,159 --> 00:03:13,360
and so the lg dt is going to be loading

79
00:03:10,480 --> 00:03:15,200
up six bytes it's going to have 16 bits

80
00:03:13,360 --> 00:03:17,760
the bottom two bytes are going to be the

81
00:03:15,200 --> 00:03:20,720
gdt limit and then the next four bytes

82
00:03:17,760 --> 00:03:23,440
are going to be at the gdt base

83
00:03:20,720 --> 00:03:27,680
so we expect that this gdt is going to

84
00:03:23,440 --> 00:03:31,519
be filled in with this ffff

85
00:03:27,680 --> 00:03:34,000
a0 with a limit of 17 hex 17. so let's

86
00:03:31,519 --> 00:03:38,239
step and that's what we see

87
00:03:34,000 --> 00:03:39,120
six f's a zero and a limit of 17.

88
00:03:38,239 --> 00:03:41,519
all right

89
00:03:39,120 --> 00:03:44,080
moving some constant into eax and then

90
00:03:41,519 --> 00:03:45,280
eax into cr0 so that's just going to set

91
00:03:44,080 --> 00:03:47,280
cr0

92
00:03:45,280 --> 00:03:48,319
let's just step over those and see what

93
00:03:47,280 --> 00:03:51,120
changed

94
00:03:48,319 --> 00:03:54,560
well we see that pe the protected mode

95
00:03:51,120 --> 00:03:56,799
enable bit was set to 1 so that means it

96
00:03:54,560 --> 00:03:59,280
is just transitioned from

97
00:03:56,799 --> 00:04:01,760
real mode into protected mode so now

98
00:03:59,280 --> 00:04:03,439
it's actually using that segmentation

99
00:04:01,760 --> 00:04:04,959
and then it's got some other bits that

100
00:04:03,439 --> 00:04:06,239
we don't really care about having

101
00:04:04,959 --> 00:04:08,799
changed

102
00:04:06,239 --> 00:04:09,599
next there's going to be a jump through

103
00:04:08,799 --> 00:04:12,799
the

104
00:04:09,599 --> 00:04:15,680
gdt so now this is sort of segmentation

105
00:04:12,799 --> 00:04:18,079
as we typically understood it so it's a

106
00:04:15,680 --> 00:04:19,359
segment selector which is going to be 10

107
00:04:18,079 --> 00:04:22,400
and so that's going to be you know

108
00:04:19,359 --> 00:04:24,800
selecting something inside the gdt so

109
00:04:22,400 --> 00:04:27,360
you take the bottom three bits you shift

110
00:04:24,800 --> 00:04:29,919
those to the right and then what you're

111
00:04:27,360 --> 00:04:33,440
left with is a gdt entry so let's go

112
00:04:29,919 --> 00:04:37,280
ahead and put in the gdt

113
00:04:33,440 --> 00:04:39,199
base address into our memory window

114
00:04:37,280 --> 00:04:41,680
all right so this is the gdt

115
00:04:39,199 --> 00:04:43,759
and this first

116
00:04:41,680 --> 00:04:45,759
eight bytes is going to be gdt entry

117
00:04:43,759 --> 00:04:47,520
zero which we said in architecture 2001

118
00:04:45,759 --> 00:04:51,919
is invalid and it's not allowed to be

119
00:04:47,520 --> 00:04:54,880
used this is index one this is index two

120
00:04:51,919 --> 00:04:56,320
etc and so we could you know parse all

121
00:04:54,880 --> 00:04:58,400
of that but you know what you're going

122
00:04:56,320 --> 00:05:00,080
to see is the simplest thing that i said

123
00:04:58,400 --> 00:05:02,880
everybody just uses the simplest which

124
00:05:00,080 --> 00:05:05,039
is base of zero offset of or sorry base

125
00:05:02,880 --> 00:05:08,479
of zero limit of all f's

126
00:05:05,039 --> 00:05:11,800
so big flat segment and that means that

127
00:05:08,479 --> 00:05:15,280
if the base is zero then this

128
00:05:11,800 --> 00:05:16,960
ffff7c is just going to jump to that so

129
00:05:15,280 --> 00:05:19,759
you can see that the addresses thus far

130
00:05:16,960 --> 00:05:23,199
have been acting as if they are you know

131
00:05:19,759 --> 00:05:25,600
zero zero zero fs but because of the

132
00:05:23,199 --> 00:05:28,600
code segment selector being at base of

133
00:05:25,600 --> 00:05:28,600
ffff000

134
00:05:28,720 --> 00:05:32,800
it's actually you know in the higher

135
00:05:30,240 --> 00:05:35,039
range so it's actually pulling from a

136
00:05:32,800 --> 00:05:37,360
bios code so we'll talk about this in

137
00:05:35,039 --> 00:05:40,320
the slides coming up

138
00:05:37,360 --> 00:05:44,320
so then we step through that and we do

139
00:05:40,320 --> 00:05:46,240
transition to six fs and 7c and now we

140
00:05:44,320 --> 00:05:49,520
have a little bit more code so moving

141
00:05:46,240 --> 00:05:52,160
640 into eax and then eax into

142
00:05:49,520 --> 00:05:54,240
cr4 so let's go ahead and step over that

143
00:05:52,160 --> 00:05:55,840
see what changes all right just some

144
00:05:54,240 --> 00:05:57,840
bits that we don't know anything about

145
00:05:55,840 --> 00:06:01,120
so let's keep trucking then we're going

146
00:05:57,840 --> 00:06:03,120
to see 8 into ax and ax into a bunch of

147
00:06:01,120 --> 00:06:05,360
segment selectors

148
00:06:03,120 --> 00:06:07,199
so step step step step and it's

149
00:06:05,360 --> 00:06:09,280
basically just getting you know set up

150
00:06:07,199 --> 00:06:11,199
that all of these sort of data segments

151
00:06:09,280 --> 00:06:13,520
not the code segment but all of these

152
00:06:11,199 --> 00:06:16,880
data segments are pointing at you know

153
00:06:13,520 --> 00:06:18,880
the first index in the gdt

154
00:06:16,880 --> 00:06:20,720
so those are all set up you can see that

155
00:06:18,880 --> 00:06:22,960
upon each of them loading it was set to

156
00:06:20,720 --> 00:06:24,560
8 but then it fills in the the base it

157
00:06:22,960 --> 00:06:27,120
fills in the limit it fills in the

158
00:06:24,560 --> 00:06:29,360
attributes so all of these are now

159
00:06:27,120 --> 00:06:31,240
filled in from the gdt

160
00:06:29,360 --> 00:06:33,039
all right and then a jump to

161
00:06:31,240 --> 00:06:36,080
ffffce

162
00:06:33,039 --> 00:06:37,680
and another jump after that and then

163
00:06:36,080 --> 00:06:39,759
this is where i said you can stop

164
00:06:37,680 --> 00:06:41,840
carrying so now we're going to see

165
00:06:39,759 --> 00:06:43,280
the thing just continuously looping

166
00:06:41,840 --> 00:06:46,000
backwards looking for this magic

167
00:06:43,280 --> 00:06:48,960
constant which just happens to be a grid

168
00:06:46,000 --> 00:06:50,400
used for efi file systems so that's all

169
00:06:48,960 --> 00:06:52,080
we really wanted to see we just wanted

170
00:06:50,400 --> 00:06:54,639
to see the fact that you know the system

171
00:06:52,080 --> 00:06:56,639
tries to get out of a real mode as soon

172
00:06:54,639 --> 00:06:58,880
as humanly possible sets some control

173
00:06:56,639 --> 00:07:02,479
bits but otherwise just you know sets up

174
00:06:58,880 --> 00:07:04,960
a gdt and moves into 32-bit protected

175
00:07:02,479 --> 00:07:04,960
mode

