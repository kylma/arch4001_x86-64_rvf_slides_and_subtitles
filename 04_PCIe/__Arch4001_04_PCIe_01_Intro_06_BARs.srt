1
00:00:00,560 --> 00:00:03,040
in this section i want to talk to you

2
00:00:01,839 --> 00:00:06,240
about bars

3
00:00:03,040 --> 00:00:09,840
not these bars not those bars but these

4
00:00:06,240 --> 00:00:12,160
bars bars bars bars bars

5
00:00:09,840 --> 00:00:14,400
and so in honor of these bars i've

6
00:00:12,160 --> 00:00:16,080
learned a bar joke for you

7
00:00:14,400 --> 00:00:17,760
so a horse walks into a bar and the

8
00:00:16,080 --> 00:00:19,439
bartender says you know you've been in

9
00:00:17,760 --> 00:00:22,080
here a lot lately do you think you might

10
00:00:19,439 --> 00:00:23,760
be an alcoholic and the horse says i

11
00:00:22,080 --> 00:00:25,439
don't think i am

12
00:00:23,760 --> 00:00:27,119
and it promptly disappears out of

13
00:00:25,439 --> 00:00:28,080
existence

14
00:00:27,119 --> 00:00:30,560
boom

15
00:00:28,080 --> 00:00:33,120
you've been decarded

16
00:00:30,560 --> 00:00:35,360
so base address registers what are they

17
00:00:33,120 --> 00:00:37,920
well base address registers are a

18
00:00:35,360 --> 00:00:40,960
location that specifies some sort of

19
00:00:37,920 --> 00:00:43,440
optional place where a part of the

20
00:00:40,960 --> 00:00:45,520
hardware a pci device is going to be

21
00:00:43,440 --> 00:00:47,840
exposed to the rest of the system could

22
00:00:45,520 --> 00:00:49,360
be via memory mapped i o could be via

23
00:00:47,840 --> 00:00:51,199
port i o

24
00:00:49,360 --> 00:00:53,760
the bars are read write and it's the

25
00:00:51,199 --> 00:00:55,680
responsibility of the bios to program

26
00:00:53,760 --> 00:00:58,079
them in the context of the overall

27
00:00:55,680 --> 00:00:59,520
memory map so if it's going to program

28
00:00:58,079 --> 00:01:00,800
you know one particular bar and then the

29
00:00:59,520 --> 00:01:02,960
next particular bar it needs to make

30
00:01:00,800 --> 00:01:05,360
sure those don't overlap with each other

31
00:01:02,960 --> 00:01:07,040
or any other devices on the system

32
00:01:05,360 --> 00:01:08,960
because when you're accessing some

33
00:01:07,040 --> 00:01:10,400
particular port i o or memory mapped i o

34
00:01:08,960 --> 00:01:12,400
range you want to make sure it's going

35
00:01:10,400 --> 00:01:14,000
to the device that you expect

36
00:01:12,400 --> 00:01:16,400
now obviously it's showing that there's

37
00:01:14,000 --> 00:01:19,360
space for six base address registers

38
00:01:16,400 --> 00:01:21,119
each of which is 32 bits long and that

39
00:01:19,360 --> 00:01:23,920
can be used for things that are going to

40
00:01:21,119 --> 00:01:26,240
be mapped inside of a 32-bit address

41
00:01:23,920 --> 00:01:28,799
range in the context of memory mapped io

42
00:01:26,240 --> 00:01:31,119
but you can also take and combine things

43
00:01:28,799 --> 00:01:33,840
that are adjacent in order to create

44
00:01:31,119 --> 00:01:36,000
bars that specify a 64-bit address if

45
00:01:33,840 --> 00:01:38,479
you need to base something in a base

46
00:01:36,000 --> 00:01:40,720
address register at a location above

47
00:01:38,479 --> 00:01:42,399
four gigabytes so if you were to use a

48
00:01:40,720 --> 00:01:44,560
tool like read write everything or

49
00:01:42,399 --> 00:01:46,159
chipset in order to go read out the pci

50
00:01:44,560 --> 00:01:47,680
configuration address space at the

51
00:01:46,159 --> 00:01:49,520
relevant offset for base address

52
00:01:47,680 --> 00:01:51,600
registers and if you saw something

53
00:01:49,520 --> 00:01:53,280
non-zero this is the way that you would

54
00:01:51,600 --> 00:01:55,520
interpret what the bios had written

55
00:01:53,280 --> 00:01:57,439
there now it's not just the case that

56
00:01:55,520 --> 00:01:59,200
you just take whatever value there and

57
00:01:57,439 --> 00:02:01,759
that's the base address of some memory

58
00:01:59,200 --> 00:02:03,920
mapped i o or port i o it has particular

59
00:02:01,759 --> 00:02:05,600
bits that require interpretation

60
00:02:03,920 --> 00:02:07,360
so first of all because they're in the

61
00:02:05,600 --> 00:02:09,039
case of memory mapped i o there are four

62
00:02:07,360 --> 00:02:10,959
least significant bits have special

63
00:02:09,039 --> 00:02:12,879
values and special meanings that means

64
00:02:10,959 --> 00:02:15,120
you would take whatever 32-bit value

65
00:02:12,879 --> 00:02:18,080
there was and you would mask it with all

66
00:02:15,120 --> 00:02:19,920
fs and a zero in order to remove the

67
00:02:18,080 --> 00:02:21,680
least significant four bits and that

68
00:02:19,920 --> 00:02:23,599
would give you the actual base address

69
00:02:21,680 --> 00:02:25,599
that would be used in this case in

70
00:02:23,599 --> 00:02:28,400
memory mapped io

71
00:02:25,599 --> 00:02:30,160
now the least significant bit being zero

72
00:02:28,400 --> 00:02:32,800
specifically says that this is going to

73
00:02:30,160 --> 00:02:35,280
be a memory mapped io base address

74
00:02:32,800 --> 00:02:37,920
register so it's saying i want this base

75
00:02:35,280 --> 00:02:40,560
address as calculated here to be used

76
00:02:37,920 --> 00:02:43,040
within the physical address range

77
00:02:40,560 --> 00:02:44,800
now these next two bits right here will

78
00:02:43,040 --> 00:02:47,200
specify whether or not this address

79
00:02:44,800 --> 00:02:50,160
should be treated as a 32-bit address or

80
00:02:47,200 --> 00:02:51,920
a 64-bit address so if it's one zero

81
00:02:50,160 --> 00:02:54,000
then that means this could be inside the

82
00:02:51,920 --> 00:02:55,599
64-bit address space which means you're

83
00:02:54,000 --> 00:02:57,680
going to need to use two consecutive

84
00:02:55,599 --> 00:02:59,760
bars and if it was zero zero then you

85
00:02:57,680 --> 00:03:02,080
were talking about a 32-bit address

86
00:02:59,760 --> 00:03:04,640
which would therefore fit in here as

87
00:03:02,080 --> 00:03:06,400
mentioned before 64-bit addresses means

88
00:03:04,640 --> 00:03:09,040
that you need to take two consecutive

89
00:03:06,400 --> 00:03:11,200
bars two consecutive 32-bit values and

90
00:03:09,040 --> 00:03:13,840
fill in an address in this portion and

91
00:03:11,200 --> 00:03:15,519
then also in portion of the next bar so

92
00:03:13,840 --> 00:03:17,280
the way that's actually calculated is

93
00:03:15,519 --> 00:03:18,400
that you know whatever let's say it was

94
00:03:17,280 --> 00:03:20,239
bar zero

95
00:03:18,400 --> 00:03:22,480
and you would take and mask off the

96
00:03:20,239 --> 00:03:25,280
least significant four bits as usual and

97
00:03:22,480 --> 00:03:26,560
then you would you know add that to the

98
00:03:25,280 --> 00:03:28,080
next bar

99
00:03:26,560 --> 00:03:30,159
shifted by

100
00:03:28,080 --> 00:03:31,840
masked with all f so basically or just

101
00:03:30,159 --> 00:03:34,000
not masked at all

102
00:03:31,840 --> 00:03:36,720
shifted by 32 bits which is basically

103
00:03:34,000 --> 00:03:40,239
the most significant 32 bits of that 64

104
00:03:36,720 --> 00:03:43,760
bit value so the bar 0 is the least

105
00:03:40,239 --> 00:03:46,560
significant 32 bits and the bar 1 is the

106
00:03:43,760 --> 00:03:48,640
most significant 64 bits on the other

107
00:03:46,560 --> 00:03:50,080
hand if you read back the 32-bit value

108
00:03:48,640 --> 00:03:51,760
from the base address registering you

109
00:03:50,080 --> 00:03:53,760
found that the least significant bit was

110
00:03:51,760 --> 00:03:55,680
one this is indicating that this is

111
00:03:53,760 --> 00:03:57,840
actually a port i o

112
00:03:55,680 --> 00:04:00,720
base address register so it's going to

113
00:03:57,840 --> 00:04:03,120
be an address within the port i o range

114
00:04:00,720 --> 00:04:06,000
so when we were talking about fixed i o

115
00:04:03,120 --> 00:04:08,640
versus variable i o this is a means by

116
00:04:06,000 --> 00:04:12,159
which pcie hardware can create a

117
00:04:08,640 --> 00:04:14,480
variable range to be used for port i o

118
00:04:12,159 --> 00:04:16,479
so again because there's two special

119
00:04:14,480 --> 00:04:18,560
bits on the least significant bits you

120
00:04:16,479 --> 00:04:21,519
would take a base address that you see

121
00:04:18,560 --> 00:04:23,440
in the bar you would mask it with ffff

122
00:04:21,519 --> 00:04:25,360
and c in order to get rid of least

123
00:04:23,440 --> 00:04:27,680
significant two bits and that would be

124
00:04:25,360 --> 00:04:29,520
the actual port that you would start

125
00:04:27,680 --> 00:04:31,440
accessing memory on

126
00:04:29,520 --> 00:04:33,360
now the pcie sig does not actually

127
00:04:31,440 --> 00:04:36,080
recommend using port i o because it's a

128
00:04:33,360 --> 00:04:38,080
very limited space and fragmented so

129
00:04:36,080 --> 00:04:40,400
generally speaking most devices will use

130
00:04:38,080 --> 00:04:42,320
memory mapped io but go ahead and you

131
00:04:40,400 --> 00:04:44,560
know check your device later you'll see

132
00:04:42,320 --> 00:04:48,400
whether or not you have any port i o

133
00:04:44,560 --> 00:04:48,400
available through pci devices

134
00:04:48,560 --> 00:04:52,479
so returning to this picture of miss

135
00:04:50,400 --> 00:04:55,040
frizzle accessing the various address

136
00:04:52,479 --> 00:04:57,919
spaces now we can understand that for

137
00:04:55,040 --> 00:04:59,759
this particular nick if it has a 256

138
00:04:57,919 --> 00:05:01,759
byte configuration address space or

139
00:04:59,759 --> 00:05:03,919
maybe it's 4k if it's using pcie

140
00:05:01,759 --> 00:05:06,880
extended address space if it has this

141
00:05:03,919 --> 00:05:09,759
256 bytes within that space within the

142
00:05:06,880 --> 00:05:13,280
standardized header are going to be bars

143
00:05:09,759 --> 00:05:15,120
and so the driver could set bar 0 to be

144
00:05:13,280 --> 00:05:17,120
some sort of memory mapped i o which

145
00:05:15,120 --> 00:05:18,479
would allow access to some sort of

146
00:05:17,120 --> 00:05:21,120
internal information from this

147
00:05:18,479 --> 00:05:23,520
particular device or it could set bar 1

148
00:05:21,120 --> 00:05:25,199
to be port i o in order to again just

149
00:05:23,520 --> 00:05:26,720
access some other different portion of

150
00:05:25,199 --> 00:05:27,919
this device or maybe they could overlap

151
00:05:26,720 --> 00:05:29,680
and they're just two different ways to

152
00:05:27,919 --> 00:05:31,759
access the same information or maybe

153
00:05:29,680 --> 00:05:33,600
there could be none of them at all and

154
00:05:31,759 --> 00:05:36,000
you know it's all up to the particular

155
00:05:33,600 --> 00:05:38,240
device manufacturer it's all up to how

156
00:05:36,000 --> 00:05:39,840
they make how they intend to make a

157
00:05:38,240 --> 00:05:42,000
driver interact with their particular

158
00:05:39,840 --> 00:05:44,080
hardware but that leaves us with one

159
00:05:42,000 --> 00:05:47,360
question still and that is you know i've

160
00:05:44,080 --> 00:05:49,759
listed thus far tbd bytes of nick memory

161
00:05:47,360 --> 00:05:52,320
mapped iospace so how do we determine

162
00:05:49,759 --> 00:05:54,479
how does the bios find out how big of a

163
00:05:52,320 --> 00:05:56,080
chunk of the memory mapped io space that

164
00:05:54,479 --> 00:05:57,759
it should reserve for instance for

165
00:05:56,080 --> 00:05:59,600
talking to this device

166
00:05:57,759 --> 00:06:02,080
so i want to welcome you to the wacky

167
00:05:59,600 --> 00:06:04,400
world of bar sizing

168
00:06:02,080 --> 00:06:06,639
so the bios doesn't get to just say like

169
00:06:04,400 --> 00:06:08,960
okay well you get hex 1000 bytes and you

170
00:06:06,639 --> 00:06:10,720
get hex 2000 bytes it has to ask the

171
00:06:08,960 --> 00:06:12,080
device how many bytes do you actually

172
00:06:10,720 --> 00:06:15,360
need how much are you going to be

173
00:06:12,080 --> 00:06:17,520
exposing to memory map diode or port io

174
00:06:15,360 --> 00:06:19,680
and the way that it actually requests

175
00:06:17,520 --> 00:06:22,400
the information about the size

176
00:06:19,680 --> 00:06:25,840
is that it writes all ones in binary or

177
00:06:22,400 --> 00:06:26,880
all fs and hex into this 32-bit value of

178
00:06:25,840 --> 00:06:29,280
a bar

179
00:06:26,880 --> 00:06:30,880
if the device actually has something

180
00:06:29,280 --> 00:06:33,280
that it wants to expose via memory

181
00:06:30,880 --> 00:06:36,080
mapped i o or port i o the device will

182
00:06:33,280 --> 00:06:37,600
return something not equal to all the

183
00:06:36,080 --> 00:06:39,680
ones that were just written in there if

184
00:06:37,600 --> 00:06:41,440
it's still all ones then that means that

185
00:06:39,680 --> 00:06:44,960
this thing has you know no space that it

186
00:06:41,440 --> 00:06:47,360
wants to map but if it's non all fs

187
00:06:44,960 --> 00:06:48,560
then that means the bits that were set

188
00:06:47,360 --> 00:06:50,560
to zero

189
00:06:48,560 --> 00:06:52,160
the device is basically saying these

190
00:06:50,560 --> 00:06:54,240
bits that are set to zero these are

191
00:06:52,160 --> 00:06:55,599
valid address bits that you can set

192
00:06:54,240 --> 00:06:57,199
these are things where i don't care if

193
00:06:55,599 --> 00:06:59,280
you go ahead and set these when you're

194
00:06:57,199 --> 00:07:00,720
accessing my memory mapped io once you

195
00:06:59,280 --> 00:07:01,759
decide what base address you're going to

196
00:07:00,720 --> 00:07:04,000
put it at

197
00:07:01,759 --> 00:07:06,000
so one way of thinking about that is if

198
00:07:04,000 --> 00:07:07,840
this particular device returned three

199
00:07:06,000 --> 00:07:09,840
f's and five zeros

200
00:07:07,840 --> 00:07:13,840
the you know i don't care and you may

201
00:07:09,840 --> 00:07:16,000
set bits are you know zero and fff so

202
00:07:13,840 --> 00:07:17,840
all these bits are the ones that are

203
00:07:16,000 --> 00:07:19,759
don't care and can be used for addresses

204
00:07:17,840 --> 00:07:21,440
so if we interpret it that way then it's

205
00:07:19,759 --> 00:07:23,919
basically saying you can create an

206
00:07:21,440 --> 00:07:28,160
address within my memory mapped io range

207
00:07:23,919 --> 00:07:30,479
from 0 to fff so 5 fs

208
00:07:28,160 --> 00:07:33,039
and so that means that if the indexes of

209
00:07:30,479 --> 00:07:36,000
the addresses are zero to five f's that

210
00:07:33,039 --> 00:07:38,639
means the total size is one and five

211
00:07:36,000 --> 00:07:40,080
zeros which is one megabyte so basically

212
00:07:38,639 --> 00:07:42,160
this would be a device saying you know

213
00:07:40,080 --> 00:07:45,039
what i need one megabyte of memory

214
00:07:42,160 --> 00:07:47,120
mapped i o space so you can think about

215
00:07:45,039 --> 00:07:49,360
these bits that get set to zero as do

216
00:07:47,120 --> 00:07:51,039
not care bits as address bits or you

217
00:07:49,360 --> 00:07:53,680
could just think of it as the device

218
00:07:51,039 --> 00:07:55,759
returns the two's complement of the size

219
00:07:53,680 --> 00:07:58,479
that it actually wants reserved so you

220
00:07:55,759 --> 00:08:00,560
can see that if we took this value fs we

221
00:07:58,479 --> 00:08:02,479
flip the bits like this and we add one

222
00:08:00,560 --> 00:08:04,639
we get this one megabyte size so it's

223
00:08:02,479 --> 00:08:07,440
ultimately the two's complement of the

224
00:08:04,639 --> 00:08:09,280
total size needed but there is one more

225
00:08:07,440 --> 00:08:11,120
register that is of importance when

226
00:08:09,280 --> 00:08:13,680
dealing with these memory mapped i o

227
00:08:11,120 --> 00:08:15,599
base address registers or port i o base

228
00:08:13,680 --> 00:08:18,319
address registers and that is the

229
00:08:15,599 --> 00:08:21,280
command register at offset four

230
00:08:18,319 --> 00:08:24,240
two bytes large inside the pci header

231
00:08:21,280 --> 00:08:25,759
header and that has bit zero and one

232
00:08:24,240 --> 00:08:28,160
which have to do with whether this

233
00:08:25,759 --> 00:08:30,879
device should be enabled for access

234
00:08:28,160 --> 00:08:33,680
within the i o address space port i o or

235
00:08:30,879 --> 00:08:35,519
the memory space so basically the bios

236
00:08:33,680 --> 00:08:38,320
could set a base address register but

237
00:08:35,519 --> 00:08:40,080
unless these command bits were set then

238
00:08:38,320 --> 00:08:42,320
it wouldn't actually be enabled for port

239
00:08:40,080 --> 00:08:44,480
i o or memory map dio and you can

240
00:08:42,320 --> 00:08:46,640
actually see this on a device that is

241
00:08:44,480 --> 00:08:48,399
fully enabled and working if you go

242
00:08:46,640 --> 00:08:50,640
toggle these bits off then all of a

243
00:08:48,399 --> 00:08:52,720
sudden the memory mapped io range will

244
00:08:50,640 --> 00:08:54,720
disappear from physical memory and

245
00:08:52,720 --> 00:08:56,480
likewise for port i o of course if you

246
00:08:54,720 --> 00:08:58,399
do that the driver is probably going to

247
00:08:56,480 --> 00:09:00,320
have a pretty bad time it's going to

248
00:08:58,399 --> 00:09:01,600
stop working whatever device you're

249
00:09:00,320 --> 00:09:04,160
interacting with is probably going to

250
00:09:01,600 --> 00:09:04,160
crash

