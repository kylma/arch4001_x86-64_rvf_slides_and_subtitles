1
00:00:00,240 --> 00:00:05,680
all right we've finally covered port io

2
00:00:02,639 --> 00:00:08,720
access to configuration address space to

3
00:00:05,680 --> 00:00:11,599
my satisfaction so now let's move on to

4
00:00:08,720 --> 00:00:14,559
memory mapped io and specifically the

5
00:00:11,599 --> 00:00:17,600
pcie con extended configuration address

6
00:00:14,559 --> 00:00:20,080
space so if this is the compatible or

7
00:00:17,600 --> 00:00:22,080
legacy pci configuration address space

8
00:00:20,080 --> 00:00:24,880
256 bytes

9
00:00:22,080 --> 00:00:28,160
then the pcie extended configuration

10
00:00:24,880 --> 00:00:31,119
address space is extended up to 4

11
00:00:28,160 --> 00:00:34,719
kilobytes so fffc here all the way up to

12
00:00:31,119 --> 00:00:36,000
ffff sorry 3f's that's going to be 4

13
00:00:34,719 --> 00:00:38,000
kilobytes

14
00:00:36,000 --> 00:00:39,680
so we said before that the only way to

15
00:00:38,000 --> 00:00:41,840
access the extended configuration

16
00:00:39,680 --> 00:00:44,399
address space is via memory mapped i o

17
00:00:41,840 --> 00:00:47,440
not port i o and specifically there is

18
00:00:44,399 --> 00:00:49,840
going to be a register pci x bar

19
00:00:47,440 --> 00:00:52,000
which is going to say the location where

20
00:00:49,840 --> 00:00:54,320
something should be mapped into memory

21
00:00:52,000 --> 00:00:56,399
i've got this sort of old documentation

22
00:00:54,320 --> 00:00:58,800
from a 3 series chipset which i'm going

23
00:00:56,399 --> 00:01:01,120
to use because it has a nice uncolored

24
00:00:58,800 --> 00:01:02,800
diagram that i get to color myself so

25
00:01:01,120 --> 00:01:05,199
what does this documentation tell us

26
00:01:02,800 --> 00:01:07,040
about pci xbar

27
00:01:05,199 --> 00:01:09,439
well it tells us

28
00:01:07,040 --> 00:01:12,080
there is a register

29
00:01:09,439 --> 00:01:14,240
there is a register pci x bar that

30
00:01:12,080 --> 00:01:16,640
defines the base address for the block

31
00:01:14,240 --> 00:01:18,720
of addresses below 4 gigabytes for the

32
00:01:16,640 --> 00:01:20,960
configuration space associated with

33
00:01:18,720 --> 00:01:23,200
buses devices and functions that are

34
00:01:20,960 --> 00:01:26,920
potentially a part of the pci express

35
00:01:23,200 --> 00:01:26,920
root complex hierarchy

36
00:01:26,960 --> 00:01:30,960
okay well what does that mean

37
00:01:28,799 --> 00:01:35,040
well what it means is that there's some

38
00:01:30,960 --> 00:01:37,920
big long array of four kilobyte pci

39
00:01:35,040 --> 00:01:40,640
express extended address spaces that

40
00:01:37,920 --> 00:01:44,079
when laid end to end

41
00:01:40,640 --> 00:01:48,960
starting at pci x bar can end up being

42
00:01:44,079 --> 00:01:52,000
up to 256 megabytes of memory mapped i o

43
00:01:48,960 --> 00:01:54,560
so why is it 256 megabytes well because

44
00:01:52,000 --> 00:01:56,159
at the very bottom you have the stuff

45
00:01:54,560 --> 00:01:58,479
for bus zero

46
00:01:56,159 --> 00:02:01,520
and then within the bus zero you have

47
00:01:58,479 --> 00:02:03,600
the 32 devices within the device you

48
00:02:01,520 --> 00:02:05,840
have eight functions and when within

49
00:02:03,600 --> 00:02:08,720
each function you can have four

50
00:02:05,840 --> 00:02:11,200
kilobytes of extended address space

51
00:02:08,720 --> 00:02:13,280
right so ff that would be hex 1000

52
00:02:11,200 --> 00:02:16,560
that's four kilobytes then four

53
00:02:13,280 --> 00:02:18,879
kilobytes times eight hex 8000 is going

54
00:02:16,560 --> 00:02:20,840
to be 32 kilobytes

55
00:02:18,879 --> 00:02:24,959
and then that times

56
00:02:20,840 --> 00:02:28,080
32 one and five zeros is one megabyte so

57
00:02:24,959 --> 00:02:32,879
one megabyte to specify a given bus and

58
00:02:28,080 --> 00:02:34,560
then 256 buses so up to 256 megabytes

59
00:02:32,879 --> 00:02:35,760
when you lay these things end to end and

60
00:02:34,560 --> 00:02:38,080
that's basically the way you should

61
00:02:35,760 --> 00:02:40,959
think of it it's just a big array of

62
00:02:38,080 --> 00:02:43,760
four kilobyte at a time pci extended

63
00:02:40,959 --> 00:02:46,239
address spaces for bus zero device zero

64
00:02:43,760 --> 00:02:48,720
function zero plus zero device zero

65
00:02:46,239 --> 00:02:51,680
function one plus zero device zero

66
00:02:48,720 --> 00:02:56,080
function two etc and then for that

67
00:02:51,680 --> 00:02:57,920
multiple busses all the way up to 255.

68
00:02:56,080 --> 00:03:01,280
now while that would take up a maximum

69
00:02:57,920 --> 00:03:03,599
of 256 megabytes if the particular

70
00:03:01,280 --> 00:03:06,560
device wanted to actually address things

71
00:03:03,599 --> 00:03:08,239
up at bus 255 almost no devices are

72
00:03:06,560 --> 00:03:10,720
going to actually need to address things

73
00:03:08,239 --> 00:03:13,280
up that high so there's a provision in

74
00:03:10,720 --> 00:03:16,480
here to allow it to instead you know cut

75
00:03:13,280 --> 00:03:18,239
this in half use only 128 megabytes and

76
00:03:16,480 --> 00:03:20,319
then consequently you can only access

77
00:03:18,239 --> 00:03:23,599
you know memory mapped io up to the you

78
00:03:20,319 --> 00:03:25,840
know bus 127 or you could cut it in

79
00:03:23,599 --> 00:03:27,920
fourths and only use the bottom 64

80
00:03:25,840 --> 00:03:30,959
megabytes and that's basically just to

81
00:03:27,920 --> 00:03:32,480
save some of the physical address space

82
00:03:30,959 --> 00:03:34,560
so that you can reuse it for other

83
00:03:32,480 --> 00:03:36,799
memory mapped i o devices

84
00:03:34,560 --> 00:03:38,480
now when it comes to indexing into

85
00:03:36,799 --> 00:03:40,959
memory mapped i o it's going to look

86
00:03:38,480 --> 00:03:42,799
different from port i o so back in port

87
00:03:40,959 --> 00:03:44,319
i o we learned about this particular

88
00:03:42,799 --> 00:03:46,799
format and we said you know there's

89
00:03:44,319 --> 00:03:48,480
eight bits for the offset and there's

90
00:03:46,799 --> 00:03:50,400
three bits for the function five bits

91
00:03:48,480 --> 00:03:53,040
for the device and eight bits for the

92
00:03:50,400 --> 00:03:55,920
bus number but specifically because

93
00:03:53,040 --> 00:03:58,799
there's only eight bits for the offset

94
00:03:55,920 --> 00:04:02,560
that means you can only access hex 1000

95
00:03:58,799 --> 00:04:04,879
x 100 bytes at a time 256 bytes and that

96
00:04:02,560 --> 00:04:07,040
means that's why port i o cannot get you

97
00:04:04,879 --> 00:04:08,879
access to the full extended pcie

98
00:04:07,040 --> 00:04:11,200
configuration address space

99
00:04:08,879 --> 00:04:14,159
on the other hand the encoding for

100
00:04:11,200 --> 00:04:16,959
accessing via memory mapped io has the

101
00:04:14,159 --> 00:04:18,799
least significant 12 bits usable and

102
00:04:16,959 --> 00:04:21,280
that of course means you can access hex

103
00:04:18,799 --> 00:04:23,919
1000 bytes at a time 4 kilobytes which

104
00:04:21,280 --> 00:04:26,880
is the full offset within the extended

105
00:04:23,919 --> 00:04:29,120
address space and we can also see that

106
00:04:26,880 --> 00:04:31,840
it allows for mapping this above 4

107
00:04:29,120 --> 00:04:34,560
gigabytes so you can see that bits 35 to

108
00:04:31,840 --> 00:04:37,040
28 can be used for the upper address

109
00:04:34,560 --> 00:04:39,840
bits and all of these lower 28 bits are

110
00:04:37,040 --> 00:04:41,440
going to be assumed to be zero

111
00:04:39,840 --> 00:04:43,919
so that means that you know back here

112
00:04:41,440 --> 00:04:45,520
this diagram in this documentation was

113
00:04:43,919 --> 00:04:47,040
actually inaccurate and that you can

114
00:04:45,520 --> 00:04:49,040
actually map the thing above four

115
00:04:47,040 --> 00:04:52,560
gigabytes

116
00:04:49,040 --> 00:04:54,639
so but the nerd in me says well actually

117
00:04:52,560 --> 00:04:57,840
it's not always the case that it's using

118
00:04:54,639 --> 00:04:59,520
bits 35 to 28 so it turns out that

119
00:04:57,840 --> 00:05:01,600
different cpus different memory

120
00:04:59,520 --> 00:05:03,120
controllers back uh you know before

121
00:05:01,600 --> 00:05:05,039
using pchs

122
00:05:03,120 --> 00:05:06,639
they have a different number of bits

123
00:05:05,039 --> 00:05:08,720
that are used for the most significant

124
00:05:06,639 --> 00:05:10,560
bits of the pci x-bar

125
00:05:08,720 --> 00:05:12,560
and i was looking at the core the first

126
00:05:10,560 --> 00:05:14,800
generation core and i couldn't find it

127
00:05:12,560 --> 00:05:16,240
mapped in the normal place probably a

128
00:05:14,800 --> 00:05:18,479
consequence of you know just

129
00:05:16,240 --> 00:05:21,759
documentation errors and stuff like that

130
00:05:18,479 --> 00:05:24,240
i did find a sad pci express bar the

131
00:05:21,759 --> 00:05:26,560
system address decoder thing but because

132
00:05:24,240 --> 00:05:28,720
the bits there are kind of like not at

133
00:05:26,560 --> 00:05:31,039
all similar to these other ones not sure

134
00:05:28,720 --> 00:05:32,560
whether that's actually true so you know

135
00:05:31,039 --> 00:05:34,639
someone either with access to the

136
00:05:32,560 --> 00:05:36,560
proprietary documentation or an intel

137
00:05:34,639 --> 00:05:39,120
engineer will have to tell me whether or

138
00:05:36,560 --> 00:05:42,639
not that's just missing the standard pci

139
00:05:39,120 --> 00:05:45,600
x bar definition in the documentation

140
00:05:42,639 --> 00:05:48,160
so pci explorer pci x bar where for art

141
00:05:45,600 --> 00:05:50,800
thou pci x bar well it's typically at

142
00:05:48,160 --> 00:05:53,120
bdf00060

143
00:05:50,800 --> 00:05:55,039
so again other than the first generation

144
00:05:53,120 --> 00:05:56,400
core where i can't find it for there for

145
00:05:55,039 --> 00:05:58,400
some reason probably just because the

146
00:05:56,400 --> 00:06:00,160
documentation got messed up when they

147
00:05:58,400 --> 00:06:02,240
were transitioning from the pre-chorus

148
00:06:00,160 --> 00:06:04,680
varies to the core series

149
00:06:02,240 --> 00:06:07,199
and as you may recall

150
00:06:04,680 --> 00:06:08,720
bdf-000 is the dram controller so it

151
00:06:07,199 --> 00:06:10,160
kind of makes sense that you know the

152
00:06:08,720 --> 00:06:11,919
memory controller is going to be

153
00:06:10,160 --> 00:06:14,319
responsible for setting up this big

154
00:06:11,919 --> 00:06:16,960
chunk of memory mapped i o so now i want

155
00:06:14,319 --> 00:06:19,759
you to go ahead and do a lab find your

156
00:06:16,960 --> 00:06:21,759
pci x bar and go ahead and access the

157
00:06:19,759 --> 00:06:24,080
extended configuration address space via

158
00:06:21,759 --> 00:06:25,520
memory map dial access the compatible

159
00:06:24,080 --> 00:06:28,720
configuration address space the first

160
00:06:25,520 --> 00:06:31,120
256 bytes by memory io and compare the

161
00:06:28,720 --> 00:06:32,800
results to what you see with port i o so

162
00:06:31,120 --> 00:06:35,360
let's have you go off and do that right

163
00:06:32,800 --> 00:06:35,360
now

