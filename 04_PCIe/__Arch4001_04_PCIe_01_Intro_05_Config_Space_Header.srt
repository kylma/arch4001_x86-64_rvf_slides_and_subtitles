1
00:00:00,080 --> 00:00:03,439
now let's do a deeper dive on what

2
00:00:01,760 --> 00:00:05,440
exactly can be found inside the

3
00:00:03,439 --> 00:00:08,400
configuration address space as already

4
00:00:05,440 --> 00:00:11,360
stated it's 256 bytes and it is required

5
00:00:08,400 --> 00:00:13,920
so every pcie device must implement it

6
00:00:11,360 --> 00:00:16,080
the first 40 bytes are going to be a

7
00:00:13,920 --> 00:00:18,240
standardized header and the remaining

8
00:00:16,080 --> 00:00:19,439
bytes are all just vendor dependent

9
00:00:18,240 --> 00:00:21,439
information

10
00:00:19,439 --> 00:00:23,119
so let's drill deeper on that config

11
00:00:21,439 --> 00:00:26,480
space header

12
00:00:23,119 --> 00:00:27,680
ah yes mother flipping data structures

13
00:00:26,480 --> 00:00:29,519
that's right flipping because i don't

14
00:00:27,680 --> 00:00:31,599
want to deal with youtube content

15
00:00:29,519 --> 00:00:34,000
moderation but while we're here

16
00:00:31,599 --> 00:00:34,000
flip

17
00:00:34,719 --> 00:00:40,960
alright so first 40 bytes enhance header

18
00:00:38,160 --> 00:00:42,480
rotate 0 degrees

19
00:00:40,960 --> 00:00:44,480
so the

20
00:00:42,480 --> 00:00:46,960
first thing i would point out is this

21
00:00:44,480 --> 00:00:49,120
header type field because all of this

22
00:00:46,960 --> 00:00:51,600
information is going to depend on the

23
00:00:49,120 --> 00:00:53,199
header type field so if it is type zero

24
00:00:51,600 --> 00:00:55,680
it's a general device and that's usually

25
00:00:53,199 --> 00:00:59,120
what we care about but there are special

26
00:00:55,680 --> 00:01:00,800
types one and two for pci to pci bridges

27
00:00:59,120 --> 00:01:02,800
and card bus bridges and we don't really

28
00:01:00,800 --> 00:01:05,119
care about those most of the time

29
00:01:02,800 --> 00:01:07,280
now this first 16 bytes is what's

30
00:01:05,119 --> 00:01:09,600
actually required required and the rest

31
00:01:07,280 --> 00:01:11,600
of it is optional so a vendor doesn't

32
00:01:09,600 --> 00:01:14,000
actually have to have anything special

33
00:01:11,600 --> 00:01:16,000
in the rest of these but if they do have

34
00:01:14,000 --> 00:01:18,880
something there then it needs to conform

35
00:01:16,000 --> 00:01:21,119
to this outlined specification

36
00:01:18,880 --> 00:01:23,119
remember this is not yet the vendor

37
00:01:21,119 --> 00:01:25,840
implementation dependent space that

38
00:01:23,119 --> 00:01:27,520
comes after the first hex 40 bytes so if

39
00:01:25,840 --> 00:01:28,960
they're using something here it has to

40
00:01:27,520 --> 00:01:30,240
be used the way that it's outlined and

41
00:01:28,960 --> 00:01:32,799
we'll talk about some of them like the

42
00:01:30,240 --> 00:01:34,320
base address registers later on now

43
00:01:32,799 --> 00:01:36,799
let's enhance the header header and

44
00:01:34,320 --> 00:01:39,040
rotate zero degrees now just a quick

45
00:01:36,799 --> 00:01:41,439
cautionary note because these data

46
00:01:39,040 --> 00:01:42,720
structures are from the pci sig they are

47
00:01:41,439 --> 00:01:45,119
not shown in the way that you would

48
00:01:42,720 --> 00:01:47,040
typically expect based on intel manuals

49
00:01:45,119 --> 00:01:49,360
so the low addresses are high and the

50
00:01:47,040 --> 00:01:51,439
high addresses are low and the least

51
00:01:49,360 --> 00:01:53,680
significant bits are to the right as you

52
00:01:51,439 --> 00:01:56,399
would expect so this would be byte zero

53
00:01:53,680 --> 00:01:57,840
this would be by one two and three so

54
00:01:56,399 --> 00:01:59,600
just keep that in mind that this is

55
00:01:57,840 --> 00:02:01,119
perhaps upside down from what you might

56
00:01:59,600 --> 00:02:03,360
expect if this was just an intel

57
00:02:01,119 --> 00:02:05,360
document so under most circumstances you

58
00:02:03,360 --> 00:02:07,439
can actually identify a pci device by

59
00:02:05,360 --> 00:02:09,440
just reading the first two fields the

60
00:02:07,439 --> 00:02:11,440
vendor id and device id

61
00:02:09,440 --> 00:02:13,920
now the vendor id is set to by the

62
00:02:11,440 --> 00:02:15,840
manufacturer it's actually allocated by

63
00:02:13,920 --> 00:02:17,280
the pci sig so that they can you know

64
00:02:15,840 --> 00:02:21,120
say this vendor is different from that

65
00:02:17,280 --> 00:02:23,520
one and intel happens to have 8086

66
00:02:21,120 --> 00:02:26,080
next up is the device id and the

67
00:02:23,520 --> 00:02:27,680
particular vendor sets a device id for

68
00:02:26,080 --> 00:02:29,599
whatever pci device they're talking

69
00:02:27,680 --> 00:02:31,120
about so it's the second field that you

70
00:02:29,599 --> 00:02:33,680
were actually looking at before when you

71
00:02:31,120 --> 00:02:36,720
were looking up the device id for your

72
00:02:33,680 --> 00:02:38,160
memory controller or your lpc device you

73
00:02:36,720 --> 00:02:40,400
would have seen that the vendor id was

74
00:02:38,160 --> 00:02:43,040
always 8086 because that's just intel

75
00:02:40,400 --> 00:02:44,879
and the device id was whatever intel set

76
00:02:43,040 --> 00:02:47,120
it to for that particular memory

77
00:02:44,879 --> 00:02:48,720
controller or lpc device so even though

78
00:02:47,120 --> 00:02:50,800
we didn't understand it at the time we

79
00:02:48,720 --> 00:02:52,480
were looking at pci config address space

80
00:02:50,800 --> 00:02:54,640
we were looking at a standardized header

81
00:02:52,480 --> 00:02:57,280
header within the first 40 bytes within

82
00:02:54,640 --> 00:02:59,840
the first 16 bytes of the first 40 bytes

83
00:02:57,280 --> 00:03:01,760
and this is all specified by the pci

84
00:02:59,840 --> 00:03:04,239
special interest group

85
00:03:01,760 --> 00:03:06,239
now sometimes you also need to have the

86
00:03:04,239 --> 00:03:08,239
revision id so it could be a device with

87
00:03:06,239 --> 00:03:09,680
some particular revision and maybe the

88
00:03:08,239 --> 00:03:11,280
vendor wants to make a distinction

89
00:03:09,680 --> 00:03:12,800
between you know slight changes to the

90
00:03:11,280 --> 00:03:13,680
way that the hardware works based on

91
00:03:12,800 --> 00:03:15,840
that

92
00:03:13,680 --> 00:03:17,680
and there's also a class code which can

93
00:03:15,840 --> 00:03:19,599
speak about the generic functionality of

94
00:03:17,680 --> 00:03:21,440
a particular device saying you know i'm

95
00:03:19,599 --> 00:03:22,800
a network card i'm a graphics card and

96
00:03:21,440 --> 00:03:24,239
that kind of thing

97
00:03:22,800 --> 00:03:26,720
and then there's also the header type

98
00:03:24,239 --> 00:03:29,599
which as we said before defines how the

99
00:03:26,720 --> 00:03:31,040
overall hex 40 of information at the

100
00:03:29,599 --> 00:03:33,440
beginning of the configuration address

101
00:03:31,040 --> 00:03:35,120
space works and the most significant bit

102
00:03:33,440 --> 00:03:37,040
of that field is supposed to specify

103
00:03:35,120 --> 00:03:39,120
whether this is a multi-function device

104
00:03:37,040 --> 00:03:41,599
meaning that it has function 0 function

105
00:03:39,120 --> 00:03:44,480
1 etc i've seen multi-function devices

106
00:03:41,599 --> 00:03:46,480
that don't actually have that set though

107
00:03:44,480 --> 00:03:48,640
now just as an aside even if you didn't

108
00:03:46,480 --> 00:03:50,400
have read write everything installed on

109
00:03:48,640 --> 00:03:53,200
a particular windows system it is

110
00:03:50,400 --> 00:03:56,400
possible to see how windows uses these

111
00:03:53,200 --> 00:03:58,319
pci device ids if you go to the device

112
00:03:56,400 --> 00:03:59,920
manager and you eventually select on

113
00:03:58,319 --> 00:04:02,400
something select the details and the

114
00:03:59,920 --> 00:04:05,599
hardware ids you can see right here that

115
00:04:02,400 --> 00:04:07,760
it says pcie then 8086 that would be the

116
00:04:05,599 --> 00:04:11,360
vendor id dev that would be the device

117
00:04:07,760 --> 00:04:13,360
id subsystem and the revision down here

118
00:04:11,360 --> 00:04:15,840
then it shows the cc which would be the

119
00:04:13,360 --> 00:04:15,840
class code

