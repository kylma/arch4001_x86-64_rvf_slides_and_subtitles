1
00:00:00,04 --> 00:00:02,2
So let's talk about the different locations where S

2
00:00:02,2 --> 00:00:02,34
M

3
00:00:02,34 --> 00:00:03,46
Ram could reside

4
00:00:03,94 --> 00:00:04,83
So we saw that S

5
00:00:04,83 --> 00:00:04,95
M

6
00:00:04,95 --> 00:00:08,32
Base can be overwritten by the sme handler via rewriting

7
00:00:08,32 --> 00:00:11,31
the safe state and then it is a 32 bit

8
00:00:11,31 --> 00:00:12,35
value only

9
00:00:12,35 --> 00:00:14,93
So it can only exist somewhere inside of the four

10
00:00:14,93 --> 00:00:16,75
gigabyte physical address range

11
00:00:17,24 --> 00:00:17,44
Now,

12
00:00:17,44 --> 00:00:19,39
typically on modern systems S

13
00:00:19,39 --> 00:00:21,43
Mm is relocated at least once

14
00:00:21,5 --> 00:00:24,5
So it'll start out at S and base of hex

15
00:00:24,5 --> 00:00:25,37
30,000

16
00:00:25,5 --> 00:00:27,56
But then it'll move around for reasons,

17
00:00:27,56 --> 00:00:28,6
we'll see a little bit later

18
00:00:29,14 --> 00:00:30,57
That would look something like this

19
00:00:30,68 --> 00:00:33,35
Let's say that we have the bios here and it's

20
00:00:33,36 --> 00:00:35,25
executing some code from RAM

21
00:00:35,27 --> 00:00:37,72
So the bios may start up in the spi flash

22
00:00:37,72 --> 00:00:40,14
chip but at some point it's going to copy the

23
00:00:40,14 --> 00:00:44,44
code off to RAM so it takes and it copies

24
00:00:44,44 --> 00:00:47,12
the code that it would like to have be the

25
00:00:47,12 --> 00:00:50,96
initial sm RAM code down to physical address hex 30,000

26
00:00:51,54 --> 00:00:52,75
And if you caught it,

27
00:00:53,01 --> 00:00:54,5
that was dark sonic

28
00:00:54,51 --> 00:00:58,08
Following along to be the code that's running in S

29
00:00:58,08 --> 00:00:58,36
mm

30
00:00:58,36 --> 00:00:58,86
Later on,

31
00:00:59,19 --> 00:01:02,51
the bios will also create a copy of the actual

32
00:01:02,51 --> 00:01:04,78
sm room where they're going to want it later somewhere

33
00:01:04,78 --> 00:01:05,17
else

34
00:01:05,39 --> 00:01:09,02
Then they're going to cause a system management interrupt which

35
00:01:09,02 --> 00:01:12,06
will vector to hear this code right here

36
00:01:12,94 --> 00:01:15,69
And then now it's in S mm Now I didn't

37
00:01:15,69 --> 00:01:18,85
actually create this particular picture when I was just you

38
00:01:18,85 --> 00:01:18,99
know,

39
00:01:18,99 --> 00:01:22,45
searching through my MS frizzle pictures before someone else created

40
00:01:22,45 --> 00:01:23,82
this erased face thing

41
00:01:23,82 --> 00:01:26,65
And I just thought it looked interesting and I actually

42
00:01:26,65 --> 00:01:29,49
found this before I found the non erased faced version

43
00:01:29,58 --> 00:01:33,66
But this is a perfect representation of the code executing

44
00:01:33,66 --> 00:01:34,01
in S

45
00:01:34,01 --> 00:01:34,46
Mm

46
00:01:34,7 --> 00:01:35,87
It's in a dark place

47
00:01:35,87 --> 00:01:38,39
It's faceless because you can't see it for reasons we'll

48
00:01:38,39 --> 00:01:39,06
see later

49
00:01:39,44 --> 00:01:40,52
So yeah that's S mm

50
00:01:40,53 --> 00:01:41,75
What does S mm do,

51
00:01:41,75 --> 00:01:45,66
what does this first initial relocating Sm RAM do?

52
00:01:45,79 --> 00:01:48,54
Well it starts at 3800

53
00:01:48,54 --> 00:01:49,62
So the I

54
00:01:49,62 --> 00:01:49,96
P

55
00:01:49,96 --> 00:01:50,99
Is 8000

56
00:01:50,99 --> 00:01:53,12
The bases 30,000

57
00:01:53,13 --> 00:01:56,95
So the entry point is there at 38,000 I think

58
00:01:56,95 --> 00:01:58,75
I said 30 838,000

59
00:01:59,34 --> 00:02:01,54
And what it's going to do is it's going to

60
00:02:01,54 --> 00:02:05,26
take this saved area where the safe state from before

61
00:02:05,27 --> 00:02:05,47
S

62
00:02:05,47 --> 00:02:06,42
Mm was running

63
00:02:06,44 --> 00:02:09,71
And it's going to rewrite that with a new sm

64
00:02:09,71 --> 00:02:13,39
base for the location of the actual later on SmR

65
00:02:13,6 --> 00:02:15,81
So you can see this code can be pretty simple

66
00:02:15,81 --> 00:02:17,82
This just has to be a pretty simple stub that

67
00:02:17,82 --> 00:02:20,74
just kind of smashes S and base with some correct

68
00:02:20,74 --> 00:02:23,56
value that the bios actually wants the sm room to

69
00:02:23,56 --> 00:02:24,04
live at

70
00:02:24,19 --> 00:02:27,7
And then after that it can just resume and boom

71
00:02:27,71 --> 00:02:29,45
like that you're back in bios

72
00:02:29,94 --> 00:02:32,37
So now that you have resumed from S

73
00:02:32,37 --> 00:02:32,49
M

74
00:02:32,49 --> 00:02:32,7
M

75
00:02:32,7 --> 00:02:32,92
S

76
00:02:32,92 --> 00:02:36,04
And base has been updated with whatever value was saved

77
00:02:36,04 --> 00:02:37,41
there before X

78
00:02:37,41 --> 00:02:39,26
123000

79
00:02:40,44 --> 00:02:42,56
And the actual dark sonic

80
00:02:42,56 --> 00:02:45,85
The actual code that's going to run in Sm RAM

81
00:02:45,86 --> 00:02:46,85
is going to be here

82
00:02:46,89 --> 00:02:49,68
So the next time there's a system management interrupt as

83
00:02:49,68 --> 00:02:52,17
a base is going to be 123000

84
00:02:52,18 --> 00:02:54,47
And this is the code that's actually going to run

85
00:02:54,77 --> 00:02:55,13
All right

86
00:02:55,13 --> 00:02:57,74
Intel defines a few locations for S

87
00:02:57,74 --> 00:03:00,85
M RAM and this guidance is basically trying to accommodate

88
00:03:00,85 --> 00:03:01,52
a few things

89
00:03:01,64 --> 00:03:04,13
One is trying to make it easier for bios developers

90
00:03:04,13 --> 00:03:04,88
to just know,

91
00:03:04,89 --> 00:03:05,15
hey,

92
00:03:05,15 --> 00:03:07,01
you should probably put it there too

93
00:03:07,01 --> 00:03:10,13
It's trying to avoid SM RAM overlapping with other stuff

94
00:03:10,14 --> 00:03:10,95
And three,

95
00:03:10,95 --> 00:03:14,09
there are hardware mechanisms put in place to protect SM

96
00:03:14,09 --> 00:03:17,26
RAM and if you want to use those mechanisms,

97
00:03:17,44 --> 00:03:20,56
if you're trying to do something actually that requires restriction

98
00:03:20,56 --> 00:03:23,15
then you should abide by intel's recommendations in order to

99
00:03:23,15 --> 00:03:24,02
get that protection

100
00:03:24,64 --> 00:03:27,29
And so this is all just another part of the

101
00:03:27,29 --> 00:03:29,35
biases job of building up a memory map

102
00:03:29,84 --> 00:03:30,09
Now,

103
00:03:30,1 --> 00:03:34,74
older documentation listed these three locations as possible

104
00:03:34,75 --> 00:03:38,86
S MM locations but on modern PCH systems there's only

105
00:03:38,86 --> 00:03:39,5
two options

106
00:03:39,5 --> 00:03:40,55
So we'll focus on that

107
00:03:41,04 --> 00:03:45,27
There's the compatible or C SEG range and there's just

108
00:03:45,27 --> 00:03:46,52
the thing called T Segway,

109
00:03:46,52 --> 00:03:48,81
they never exactly tell you what T stands for

110
00:03:48,92 --> 00:03:51,54
So we're going to concern ourselves primarily with these,

111
00:03:51,55 --> 00:03:53,93
starting with C SEG and then moving on to T

112
00:03:53,93 --> 00:03:54,26
Segue

113
00:03:55,14 --> 00:03:55,47
Well,

114
00:03:55,47 --> 00:03:58,77
this was our memory map Tetris from before and we

115
00:03:58,77 --> 00:04:00,47
saw within that there was an S

116
00:04:00,47 --> 00:04:04,05
M RAM range that was listed as being at T

117
00:04:04,05 --> 00:04:07,38
sing well with that we'll get to that,

118
00:04:07,38 --> 00:04:08,78
that's going to be T segue for later

119
00:04:08,78 --> 00:04:11,81
But the C SEG is actually down here in this

120
00:04:11,98 --> 00:04:14,12
DOS compatible memory range

121
00:04:14,13 --> 00:04:17,22
So let's drill in on that and see what's stored

122
00:04:17,22 --> 00:04:20,06
there from the fourth generation CPU data sheets

123
00:04:20,06 --> 00:04:23,92
We see something like this and it's basically saying that

124
00:04:23,93 --> 00:04:27,1
S mm will live at a fixed area starting at

125
00:04:27,1 --> 00:04:30,51
physical address A and four zeros and ending at physical

126
00:04:30,51 --> 00:04:32,09
address B and four chefs

127
00:04:32,44 --> 00:04:35,45
There is also referred to as the legacy video area

128
00:04:35,6 --> 00:04:38,86
So this memory ranges actually reused

129
00:04:39,24 --> 00:04:41,84
If the processor is in S mm then it can

130
00:04:41,84 --> 00:04:44,76
be used for system management mode code

131
00:04:45,14 --> 00:04:47,84
But if the processor is not an S mm then

132
00:04:47,84 --> 00:04:50,62
it will be used as the sort of legacy or

133
00:04:50,62 --> 00:04:52,41
DOS video memory area

134
00:04:52,72 --> 00:04:54,84
And as I was googling around because I was just

135
00:04:54,85 --> 00:04:56,83
curious to learn a little bit more about that

136
00:04:56,83 --> 00:04:59,31
I found this good website down here where if you

137
00:04:59,31 --> 00:05:01,52
want to go and learn a little bit more about

138
00:05:01,52 --> 00:05:03,35
how dos used that you can

139
00:05:03,64 --> 00:05:05,71
But for our purposes we're going to think of this

140
00:05:05,72 --> 00:05:09,43
A two will just say A to C as S

141
00:05:09,43 --> 00:05:09,85
M RAM

142
00:05:10,44 --> 00:05:12,83
So just as an fyi while we have this picture

143
00:05:12,83 --> 00:05:13,03
up,

144
00:05:13,03 --> 00:05:15,34
I would just point out that this range right here

145
00:05:15,35 --> 00:05:18,26
F and 000 up to F and F

146
00:05:18,26 --> 00:05:18,42
F

147
00:05:18,42 --> 00:05:18,95
F

148
00:05:18,96 --> 00:05:22,11
This would have been the original 80 86 reset vector

149
00:05:22,12 --> 00:05:25,29
And even on later systems like 80 to 86 which

150
00:05:25,29 --> 00:05:27,45
have 32 bit support still,

151
00:05:27,45 --> 00:05:29,58
if they would have changed the code segment,

152
00:05:29,58 --> 00:05:31,62
they would be sort of restricted to that range right

153
00:05:31,62 --> 00:05:32,05
there

154
00:05:32,14 --> 00:05:35,86
And note this area right here is the expansion area

155
00:05:35,87 --> 00:05:38,79
and that was used for option RAMs back on old

156
00:05:38,79 --> 00:05:41,38
bios is they would copy it into this physical address

157
00:05:41,38 --> 00:05:43,66
range and execute the option rooms from there

158
00:05:44,14 --> 00:05:45,35
So again,

159
00:05:45,36 --> 00:05:48,74
this is going to be the compatible or C Seg

160
00:05:48,75 --> 00:05:51,67
memory range for S mm where dark sonic is hiding

161
00:05:51,67 --> 00:05:52,56
behind the scenes

162
00:05:53,54 --> 00:05:56,0
How do you enable that compatible S mm range

163
00:05:56,0 --> 00:05:58,08
We said it could be video memory or it could

164
00:05:58,08 --> 00:05:59,26
be sm ramp

165
00:05:59,74 --> 00:06:00,07
Well,

166
00:06:00,07 --> 00:06:03,94
there is a register called sM RAMSI System management RAM

167
00:06:03,94 --> 00:06:08,6
control and this has a G S S M RAM

168
00:06:08,61 --> 00:06:12,66
E global or whatever S M RAM enable

169
00:06:13,04 --> 00:06:14,59
And it says that if there's a set to one

170
00:06:14,59 --> 00:06:16,65
compatible sm RAM functions are enabled,

171
00:06:16,65 --> 00:06:19,66
providing 100 and 28 K of diagram accessible at a

172
00:06:20,04 --> 00:06:20,7
you know,

173
00:06:20,7 --> 00:06:22,84
starting at a zero and so going up to B

174
00:06:22,84 --> 00:06:25,13
f F F F and then it says once D

175
00:06:25,13 --> 00:06:25,67
lock is set,

176
00:06:25,67 --> 00:06:26,99
this bit becomes our oh,

177
00:06:27,0 --> 00:06:29,15
so we're going to learn about deadlock in a second

178
00:06:29,84 --> 00:06:30,25
Now,

179
00:06:30,25 --> 00:06:32,37
I would just make the point that this sm room

180
00:06:32,37 --> 00:06:35,92
register was located in a different location back on em

181
00:06:35,92 --> 00:06:36,32
ch,

182
00:06:36,32 --> 00:06:38,6
000 90

183
00:06:38,65 --> 00:06:41,94
And then it disappeared in the documentation for the first

184
00:06:41,94 --> 00:06:44,88
through third generation cores And then it reappeared magically in

185
00:06:44,88 --> 00:06:46,19
the fourth generation,

186
00:06:46,19 --> 00:06:47,56
at a slightly different address,

187
00:06:47,57 --> 00:06:49,46
000 88

188
00:06:49,74 --> 00:06:52,06
And then it has now disappeared again on the 10th

189
00:06:52,06 --> 00:06:53,31
generation systems

190
00:06:53,84 --> 00:06:54,27
Now,

191
00:06:54,28 --> 00:06:58,14
I don't know exactly yet whether or not compatible range

192
00:06:58,14 --> 00:06:58,94
is just straight up,

193
00:06:58,94 --> 00:06:59,82
not supported

194
00:07:00,07 --> 00:07:02,43
There is some evidence that I will show you in

195
00:07:02,43 --> 00:07:03,55
a little bit for that

196
00:07:03,55 --> 00:07:04,96
It might not be supported anymore,

197
00:07:05,34 --> 00:07:07,13
But I don't have a 10th generation,

198
00:07:07,13 --> 00:07:08,85
so I can't fiddle with it

