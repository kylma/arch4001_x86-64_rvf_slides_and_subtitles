1
00:00:00,640 --> 00:00:04,960
now let's learn about t-seg toward and

2
00:00:02,720 --> 00:00:06,640
the case of the stolen graphics memory

3
00:00:04,960 --> 00:00:08,480
which is to say t-seg we're going to

4
00:00:06,640 --> 00:00:10,480
learn about t-seg now an interesting

5
00:00:08,480 --> 00:00:12,639
thing about the 11th generation core

6
00:00:10,480 --> 00:00:15,280
data sheets is that they don't even list

7
00:00:12,639 --> 00:00:17,840
the compatible or c-seg anymore they

8
00:00:15,280 --> 00:00:19,840
only list tseg so this is why i said

9
00:00:17,840 --> 00:00:22,480
before i don't know whether the reason

10
00:00:19,840 --> 00:00:23,439
that things like smram c went away is

11
00:00:22,480 --> 00:00:25,279
because

12
00:00:23,439 --> 00:00:27,680
a it could be because intel you know

13
00:00:25,279 --> 00:00:29,519
messed up the documentation that happens

14
00:00:27,680 --> 00:00:31,359
b it could have you know changed named

15
00:00:29,519 --> 00:00:32,880
something completely different c they

16
00:00:31,359 --> 00:00:34,399
could straight up not be supporting

17
00:00:32,880 --> 00:00:37,040
compatible

18
00:00:34,399 --> 00:00:39,760
sm ram anymore i don't know exactly

19
00:00:37,040 --> 00:00:42,559
needs further investigation but anyways

20
00:00:39,760 --> 00:00:45,440
so let's learn about tseg it's defined

21
00:00:42,559 --> 00:00:47,680
as toled minus stolen minus t seg two

22
00:00:45,440 --> 00:00:49,920
told minus stolen well what does that

23
00:00:47,680 --> 00:00:53,440
mean what's toilet what's stolen in our

24
00:00:49,920 --> 00:00:57,120
memory map tetris tolud was listed there

25
00:00:53,440 --> 00:01:00,160
and above tolud is this pci memory

26
00:00:57,120 --> 00:01:02,800
that's the mmio space that is used for

27
00:01:00,160 --> 00:01:04,000
things like the extended configuration

28
00:01:02,800 --> 00:01:06,880
address space

29
00:01:04,000 --> 00:01:08,880
below toled is the graphics memory or

30
00:01:06,880 --> 00:01:11,520
graphic stolen memory as you'll see it's

31
00:01:08,880 --> 00:01:14,880
called by the documentation

32
00:01:11,520 --> 00:01:17,360
and then beyond that is tseg so that's

33
00:01:14,880 --> 00:01:20,400
what we're going to care about t segment

34
00:01:17,360 --> 00:01:23,200
and you can see it's listed as sm ram so

35
00:01:20,400 --> 00:01:25,200
between t segment and toilet is this

36
00:01:23,200 --> 00:01:27,200
graphics memory which is called the

37
00:01:25,200 --> 00:01:30,560
graphic stolen memory in the graphics

38
00:01:27,200 --> 00:01:32,640
gtt stolen memory so we gotta understand

39
00:01:30,560 --> 00:01:35,200
what's going on in this little snippet

40
00:01:32,640 --> 00:01:37,600
of the memory space well zooming in on

41
00:01:35,200 --> 00:01:39,840
it from the 11th generation data sheet

42
00:01:37,600 --> 00:01:42,479
you've got the pci memory range and then

43
00:01:39,840 --> 00:01:44,240
you've got to load toward is defined as

44
00:01:42,479 --> 00:01:46,159
bus zero device zero function zero

45
00:01:44,240 --> 00:01:49,040
offset bc

46
00:01:46,159 --> 00:01:51,600
the graphics stolen memory starts at bus

47
00:01:49,040 --> 00:01:53,280
zero device zero offset zero

48
00:01:51,600 --> 00:01:57,840
plus zero device zero function zero

49
00:01:53,280 --> 00:01:59,360
offset b0 this one's b4 and tseg is b8

50
00:01:57,840 --> 00:02:02,079
so you can see a whole bunch of stuff in

51
00:01:59,360 --> 00:02:04,560
the memory controller space plus zero

52
00:02:02,079 --> 00:02:08,000
bus zero device zero function zero and

53
00:02:04,560 --> 00:02:10,319
so the smram here is bounded by t-seg on

54
00:02:08,000 --> 00:02:12,239
the bottom and graphic stolen memory on

55
00:02:10,319 --> 00:02:13,760
the top so let's look at the definition

56
00:02:12,239 --> 00:02:18,160
of those registers

57
00:02:13,760 --> 00:02:19,680
all right toled top of lower usable dram

58
00:02:18,160 --> 00:02:23,120
offset bc

59
00:02:19,680 --> 00:02:25,520
so this register has bits 20 to 31 are

60
00:02:23,120 --> 00:02:27,599
used for a address which is the top of

61
00:02:25,520 --> 00:02:30,080
lower usable dram then there's some

62
00:02:27,599 --> 00:02:32,959
reserve space then there's a lock

63
00:02:30,080 --> 00:02:35,599
all right the top of lower usable dram

64
00:02:32,959 --> 00:02:37,440
is basically saying in the context of

65
00:02:35,599 --> 00:02:39,280
you know this space

66
00:02:37,440 --> 00:02:41,040
toilet is here and it's saying this is

67
00:02:39,280 --> 00:02:43,120
ram stuff below this like there's the

68
00:02:41,040 --> 00:02:45,760
normal main memory there's a dma

69
00:02:43,120 --> 00:02:48,720
protected region as it's called some sm

70
00:02:45,760 --> 00:02:51,280
ram some stolen memory for the graphics

71
00:02:48,720 --> 00:02:53,840
but then above toled is where you start

72
00:02:51,280 --> 00:02:56,319
seeing pcie and memory mapped i o and

73
00:02:53,840 --> 00:02:58,239
things like that so this above it is

74
00:02:56,319 --> 00:03:01,200
like space that is used for memory

75
00:02:58,239 --> 00:03:03,440
mapped i o and this down here is special

76
00:03:01,200 --> 00:03:05,599
ram usage effectively

77
00:03:03,440 --> 00:03:08,239
so toilet is the top of lower usable

78
00:03:05,599 --> 00:03:10,080
dram and we had talked very briefly

79
00:03:08,239 --> 00:03:11,760
before i think we'll see it again yeah

80
00:03:10,080 --> 00:03:13,920
we will see it again

81
00:03:11,760 --> 00:03:15,840
that there is a way to sort of reclaim

82
00:03:13,920 --> 00:03:18,720
this physical memory space that's stolen

83
00:03:15,840 --> 00:03:20,480
by memory mapped i o so basically toled

84
00:03:18,720 --> 00:03:24,799
is where does the

85
00:03:20,480 --> 00:03:26,959
thievery for memory mapped io start

86
00:03:24,799 --> 00:03:29,519
and there's sonic again

87
00:03:26,959 --> 00:03:32,720
all right so toled top of lower usable

88
00:03:29,519 --> 00:03:35,280
dram then there's the base of data of

89
00:03:32,720 --> 00:03:37,280
stolen memory and so this is graphics

90
00:03:35,280 --> 00:03:41,200
data stolen dram

91
00:03:37,280 --> 00:03:43,840
and it has this again 28 to 31 is the

92
00:03:41,200 --> 00:03:46,560
base and there's a lock bit

93
00:03:43,840 --> 00:03:49,040
then there's the gtt stolen memory this

94
00:03:46,560 --> 00:03:52,000
stands for graphics translation table

95
00:03:49,040 --> 00:03:53,120
and that again has bits 20 to 31 with a

96
00:03:52,000 --> 00:03:56,640
lock bit

97
00:03:53,120 --> 00:03:58,239
and finally tseg bits 20 to 31 and a

98
00:03:56,640 --> 00:04:00,000
lock bit

99
00:03:58,239 --> 00:04:01,760
and so the question is why do all these

100
00:04:00,000 --> 00:04:04,720
things have a lock bit

101
00:04:01,760 --> 00:04:06,319
well so let's think of it this way if

102
00:04:04,720 --> 00:04:08,239
toilet is the start of sort of the

103
00:04:06,319 --> 00:04:10,000
memory mapped i o and it's you know

104
00:04:08,239 --> 00:04:12,480
stuff above there is you know not going

105
00:04:10,000 --> 00:04:14,560
to ever be used as ram ram until you use

106
00:04:12,480 --> 00:04:16,799
the remapping mechanism

107
00:04:14,560 --> 00:04:19,120
the stuff down here is basically an

108
00:04:16,799 --> 00:04:20,560
adjustable range of how much memory

109
00:04:19,120 --> 00:04:22,240
needs to be stolen for this how much

110
00:04:20,560 --> 00:04:24,560
memory needs to be stolen for that how

111
00:04:22,240 --> 00:04:27,919
much memory needs to be used for smram

112
00:04:24,560 --> 00:04:29,199
and so forth so by placing tsegmb you're

113
00:04:27,919 --> 00:04:30,720
going to want to place it at some

114
00:04:29,199 --> 00:04:32,639
location and then you're going to want

115
00:04:30,720 --> 00:04:34,800
to lock it in place and you're going to

116
00:04:32,639 --> 00:04:35,840
want to place the base of the gtt and

117
00:04:34,800 --> 00:04:37,919
then you're going to want to lock it in

118
00:04:35,840 --> 00:04:40,639
place because if you don't lock these

119
00:04:37,919 --> 00:04:42,400
things in place and if sm ram is going

120
00:04:40,639 --> 00:04:44,479
to be this area that has this highly

121
00:04:42,400 --> 00:04:46,800
privileged code that can scribble all

122
00:04:44,479 --> 00:04:48,720
over everyone's memory everywhere

123
00:04:46,800 --> 00:04:50,240
then you have these potential attacks

124
00:04:48,720 --> 00:04:52,240
which i'm just calling titch attacks

125
00:04:50,240 --> 00:04:53,680
here because we're going to move things

126
00:04:52,240 --> 00:04:56,479
just a titch

127
00:04:53,680 --> 00:04:59,520
so if you didn't lock down this base of

128
00:04:56,479 --> 00:05:02,240
gtt stolen memory then an attacker could

129
00:04:59,520 --> 00:05:04,160
stitch it down this way and scribble all

130
00:05:02,240 --> 00:05:05,360
over this stuff and then move it back up

131
00:05:04,160 --> 00:05:07,280
and they would have effectively

132
00:05:05,360 --> 00:05:10,160
scribbled over sm ram

133
00:05:07,280 --> 00:05:12,240
same thing if the tseg segment was not

134
00:05:10,160 --> 00:05:13,440
locked down they could just move it a

135
00:05:12,240 --> 00:05:16,160
little titch

136
00:05:13,440 --> 00:05:18,160
and move it up and scribble over sm ram

137
00:05:16,160 --> 00:05:20,720
and then you know get arbitrary code in

138
00:05:18,160 --> 00:05:24,160
smm or for instance they could slide a

139
00:05:20,720 --> 00:05:26,000
t-segment down into main memory so they

140
00:05:24,160 --> 00:05:26,880
can put their attacker controlled code

141
00:05:26,000 --> 00:05:29,120
there

142
00:05:26,880 --> 00:05:32,400
push the sm base down here and then as

143
00:05:29,120 --> 00:05:34,560
long as their code is at sm base plus

144
00:05:32,400 --> 00:05:36,000
x8000 then their attacker controlled

145
00:05:34,560 --> 00:05:38,639
code would run there

146
00:05:36,000 --> 00:05:40,400
so these things right here basically you

147
00:05:38,639 --> 00:05:42,880
know the thing we really care about is

148
00:05:40,400 --> 00:05:45,199
between tsegmb and the graphics stolen

149
00:05:42,880 --> 00:05:47,520
memory that is going to be our tseg that

150
00:05:45,199 --> 00:05:49,280
is going to be our sm ram that's the

151
00:05:47,520 --> 00:05:51,199
place that you know intel generally

152
00:05:49,280 --> 00:05:54,240
recommends you move from compatible

153
00:05:51,199 --> 00:05:56,319
memory region up into this t-seg because

154
00:05:54,240 --> 00:05:58,319
t-seg is granted

155
00:05:56,319 --> 00:06:00,639
extra protections it's granted

156
00:05:58,319 --> 00:06:03,360
protection against dma attacks just

157
00:06:00,639 --> 00:06:05,759
automatically whatever is in this range

158
00:06:03,360 --> 00:06:08,000
is not accessible via dma translate

159
00:06:05,759 --> 00:06:10,240
transactions and it's also protected

160
00:06:08,000 --> 00:06:12,319
against someone just directly scribbling

161
00:06:10,240 --> 00:06:16,319
into it so building up our attack tree a

162
00:06:12,319 --> 00:06:19,840
little bit more the t-seg defense is to

163
00:06:16,319 --> 00:06:22,000
lock the t-segment and the bgsm the base

164
00:06:19,840 --> 00:06:24,479
of gtt stolen memory

165
00:06:22,000 --> 00:06:27,919
and that will afford protection for the

166
00:06:24,479 --> 00:06:30,400
t-seg range against both dma attacks and

167
00:06:27,919 --> 00:06:32,319
attacks originating from the cpu now

168
00:06:30,400 --> 00:06:34,800
just like protected range registers it

169
00:06:32,319 --> 00:06:36,800
is possible that even if the defender

170
00:06:34,800 --> 00:06:38,960
locked down these things they could have

171
00:06:36,800 --> 00:06:40,800
misconfigured it so an attacker could

172
00:06:38,960 --> 00:06:43,600
try to exploit a misconfiguration of

173
00:06:40,800 --> 00:06:45,840
tseg where the you know defender for

174
00:06:43,600 --> 00:06:48,240
instance set tsag too high but they're

175
00:06:45,840 --> 00:06:51,680
actually using sm ram below tseg or

176
00:06:48,240 --> 00:06:54,240
above tcg and so consequently

177
00:06:51,680 --> 00:06:56,240
that would be an exploitable scenario so

178
00:06:54,240 --> 00:06:58,720
the defense against that is to carefully

179
00:06:56,240 --> 00:07:00,720
analyze the t-seg range versus where the

180
00:06:58,720 --> 00:07:03,120
smm code is actually placed all right

181
00:07:00,720 --> 00:07:05,360
now i'm going to introduce a crossbar

182
00:07:03,120 --> 00:07:06,639
here because most of the rest of these

183
00:07:05,360 --> 00:07:07,919
defenses that we're going to be talking

184
00:07:06,639 --> 00:07:11,039
about in this threat tree apply

185
00:07:07,919 --> 00:07:13,840
equivalently well to seaseg and t say so

186
00:07:11,039 --> 00:07:16,880
for instance there's an attack of sleep

187
00:07:13,840 --> 00:07:18,560
wake attacks which as with the

188
00:07:16,880 --> 00:07:19,919
protecting the spy flash chip we said

189
00:07:18,560 --> 00:07:21,280
we're going to cover later so that's

190
00:07:19,919 --> 00:07:23,199
going to be the next major section in

191
00:07:21,280 --> 00:07:25,039
the class so we'll skip that for now

192
00:07:23,199 --> 00:07:27,199
okay well you're done and you're not

193
00:07:25,039 --> 00:07:28,800
done so you're done with learning about

194
00:07:27,199 --> 00:07:30,639
sm ram for now

195
00:07:28,800 --> 00:07:32,319
and you've seen a little bit of write

196
00:07:30,639 --> 00:07:34,240
protection but there's more to see for

197
00:07:32,319 --> 00:07:37,120
right protection as we make our way to

198
00:07:34,240 --> 00:07:37,120
the attacks

