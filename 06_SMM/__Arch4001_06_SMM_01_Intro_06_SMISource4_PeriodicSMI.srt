1
00:00:00,160 --> 00:00:04,080
and the last source of smis that we're

2
00:00:02,399 --> 00:00:06,640
going to highlight for its malware

3
00:00:04,080 --> 00:00:07,759
benefiting implications are the periodic

4
00:00:06,640 --> 00:00:09,599
smi's

5
00:00:07,759 --> 00:00:12,400
so if we once again look at our causes

6
00:00:09,599 --> 00:00:14,639
of smi's and scroll down we'll find the

7
00:00:12,400 --> 00:00:16,960
periodic timer expiring and the

8
00:00:14,639 --> 00:00:19,600
additional enable is periodic enable and

9
00:00:16,960 --> 00:00:22,160
the status in periodic status

10
00:00:19,600 --> 00:00:24,400
so we are actually back to the smi

11
00:00:22,160 --> 00:00:27,920
enable we saw that before pm base plus

12
00:00:24,400 --> 00:00:31,439
30 and bit 14 is going to enable

13
00:00:27,920 --> 00:00:34,000
periodic sms the smi handler can then

14
00:00:31,439 --> 00:00:36,480
see that it got into smm because of a

15
00:00:34,000 --> 00:00:39,680
periodic smi by checking the periodic

16
00:00:36,480 --> 00:00:41,040
status which is npm base plus 34 smi

17
00:00:39,680 --> 00:00:42,960
status

18
00:00:41,040 --> 00:00:45,200
so somewhere completely different is the

19
00:00:42,960 --> 00:00:47,520
general power management configuration

20
00:00:45,200 --> 00:00:50,079
one register at bus zero device 31

21
00:00:47,520 --> 00:00:53,520
function zero our favorite lpc device

22
00:00:50,079 --> 00:00:57,360
offset a0 and bits 0 and 1 specify

23
00:00:53,520 --> 00:00:59,039
whether it fires every 8 16 32 or 64

24
00:00:57,360 --> 00:01:00,879
seconds

25
00:00:59,039 --> 00:01:03,359
so why do i say that this has malware

26
00:01:00,879 --> 00:01:05,600
implications well if you have code

27
00:01:03,359 --> 00:01:08,000
running in system management mode it's

28
00:01:05,600 --> 00:01:10,720
only ever going to wake up and execute

29
00:01:08,000 --> 00:01:13,040
if an smi fires and there need not

30
00:01:10,720 --> 00:01:14,560
necessarily be a bunch of smi's firing

31
00:01:13,040 --> 00:01:16,640
all the time they're mostly for power

32
00:01:14,560 --> 00:01:18,799
management type things a lot of them

33
00:01:16,640 --> 00:01:21,200
much more frequently occur on like

34
00:01:18,799 --> 00:01:22,640
laptop type systems not as much on

35
00:01:21,200 --> 00:01:24,560
desktop systems

36
00:01:22,640 --> 00:01:26,159
so if you have malware and s m you need

37
00:01:24,560 --> 00:01:29,439
a way to run your malware to do

38
00:01:26,159 --> 00:01:30,960
malicious things and so consequently the

39
00:01:29,439 --> 00:01:32,400
when i was doing this proof of concept

40
00:01:30,960 --> 00:01:34,560
light eater malware which was you know

41
00:01:32,400 --> 00:01:36,400
stealing gbg keys from tails out of

42
00:01:34,560 --> 00:01:38,640
memory i wanted to wake up every now and

43
00:01:36,400 --> 00:01:40,320
then and be able to scan memory in order

44
00:01:38,640 --> 00:01:42,960
to see if there was keying material to

45
00:01:40,320 --> 00:01:45,759
steal and the periodic smi is a great

46
00:01:42,960 --> 00:01:45,759
way to do that

